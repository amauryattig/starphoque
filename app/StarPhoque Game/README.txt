Double-cliquez sur index.html pour ouvrir le jeu. Fonctionne avec Chrome et Opera PC et Chrome,Firefox et Opera Mobile.

Pour commencer une partie, choisissez un niveau dans le menu d�roulant "chooseLevel" et cliquez sur "startGame" dans la zone en haut � droite.

Les niveaux qui fonctionnent pr�sentement sont 1:Corneria,4:Fichina et 5:Sun.
Les autres sont en construction.

Les contr�les de l'avion sont sur le pav� num�rique 4/5/6/8 pour les mouvements et 7/9 pour freiner/acc�l�rer.

Bonne partie!