//APP.MovingObjectsPositionRegister = [[]];

// Units
APP.__Unit = function (params) {
    var gl = this.gl = APP.WebGLContext;
    var defaults = {

        name: "unit",
        position: vec3.fromValues(0, 0, 0),//Position
        type: "arwing", // Type of unit
        health: 100,//amount of elements
        playerOwner: 1,
        groupMesh: null,
        projectileTypes: ["defaultProjectile"],
        damage: 10,
        energy: 100,
        velocity: vec3.fromValues(0, 0, 0),
        acceleration: vec3.fromValues(0, 0, 0),
       // camera: this.camera,
        moveConstraints: [-40, 40, -2, 40, -3000, 10],
        camera: this.camera,
        isMoveEnabled : true,
    };
    var self = this;
    var _attrs = _.extend(defaults, _.clone(params));

    //Parameters

    this.name = _attrs.name;
    this.position = _attrs.position;
    this.type = _attrs.type;
    this.health = _attrs.health;
    this.playerOwner = _attrs.playerOwner;

    this.projectileTypes = _attrs.projectileTypes;
    this.damage = _attrs.damage;
    this.energy = _attrs.energy;
    this.velocity = _attrs.velocity;//Must not be touched
    this.acceleration = _attrs.acceleration;
    this.moveConstraints = _attrs.moveConstraints;
    this.camera = _attrs.camera;
    this.isMoveEnabled = _attrs.isMoveEnabled;

    //Local variables
    this.updatedVelocity = this.velocity;//the original velocity must not be touched
    var increasingFactor = 0;
    var decreasingFactor = 10;
    var velocity = _attrs.velocity;
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////    I N I T I A L I Z E    U N I T   /////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //_attrs.camera.add(_attrs.groupMesh);

    //APP.MovingObjectsPositionRegister[this.id] = groupMesh.transform.position.xyz;
    // AAAAAAAAAAAAAAAAAAAAAA  A R W I N G AAAAAAAAAAAAAAAAAAAAAAAAAA //
    //Load new arwing model by default
    if (this.type == "arwing" && _attrs.groupMesh == null) {

        var mesh = new APP.Mesh({

            strMeshOBJ: APP.ModelArwingOBJ.meshArwing["mesh0"],
            strTextureOBJ: APP.ModelArwingOBJ.textureArwing["mesh0"],
        });
        var mesh1 = new APP.Mesh({

            strMeshOBJ: APP.ModelArwingOBJ.meshArwing["mesh1"],
            strTextureOBJ: APP.ModelArwingOBJ.textureArwing["mesh1"],
        });
        var mesh2 = new APP.Mesh({

            strMeshOBJ: APP.ModelArwingOBJ.meshArwing["mesh2"],
            strTextureOBJ: APP.ModelArwingOBJ.textureArwing["mesh2"],

        });

        groupMesh.add(mesh);
        groupMesh.add(mesh1);
        groupMesh.add(mesh2);
        groupMesh.transform.rotation.y = 180;
        groupMesh.transform.scale.xyz = vec3.fromValues(0.1, 0.1, 0.1);
        _attrs.groupMesh = groupMesh;
    } 
        
    this.add(_attrs.groupMesh); //= _attrs.groupMesh;


    // AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA //

    

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////// A N I M A T E ////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //this.animate = function(){

    // AAAAAAAAAAAAAAAAAAAAAA  A R W I N G AAAAAAAAAAAAAAAAAAAAAAAAAA //
    // if (type == "arwing") {

    //_.each(groupMesh.children, function (child) {

    //    //flake rotate
    //    child.transform.rotation.z += 2 * Math.random() - 4 * Math.random();
    //    //flake falls
    //  //  child.transform.position.x += velocity[0] + (acceleration[0] * acceleration[0]) * Math.random() * 0.05;
    //    child.transform.position.y += velocity[1]; //- (acceleration[1]) * Math.random() * 0.05;
    // //   child.transform.position.z += velocity[2] + (acceleration[2] * acceleration[2]) * Math.random() * 0.05;
    //    //flake dies and is recycled above
    //    if (child.transform.position.y < -2) {
    //        child.transform.position.xyz = vec3.fromValues(center[0] + (Math.random() * scaleX) / 2,
    //                                                startingHeight,
    //                                                center[2] + (Math.random() * scaleZ) / 2)
    //    }
    //});
    //}

    //**************  Arwing Control *************************//

    // OO OO OO  BARREL ROLLING OO OO OO //
    this.doABarrelRoll = function (increasingFactor) {
        var decreasingFactor = Math.pow(2, -(increasingFactor)) * 80;
        _attrs.groupMesh.transform.rotation.z = _attrs.groupMesh.transform.rotation.z*decreasingFactor;
        if (decreasingFactor <= 0.001) {
            this.increasingFactor = 0;//reset global increasing Factor
            this.resetArwingRotation();
        }
    }

    // <- <- <-  MOVE LEFT <- <- <- //
    this.moveLeft = function () {
        _attrs.groupMesh.transform.position.x += -0.25;
        _attrs.camera.position[0] += -0.25;
        if (_attrs.groupMesh.transform.rotation.z >= -45) {
            _attrs.groupMesh.transform.rotation.z += -5;
            _attrs.groupMesh.transform.rotation.y += 5;
        }
        console.log("left");
    }
    // -> -> ->  MOVE RIGHT -> -> -> //
    this.moveRight = function () {
        _attrs.groupMesh.transform.position.x += 0.25;
        _attrs.camera.position[0] += 0.25;
        if (_attrs.groupMesh.transform.rotation.z <= 45) {
            _attrs.groupMesh.transform.rotation.z += 5;
            _attrs.groupMesh.transform.rotation.y += -5;
        }
        console.log("right");
    }

    // /\ /\ /\  MOVE UP /\ /\ /\ //
    this.moveUp = function () {
        _attrs.groupMesh.transform.position.y += 0.15;
        _attrs.camera.position[1] += 0.15;
        if (_attrs.groupMesh.transform.rotation.x >= -45) {
            _attrs.groupMesh.transform.rotation.x += -5;
        }
        console.log("up");
    }

    // \/ \/ \/  MOVE DOWN \/ \/ \/ //
    this.moveDown = function () {
        _attrs.groupMesh.transform.position.y += -0.15;
        _attrs.camera.position[1] += -0.15;
        if (_attrs.groupMesh.transform.rotation.x <= 45) {
            _attrs.groupMesh.transform.rotation.x += 5;
        }
        console.log("down");
    }

    // ++ ++ ++  ACCELERATING ++ ++ ++ //
    this.accelerate = function () {
        
        if (this.energy > 4) {
            this.energy += -4;
            this.acceleration = vec3.fromValues(0, 0, -0.05);
            //camera.setVertigoEffect(true);
        }  
        if (this.energy <= 8 || APP.isAccelerating == false) {
            this.acceleration = vec3.fromValues(0, 0, 0);
            this.updatedVelocity = APP.originalVelocity;//go back to original velocity
            _attrs.velocity = APP.originalVelocity;
            this.resetRotation();
            console.log("NO ACCELERATION:OUT OF ENERGY");
            //camera.setVertigoEffect(false);
        }
       // console.log("updated:" + this.updatedVelocity[2] + "original:" + this.velocity[2]);
    }

    // -- -- --  DECELERATING -- -- -- //
    this.decelerate = function () {
        
        if (this.energy > 4) {
            this.energy += -4;
            this.acceleration = vec3.fromValues(0, 0, 0.05);
        }
        if (this.energy <= 8 || APP.isAccelerating == false) {
            this.acceleration = vec3.fromValues(0, 0, 0);
            this.updatedVelocity = APP.originalVelocity;//go back to original velocity
            _attrs.velocity = APP.originalVelocity;
            this.resetRotation();
         //   console.log("NO DECELERATION:OUT OF ENERGY");
        }
        console.log("decelerate");
    }

    this.resetRotation = function () {
        if (_attrs.groupMesh.transform.rotation.x > 0) {
            _attrs.groupMesh.transform.rotation.x += -10;
        }
        if (_attrs.groupMesh.transform.rotation.x < 0) {
            _attrs.groupMesh.transform.rotation.x += 10;
        }
        if (_attrs.groupMesh.transform.rotation.y > 180) {
            _attrs.groupMesh.transform.rotation.y += -10;
        }
        if (_attrs.groupMesh.transform.rotation.y < 180) {
            _attrs.groupMesh.transform.rotation.y += 10;
        }
        if (_attrs.groupMesh.transform.rotation.z > 0) {
            _attrs.groupMesh.transform.rotation.z += -10;
        }
        if (_attrs.groupMesh.transform.rotation.z < 0) {
            _attrs.groupMesh.transform.rotation.z += 10;
        }
        //this.acceleration = vec3.fromValues(0, 0, 0);
        //this.updatedVelocity = APP.originalVelocity;
        //_attrs.velocity = APP.originalVelocity;
        //_attrs.camera.position[0] = _attrs.groupMesh.transform.position.x + this.acceleration[0];
        //_attrs.camera.position[1] = _attrs.groupMesh.transform.position.y + this.acceleration[1];
        //_attrs.camera.position[2] = _attrs.groupMesh.transform.position.z + 2 + this.acceleration[2];
        console.log("reset");

    }

    // AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA //
    //  }

    //this._attrs.groupMesh = _attrs.groupMesh;

    //}
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////// D R A W  //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Private drawing function.
    this._drawUnit = function (args) {

        
        if (this.isFrustumCulled(args.frustum)) {
            if (this.isMoveEnabled == true) {
                // MMMMMMMMMMMMMMMMMMMMM   MOVEMENTS MMMMMMMMMMMMMMMMMMMMM //
                //this.draw();
                //var this = this.this;
                //update unit position
                //this.updatedVelocity[0] + this.acceleration[0];// * _attrs.groupMesh.transform.position.x;
                //this.updatedVelocity[1] += this.acceleration[1];// * _attrs.groupMesh.transform.position.y;
                //this.updatedVelocity[2] += this.acceleration[2];// * _attrs.groupMesh.transform.position.z;

                if (APP.isAccelerating == true) {
                    this.updatedVelocity[0] + this.acceleration[0];// * _attrs.groupMesh.transform.position.x;
                    this.updatedVelocity[1] += this.acceleration[1];// * _attrs.groupMesh.transform.position.y;
                    this.updatedVelocity[2] += this.acceleration[2];// * _attrs.groupMesh.transform.position.z;
                    _attrs.groupMesh.transform.position.x += this.updatedVelocity[0] ;
                    _attrs.groupMesh.transform.position.y += this.updatedVelocity[1] ;
                    _attrs.groupMesh.transform.position.z += this.updatedVelocity[2] ;
                }
                else if (APP.isAccelerating == false) {
                    _attrs.groupMesh.transform.position.x += APP.originalVelocity[0];
                    _attrs.groupMesh.transform.position.y += APP.originalVelocity[1];
                    _attrs.groupMesh.transform.position.z += APP.originalVelocity[2];
                }



                _attrs.camera.position[0] = _attrs.groupMesh.transform.position.x;
                _attrs.camera.position[1] = _attrs.groupMesh.transform.position.y;
                _attrs.camera.position[2] = _attrs.groupMesh.transform.position.z+3;

                //Move Constraints XYZ
                APP.Math.clamp(_attrs.groupMesh.transform.position.x, this.moveConstraints[0], this.moveConstraints[1]);
                APP.Math.clamp(_attrs.groupMesh.transform.position.y, this.moveConstraints[2], this.moveConstraints[3]);
                APP.Math.clamp(_attrs.groupMesh.transform.position.z, this.moveConstraints[4], this.moveConstraints[5]);

                //if (_attrs.groupMesh.transform.position.x < moveConstraints[0]) {
                //    _attrs.groupMesh.transform.position.x = moveConstraints[0];
                //}
                //if (_attrs.groupMesh.transform.position.x > moveConstraints[1]) {
                //    _attrs.groupMesh.transform.position.x = moveConstraints[1];
                //}

                //if (_attrs.groupMesh.transform.position.y < moveConstraints[2]) {
                //    _attrs.groupMesh.transform.position.y = moveConstraints[2];
                //}
                //if (_attrs.groupMesh.transform.position.y > moveConstraints[3]) {
                //    _attrs.groupMesh.transform.position.y = moveConstraints[3];
                //}

                //if (_attrs.groupMesh.transform.position.z < moveConstraints[4]) {
                //    _attrs.groupMesh.transform.position.z = moveConstraints[4];
                //}
                //if (_attrs.groupMesh.transform.position.z > moveConstraints[5]) {
                //    _attrs.groupMesh.transform.position.z = moveConstraints[5];
                //}

                //Movements
                //**************  Arwing Control *************************//

                // OO OO OO  BARREL ROLLING OO OO OO //
                if (APP.isBarrelRolling == true) {
                    this.increasingFactor += 0.8;
                    this.doABarrelRoll(increasingFactor);
                }

                // <- <- <-  MOVE LEFT <- <- <- //
                if (APP.isMovingLeft == true) {
                    this.moveLeft();
                }


                // -> -> ->  MOVE RIGHT -> -> -> //
                if (APP.isMovingRight == true) {
                    this.moveRight();
                }

                // /\ /\ /\  MOVE UP /\ /\ /\ //
                if (APP.isMovingUp == true) {
                    this.moveUp();
                }

                // \/ \/ \/  MOVE DOWN \/ \/ \/ //
                if (APP.isMovingDown == true) {
                    this.moveDown();
                }

                // ++ ++ ++  ACCELERATING ++ ++ ++ //
                if (APP.isAccelerating == true) {
                    this.accelerate();
                }

                // -- -- --  DECELERATING -- -- -- //
                if (APP.isDecelerating == true) {
                    this.decelerate();
                }

                // _____  RESET ROTATION WHEN NO ACTION  ____ //
                if (APP.isBarrelRolling == false &&
                    APP.isMovingLeft == false &&
                    APP.isMovingRight == false &&
                    APP.isMovingUp == false &&
                    APP.isMovingDown == false &&
                    APP.isAccelerating == false &&
                    APP.isDecelerating == false && (
                    _attrs.groupMesh.transform.rotation.x != 0 ||
                    _attrs.groupMesh.transform.rotation.y != 180 ||
                    _attrs.groupMesh.transform.rotation.z != 0)
                    ) {
                    this.resetRotation();
                }

                if (this.energy < 100) {//refill energy 
                    this.energy += 1;
                }
            }
            // MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM //
            
            this.updateWorldMatrix();
            //*****************************************************//
            //console.log(this.energy);
            //console.log("WWWWWWWWWWWWWWW" ,APP.isBarrelRolling ,
            //    APP.isMovingLeft ,
            //    APP.isMovingRight ,
            //    APP.isMovingUp,
            //    APP.isMovingDown ,
            //    APP.isAccelerating ,
            //    APP.isDecelerating ,
            //    _attrs.groupMesh.transform.rotation.x ,
            //    _attrs.groupMesh.transform.rotation.y ,
            //    _attrs.groupMesh.transform.rotation.z );
            //console.log("rotx:" + _attrs.groupMesh.transform.rotation.x + "roty:" + _attrs.groupMesh.transform.rotation.y + "rotz:" + _attrs.groupMesh.transform.rotation.z);
            console.log("vel:" + _attrs.velocity[2] + "updated:" + this.updatedVelocity[2] + "origin:" + APP.originalVelocity[2]);
            // AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA //


            // Y

            //var shader = args.defaultShader;

            //// Bind uniforms.
            //shader.uniforms.setValues({
            //    'ModelMatrix': this.worldMatrix,
            //    'NormalMatrix': APP.Math.computeNormalMatrix3(this.worldMatrix),
            //    'UseTextures': true,
            //    'Sampler': 0
            //}).setDefaultValues();

            ////************** SET UP DRAW TEXTURE **********************//
            //gl.activeTexture(gl.TEXTURE0);
            //gl.bindTexture(gl.TEXTURE_2D, texture);

            //// Draw elements.
            //geometry.draw(gl.TRIANGLES);
            ////Clean up
            ////  APP.clearTexture(texture);
        }

    }

    this.draw = function (args) {
        this._drawUnit(args);

        // Call draw on each children.
        _.invoke(this.children, 'draw', args);

    }

}
    APP.Unit = Compose(APP.Object3D, APP.Events, APP.__Unit);



