// AnimatedEvents
APP.AnimatedEvent = function (params) {
    var gl = this.gl = APP.WebGLContext;
    var defaults = {
        center: vec3.fromValues(0, 0, 0),//Position
        type: "snow storm", // Type of event
        amount: 2,//amount of elements
        elementSize:1,
        scaleX: 1,
        scaleY: 1,
        scaleZ: 1,
        velocity: vec3.fromValues(0, 0, 0),
        acceleration: vec3.fromValues(0, 0, 0),
        lifeSpan: 1,//time before elements die
        texture: null,//used for particle emmiter
        group: new APP.Object3D()
    };

    var _attrs = _.extend(defaults, _.clone(params));

    //Parameters

    var center = _attrs.center;
    var type = _attrs.type;
    var amount = _attrs.amount;
    var elementSize = _attrs.elementSize;
    var scaleX = _attrs.scaleX;
    var scaleY = _attrs.scaleY;
    var scaleZ = _attrs.scaleZ;
    var velocity = _attrs.velocity;
    var acceleration = _attrs.acceleration;
    var lifeSpan = _attrs.lifeSpan;
    var texture = _attrs.texture;
    var group = _attrs.group;

    // * * * * * * * * * * * * * * * * * *   S N O W    S T O R M    I N I T I A L I Z E  * * * * * * * * * * * * * * * * * * * //
    if (type == "snow storm") {
        var group = new APP.Object3D();
        //Create snow flakes
        for (var i = 0; i < amount; i++) {
            var startingHeight = 25;
            var scaleFllakeXY = Math.random();
            flake = new APP.SnowFlake({
                color: [vec4.fromValues(1, 1, 1, 1)], // White
                detaillevel: _.random(2, 6),//amount of iterations (0 is a hollow triangle)
                scaleX: scaleFllakeXY * elementSize,
                scaleY: scaleFllakeXY * elementSize,
            });
            flake.transform.position.xyz = vec3.fromValues(center[0] + (Math.random() * scaleX) / 2,
                                                            Math.random() * startingHeight,
                                                            center[2] + (Math.random() * scaleZ) / 2)
            group.add(flake);
        }

        // scene.add(group);
    }
    // * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////// A N I M A T E //////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    this.animate = function(){

        // * * * * * * * * * * * * * * * * * * *   S N O W    S T O R M   A N I M A T E   * * * * * * * * * * * * * * * * * * * * * * * * //
        if (type == "snow storm") {
            
            _.each(group.children, function (child) {

                //flake rotate
                child.transform.rotation.z += 2 * Math.random() - 4 * Math.random();
                //flake falls
              //  child.transform.position.x += velocity[0] + (acceleration[0] * acceleration[0]) * Math.random() * 0.05;
                child.transform.position.y += velocity[1]; //- (acceleration[1]) * Math.random() * 0.05;
             //   child.transform.position.z += velocity[2] + (acceleration[2] * acceleration[2]) * Math.random() * 0.05;
                //flake dies and is recycled above
                if (child.transform.position.y < -2) {
                    child.transform.position.xyz = vec3.fromValues(center[0] + (Math.random() * scaleX) / 2,
                                                            startingHeight,
                                                            center[2] + (Math.random() * scaleZ) / 2)
                }
            });
        }
        // * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
    }
    
    this.group = group;

}
