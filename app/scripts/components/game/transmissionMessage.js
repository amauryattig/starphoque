//APP.MovingObjectsPositionRegister = [[]];

// TransmissionMessages
APP.__TransmissionMessage = function (params) {
    var gl = this.gl = APP.WebGLContext;
    var defaults = {

        name: "TransmissionMessage",
        position: vec3.fromValues(0, 0, 0),
        velocity: vec3.fromValues(0, 0, 0),
        acceleration: vec3.fromValues(0, 0, 0),
        displayTime: 10,
        geometry : new APP.RectangleStraightCentered(),// rectangle by default
        isUp: false,//down by default
        isOn: true, //is loaded by default       
        camera:this.camera,
    };
    var self = this;
    var _attrs = _.extend(defaults, _.clone(params));

    //Parameters

    this.name = _attrs.name;
    this.position = _attrs.position;
    this.velocity = _attrs.velocity;
    this.acceleration = _attrs.acceleration;
    this.displayTime = _attrs.displayTime;
    //this.geometry = _attrs.geometry;
    this.isUp = _attrs.isUp;
    this.isOn = _attrs.isOn;
    this.camera = _attrs.camera;


    //Local variables
    var increasingFactor = 0;
    var decreasingFactor = 10;

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////// P R O P E R T Y    C H A N G E   F U N C T I O N S /////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // Change image //
    this.changeImage = function (key, type, file) {

        this.geometry.material.texture.textureLocation.key = key;
        this.geometry.material.texture.textureLocation.type = type;
        this.geometry.material.texture.textureLocation.file = file;
    }

    // Reset geometry scale to default //
    this.resetScale = function () {

        this.geometry.width = 3;
        this.geometry.height = 2;

    }

    

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////// A N I M A T E D     F U N C T I O N S //////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //this.animate = function(){
    // display message //
    this.popUp = function () {
        //if(this.position[1]<_attrs.camera.m_cPosition[1]-this.geometry.){
        //    _attrs.groupMesh.transform.position.x += -0.15;
        //}
        
    }




    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////// D R A W  //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Private drawing function.
    this._drawTransmissionMessage = function (args) {

        
        if (this.isFrustumCulled(args.frustum)) {
            //this.draw();
            //var this = this.this;
            //update TransmissionMessage position
            //this.updatedVelocity[0] += this.acceleration[0];
            //this.updatedVelocity[1] += this.acceleration[1];
            //this.updatedVelocity[2] += this.acceleration[2];

            _attrs.geometry.transform.position.x = _attrs.camera.m_cPosition[0];
            _attrs.geometry.transform.position.y = _attrs.camera.m_cPosition[1];
            _attrs.geometry.transform.position.z = _attrs.camera.m_cPosition[2];

            //_attrs.camera.m_cPosition[0] = _attrs.groupMesh.transform.position.x + this.acceleration[0];
            //_attrs.camera.m_cPosition[1] = _attrs.groupMesh.transform.position.y + this.acceleration[1];
            //_attrs.camera.m_cPosition[2] = _attrs.groupMesh.transform.position.z+2+ this.acceleration[2];




            if (this.energy < 100) {//refill energy 
                this.energy += 1;
            }
            
            this.updateWorldMatrix();

        }

    }

    this.draw = function (args) {
        this._drawTransmissionMessage(args);

        // Call draw on each children.
        _.invoke(this.children, 'draw', args);

    }

}
    APP.TransmissionMessage = Compose(APP.Object3D, APP.Events, APP.__TransmissionMessage);



