//Level Loader
//Give it a levelNo and it loads the selected level

APP.LevelLoader = function(params) {
        // parameters//
        var defaults = {
            scene: new APP.Scene(), //Is scene has not been initialized in main, create a new one
            levelNo: 1,
            camera: this.camera,
            mainArwingPosition: vec3.fromValues(0, 0, 0),
            lvlLengthRatio: 0.2,//Can shrink or extend level components

        };

        var _attrs = _.extend(defaults, _.clone(params));
        var levelNo = _attrs.levelNo;
       // this.levelNo = _attrs.levelNo;
        var scene = _attrs.scene;
        var lvlLengthRatio = _attrs.lvlLengthRatio;
        this.mainArwingPosition = _attrs.mainArwingPosition;
        /////////////////
        this.levelNo = levelNo;

    // -+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+-  LEVEL 0 : OPENING SEQUENCE (mountaintopsnow, moutaintopsnow cumbemap) -+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+- //
        if (this.levelNo == 7) {

            // ----------- Define ground and sky ----------- //
            //APP.Base64Images['space']['posz'] = APP.negzsectory;
            //APP.base64Images. = APP.negzsectory;
            var cubemap = new APP.Cubemap({
                base: 'mountaintopsnow'
            });
            //Load Main Arwing
            //var grassRockMountain1left = new APP.Mesh({
            //    //color: vec4.fromValues(1, 0, 1, 1),
            //    strMeshOBJ: APP.ModelTerrainOBJ.meshMajesticMountain["mesh0"],
            //    textureLocation: {
            //        file: 'ModelTerrainOBJ',
            //        type: 'textureMajesticMountain',
            //        key: 'mesh0'
            //    },
            //    //scaleX: 0.0001,
            //    //scaleY: 0.0001,
            //    scaleZ: 10 * lvlLengthRatio,

            //});

            /********* Test mesh avion Amaury *************/

            this.groupMeshArwing1 = new APP.Object3D();
            this.groupMeshArwing2 = new APP.Object3D();
            this.groupMeshArwing3 = new APP.Object3D();
            this.groupMeshArwing4 = new APP.Object3D();
            this.groupRect = new APP.Object3D();

            var mesh0 = new APP.Mesh({
                strMeshOBJ: APP.ModelArwingOBJ.meshArwing["mesh0"],
                textureLocation: {
                    file: 'ModelArwingOBJ',
                    type: 'textureArwing',
                    key: 'mesh0'
                },
            });
            var mesh1 = new APP.Mesh({
                strMeshOBJ: APP.ModelArwingOBJ.meshArwing["mesh1"],
                textureLocation: {
                    file: 'ModelArwingOBJ',
                    type: 'textureArwing',
                    key: 'mesh1'
                },
            });
            var mesh2 = new APP.Mesh({
                strMeshOBJ: APP.ModelArwingOBJ.meshArwing["mesh2"],
                textureLocation: {
                    file: 'ModelArwingOBJ',
                    type: 'textureArwing',
                    key: 'mesh2'
                },
            });

            /////////////
            var mesh01 = new APP.Mesh({
                strMeshOBJ: APP.ModelArwingOBJ.meshArwing["mesh0"],
                textureLocation: {
                    file: 'ModelArwingOBJ',
                    type: 'textureArwing',
                    key: 'mesh0'
                },
            });
            var mesh11 = new APP.Mesh({
                strMeshOBJ: APP.ModelArwingOBJ.meshArwing["mesh1"],
                textureLocation: {
                    file: 'ModelArwingOBJ',
                    type: 'textureArwing',
                    key: 'mesh1'
                },
            });
            var mesh21 = new APP.Mesh({
                strMeshOBJ: APP.ModelArwingOBJ.meshArwing["mesh2"],
                textureLocation: {
                    file: 'ModelArwingOBJ',
                    type: 'textureArwing',
                    key: 'mesh2'
                },
            });

            /////////////
            var mesh02 = new APP.Mesh({
                strMeshOBJ: APP.ModelArwingOBJ.meshArwing["mesh0"],
                textureLocation: {
                    file: 'ModelArwingOBJ',
                    type: 'textureArwing',
                    key: 'mesh0'
                },
            });
            var mesh12 = new APP.Mesh({
                strMeshOBJ: APP.ModelArwingOBJ.meshArwing["mesh1"],
                textureLocation: {
                    file: 'ModelArwingOBJ',
                    type: 'textureArwing',
                    key: 'mesh1'
                },
            });
            var mesh22 = new APP.Mesh({
                strMeshOBJ: APP.ModelArwingOBJ.meshArwing["mesh2"],
                textureLocation: {
                    file: 'ModelArwingOBJ',
                    type: 'textureArwing',
                    key: 'mesh2'
                },
            });

            /////////////
            var mesh03 = new APP.Mesh({
                strMeshOBJ: APP.ModelArwingOBJ.meshArwing["mesh0"],
                textureLocation: {
                    file: 'ModelArwingOBJ',
                    type: 'textureArwing',
                    key: 'mesh0'
                },
            });
            var mesh13 = new APP.Mesh({
                strMeshOBJ: APP.ModelArwingOBJ.meshArwing["mesh1"],
                textureLocation: {
                    file: 'ModelArwingOBJ',
                    type: 'textureArwing',
                    key: 'mesh1'
                },
            });
            var mesh23 = new APP.Mesh({
                strMeshOBJ: APP.ModelArwingOBJ.meshArwing["mesh2"],
                textureLocation: {
                    file: 'ModelArwingOBJ',
                    type: 'textureArwing',
                    key: 'mesh2'
                },
            });

            /////////////

            this.groupMeshArwing1.add(mesh0);
            this.groupMeshArwing1.add(mesh1);
            this.groupMeshArwing1.add(mesh2);
           // this.groupMeshArwing1.transform.rotation.y = 180;
            this.groupMeshArwing1.transform.scale.xyz = vec3.fromValues(0.1, 0.1, 0.1);
            this.groupMeshArwing1.transform.position.y = vec3.fromValues(-55, 0, 0);

            this.groupMeshArwing2.add(mesh01);
            this.groupMeshArwing2.add(mesh11);
            this.groupMeshArwing2.add(mesh21);
           // this.groupMeshArwing2.transform.rotation.y = 180;
            this.groupMeshArwing2.transform.scale.xyz = vec3.fromValues(0.1, 0.1, 0.1);
            this.groupMeshArwing2.transform.position.y = vec3.fromValues(55, 0, 0);

            this.groupMeshArwing3.add(mesh02);
            this.groupMeshArwing3.add(mesh12);
            this.groupMeshArwing3.add(mesh22);
         //   this.groupMeshArwing3.transform.rotation.y = 180;
            this.groupMeshArwing3.transform.scale.xyz = vec3.fromValues(0.1, 0.1, 0.1);
            this.groupMeshArwing4.transform.position.y = vec3.fromValues(0, -55, 0);

            this.groupMeshArwing4.add(mesh03);
            this.groupMeshArwing4.add(mesh13);
            this.groupMeshArwing4.add(mesh23);
          //  this.groupMeshArwing4.transform.rotation.y = 180;
            this.groupMeshArwing4.transform.scale.xyz = vec3.fromValues(0.1, 0.1, 0.1);
            this.groupMeshArwing4.transform.position.y = vec3.fromValues(0, 55, 0);

            var rectCenter1 = new APP.RectangleStraightCenteredGeometry({
                center: vec3.fromValues(0, 0, 0),
                width:3,
                height:2,
                textureLocation: {
                    key: 'msg1',
                    type: 'openingSequence',
                    file: 'Base64TransmissionImages'
                },
            });

            var rectCenter2 = new APP.RectangleStraightCenteredGeometry({
                center: vec3.fromValues(0, 0, 0),
                width: 3,
                height: 2,
                textureLocation: {
                    key: 'msg2',
                    type: 'openingSequence',
                    file: 'Base64TransmissionImages'
                },
            });

            var rectCenter3 = new APP.RectangleStraightCenteredGeometry({
                center: vec3.fromValues(0, 0, 0),
                width: 3,
                height: 2,
                textureLocation: {
                    key: 'msg3',
                    type: 'openingSequence',
                    file: 'Base64TransmissionImages'
                },
            });

            var rectCenter4 = new APP.RectangleStraightCenteredGeometry({
                center: vec3.fromValues(0, 0, 0),
                width: 3,
                height: 2,
                textureLocation: {
                    key: 'msg4',
                    type: 'openingSequence',
                    file: 'Base64TransmissionImages'
                },
            });

            var rectCenter5 = new APP.RectangleStraightCenteredGeometry({
                center: vec3.fromValues(0, 0, 0),
                width: 3,
                height: 2,
                textureLocation: {
                    key: 'msg5',
                    type: 'openingSequence',
                    file: 'Base64TransmissionImages'
                },
            });

            var rectCenter6 = new APP.RectangleStraightCenteredGeometry({
                center: vec3.fromValues(0, 0, 0),
                width: 3,
                height: 2,
                textureLocation: {
                    key: 'msg6',
                    type: 'openingSequence',
                    file: 'Base64TransmissionImages'
                },
            });

            var rectCenter7 = new APP.RectangleStraightCenteredGeometry({
                center: vec3.fromValues(0, 0, 0),
                width: 3,
                height: 2,
                textureLocation: {
                    key: 'msg7',
                    type: 'openingSequence',
                    file: 'Base64TransmissionImages'
                },
            });

            var rectCenter8 = new APP.RectangleStraightCenteredGeometry({
                center: vec3.fromValues(0, 0, 0),
                width: 3,
                height: 2,
                textureLocation: {
                    key: 'msg8',
                    type: 'openingSequence',
                    file: 'Base64TransmissionImages'
                },
            });

            var rectCenter9 = new APP.RectangleStraightCenteredGeometry({
                center: vec3.fromValues(0, 0, 0),
                width: 3,
                height: 2,
                textureLocation: {
                    key: 'msg9',
                    type: 'openingSequence',
                    file: 'Base64TransmissionImages'
                },
            });

            var rectCenter10 = new APP.RectangleStraightCenteredGeometry({
                center: vec3.fromValues(0, 0, 0),
                width: 3,
                height: 2,
                textureLocation: {
                    key: 'msg10',
                    type: 'openingSequence',
                    file: 'Base64TransmissionImages'
                },
            });

            var rectCenter11 = new APP.RectangleStraightCenteredGeometry({
                center: vec3.fromValues(0, 0, 0),
                width: 3,
                height: 2,
                textureLocation: {
                    key: 'msg11',
                    type: 'openingSequence',
                    file: 'Base64TransmissionImages'
                },
            });


            var rectCenter12 = new APP.RectangleStraightCenteredGeometry({
                center: vec3.fromValues(0, 0, 0),
                width: 3,
                height: 2,
                textureLocation: {
                    key: 'msg12',
                    type: 'openingSequence',
                    file: 'Base64TransmissionImages'
                },
            });


            var rectCenter13 = new APP.RectangleStraightCenteredGeometry({
                center: vec3.fromValues(0, 0, 0),
                width: 3,
                height: 2,
                textureLocation: {
                    key: 'msg13',
                    type: 'openingSequence',
                    file: 'Base64TransmissionImages'
                },
            });


            this.groupRect.add(rectCenter1);
            this.groupRect.add(rectCenter2);
            this.groupRect.add(rectCenter3);
            this.groupRect.add(rectCenter4);
            this.groupRect.add(rectCenter5);
            this.groupRect.add(rectCenter6);
            this.groupRect.add(rectCenter7);
            this.groupRect.add(rectCenter8);
            this.groupRect.add(rectCenter9);
            this.groupRect.add(rectCenter10);
            this.groupRect.add(rectCenter11);
            this.groupRect.add(rectCenter12);
            this.groupRect.add(rectCenter13);
            this.groupRect.transform.position.y = -100;
            this.groupRect.transform.scale.xyz = vec3.fromValues(5,2,0);
            timeFactor = 0.6;
            offset = 100;
            maxYMessage = 0.5;

            //scene.add(this.groupMeshArwing1);
            //scene.add(this.groupMeshArwing2);
            //scene.add(this.groupMeshArwing3);
            //scene.add(this.groupMeshArwing4);
            scene.add(this.groupRect);

            //scene.add(APP.unitMainArwing);
            // scene.ambientLightColor = vec3.fromValues(0.2, 0.4, 0.4);
            scene.cubemap = cubemap;

            APP.openingAnimateOn = true;

            APP.animateArwings = function (increasingFactor) {
                console.log(increasingFactor);


                //if (increasingFactor <= 500 * timeFactor) {

                //    if (this.groupMeshArwing1.transform.position.x < 1) {
                //        this.groupMeshArwing1.transform.position.x += 0.1;
                //    }

                //    if (this.groupMeshArwing2.transform.position.x > 1) {
                //        this.groupMeshArwing2.transform.position.x += -0.1;
                //    }

                //    if (this.groupMeshArwing3.transform.position.y < 1) {
                //        this.groupMeshArwing3.transform.position.y += 0.1;
                //    }

                //    if (this.groupMeshArwing4.transform.position.y > 1) {
                //        this.groupMeshArwing4.transform.position.y += -0.1;
                //    }



                //}
                ////////////////////////
                if (increasingFactor > 550 * timeFactor + offset * 1 && increasingFactor < 600 * timeFactor + offset * 1 && this.groupRect.children[0].transform.position.y < maxYMessage) {
                    this.groupRect.children[0].transform.position.y += 0.5;
                }

                if (increasingFactor > 700 * timeFactor + offset * 1 && increasingFactor < 750 * timeFactor + offset * 1) {
                    this.groupRect.children[0].transform.position.y += -0.5;
                }
                ///////////////////////////

                if (increasingFactor > 550 * timeFactor + offset * 2 && increasingFactor < 600 * timeFactor + offset * 2 && this.groupRect.children[1].transform.position.y < maxYMessage) {
                    this.groupRect.children[1].transform.position.y += 0.5;
                }

                if (increasingFactor > 700 * timeFactor + offset * 2 && increasingFactor < 750 * timeFactor + offset * 2) {
                    this.groupRect.children[1].transform.position.y += -0.5;
                }
                ///////////////////////////

                if (increasingFactor > 550 * timeFactor + offset * 3 && increasingFactor < 600 * timeFactor + offset * 3 && this.groupRect.children[2].transform.position.y < maxYMessage) {
                    this.groupRect.children[2].transform.position.y += 0.5;
                }

                if (increasingFactor > 700 * timeFactor + offset * 3 && increasingFactor < 750 * timeFactor + offset * 3) {
                    this.groupRect.children[2].transform.position.y += -0.5;
                }
                ///////////////////////////

                if (increasingFactor > 550 * timeFactor + offset * 4 && increasingFactor < 600 * timeFactor + offset * 4 && this.groupRect.children[3].transform.position.y < maxYMessage) {
                    this.groupRect.children[3].transform.position.y += 0.5;
                }

                if (increasingFactor > 700 * timeFactor + offset * 4 && increasingFactor < 750 * timeFactor + offset * 4) {
                    this.groupRect.children[3].transform.position.y += -0.5;
                }
                ///////////////////////////

                if (increasingFactor > 550 * timeFactor + offset * 5 && increasingFactor < 600 * timeFactor + offset * 5 && this.groupRect.children[4].transform.position.y < maxYMessage) {
                    this.groupRect.children[4].transform.position.y += 0.5;
                }

                if (increasingFactor > 700 * timeFactor + offset * 5 && increasingFactor < 750 * timeFactor + offset * 5) {
                    this.groupRect.children[4].transform.position.y += -0.5;
                }
                ///////////////////////////

                if (increasingFactor > 550 * timeFactor + offset * 6 && increasingFactor < 600 * timeFactor + offset * 6 && this.groupRect.children[5].transform.position.y < maxYMessage) {
                    this.groupRect.children[5].transform.position.y += 0.5;
                }

                if (increasingFactor > 700 * timeFactor + offset * 6 && increasingFactor < 750 * timeFactor + offset * 6) {
                    this.groupRect.children[5].transform.position.y += -0.5;
                }
                ///////////////////////////

                if (increasingFactor > 550 * timeFactor + offset * 7 && increasingFactor < 600 * timeFactor + offset * 7 && this.groupRect.children[6].transform.position.y < maxYMessage) {
                    this.groupRect.children[6].transform.position.y += 0.5;
                }

                if (increasingFactor > 700 * timeFactor + offset * 7 && increasingFactor < 750 * timeFactor + offset * 7) {
                    this.groupRect.children[6].transform.position.y += -0.5;
                }
                ///////////////////////////

                if (increasingFactor > 550 * timeFactor + offset * 8 && increasingFactor < 600 * timeFactor + offset * 8 && this.groupRect.children[7].transform.position.y < maxYMessage) {
                    this.groupRect.children[7].transform.position.y += 0.5;
                }

                if (increasingFactor > 700 * timeFactor + offset * 8 && increasingFactor < 750 * timeFactor + offset * 8) {
                    this.groupRect.children[7].transform.position.y += -0.5;
                }
                ///////////////////////////

                if (increasingFactor > 550 * timeFactor + offset * 9 && increasingFactor < 600 * timeFactor + offset * 9 && this.groupRect.children[8].transform.position.y < maxYMessage) {
                    this.groupRect.children[8].transform.position.y += 0.5;
                }

                if (increasingFactor > 700 * timeFactor + offset * 9 && increasingFactor < 750 * timeFactor + offset * 9) {
                    this.groupRect.children[8].transform.position.y += -0.5;
                }
                ///////////////////////////

                if (increasingFactor > 550 * timeFactor + offset * 10 && increasingFactor < 600 * timeFactor + offset * 10 && this.groupRect.children[9].transform.position.y < maxYMessage) {
                    this.groupRect.children[9].transform.position.y += 0.5;
                }

                if (increasingFactor > 700 * timeFactor + offset * 10 && increasingFactor < 750 * timeFactor + offset * 10) {
                    this.groupRect.children[9].transform.position.y += -0.5;
                }
                ///////////////////////////

                if (increasingFactor > 550 * timeFactor + offset * 11 && increasingFactor < 600 * timeFactor + offset * 11 && this.groupRect.children[10].transform.position.y < maxYMessage) {
                    this.groupRect.children[10].transform.position.y += 0.5;
                }

                if (increasingFactor > 700 * timeFactor + offset * 11 && increasingFactor < 750 * timeFactor + offset * 11) {
                    this.groupRect.children[10].transform.position.y += -0.5;
                }
                ///////////////////////////

                if (increasingFactor > 550 * timeFactor + offset * 12 && increasingFactor < 600 * timeFactor + offset * 12 && this.groupRect.children[11].transform.position.y < maxYMessage) {
                    this.groupRect.children[11].transform.position.y += 0.5;
                }

                if (increasingFactor > 700 * timeFactor + offset * 12 && increasingFactor < 750 * timeFactor + offset * 12) {
                    this.groupRect.children[11].transform.position.y += -0.5;
                }
                ///////////////////////////
                if (increasingFactor > 550 * timeFactor + offset * 13 && increasingFactor < 600 * timeFactor + offset * 12 && this.groupRect.children[12].transform.position.y < maxYMessage) {
                    this.groupRect.children[12].transform.position.y += 0.5;
                }

                if (increasingFactor > 700 * timeFactor + offset * 13 && increasingFactor < 750 * timeFactor + offset * 12) {
                    this.groupRect.children[12].transform.position.y += -0.5;
                }
                ///////////////////////////







            };

            //var initialVelocityMainArwing = APP.originalVelocity;
            //var initialAccelerationMainArwing = APP.originalAcceleration;
            //APP.unitMainArwing = new APP.Unit({
            //    name: "Phoque",
            //    position: vec3.fromValues(0, 0, 0),//Position
            //    type: "arwing", // Type of unit
            //    //health: 100,//amount of elements
            //    //playerOwner: 1,
            //    groupMesh: APP.groupMeshMainArwing,
            //    //projectileTypes: ["defaultProjectile"],
            //    //damage: 10,
            //    //energy:100,
            //    velocity: initialVelocityMainArwing,
            //    acceleration: initialAccelerationMainArwing,
            //    //   scene: scene,
            //    camera: _attrs.camera,
            //});
            //APP.unitMainArwing.position = this.mainArwingPosition;


    
            




            //************** LOAD DECORATION INTO groupGround ***********************//





        }
        

    // -+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+-  LEVEL 1 : CORNERIA (Grass and water ground, sky cubemap) -+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+- //
        if (this.levelNo == 1) {

            scene.children.length = 0;

            //Load Main Arwing
            //var grassRockMountain1left = new APP.Mesh({
            //    //color: vec4.fromValues(1, 0, 1, 1),
            //    strMeshOBJ: APP.ModelTerrainOBJ.meshMajesticMountain["mesh0"],
            //    textureLocation: {
            //        file: 'ModelTerrainOBJ',
            //        type: 'textureMajesticMountain',
            //        key: 'mesh0'
            //    },
            //    //scaleX: 0.0001,
            //    //scaleY: 0.0001,
            //    scaleZ: 10 * lvlLengthRatio,

            //});

            /********* Test mesh avion Amaury *************/

            APP.groupMeshMainArwing = new APP.Object3D();


            var mesh0 = new APP.Mesh({
                strMeshOBJ: APP.ModelArwingOBJ.meshArwing["mesh0"],
                textureLocation: {
                    file: 'ModelArwingOBJ',
                    type: 'textureArwing',
                    key: 'mesh0'
                },
            });
            var mesh1 = new APP.Mesh({
                strMeshOBJ: APP.ModelArwingOBJ.meshArwing["mesh1"],
                textureLocation: {
                    file: 'ModelArwingOBJ',
                    type: 'textureArwing',
                    key: 'mesh1'
                },
            });
            var mesh2 = new APP.Mesh({
                strMeshOBJ: APP.ModelArwingOBJ.meshArwing["mesh2"],
                textureLocation: {
                    file: 'ModelArwingOBJ',
                    type: 'textureArwing',
                    key: 'mesh2'
                },
            });

            APP.groupMeshMainArwing.add(mesh0);
            APP.groupMeshMainArwing.add(mesh1);
            APP.groupMeshMainArwing.add(mesh2);
            APP.groupMeshMainArwing.transform.rotation.y = 180;
            APP.groupMeshMainArwing.transform.scale.xyz = vec3.fromValues(0.1, 0.1, 0.1);

            var initialVelocityMainArwing = APP.originalVelocity;
            var initialAccelerationMainArwing = APP.originalAcceleration;
            APP.unitMainArwing = new APP.Unit({
                name : "Phoque",
                position: vec3.fromValues(0, 0, 0),//Position
                type: "arwing", // Type of unit
                //health: 100,//amount of elements
                //playerOwner: 1,
                groupMesh: APP.groupMeshMainArwing,
                //projectileTypes: ["defaultProjectile"],
                //damage: 10,
                //energy:100,
                velocity: initialVelocityMainArwing,
                acceleration: initialAccelerationMainArwing,
                //   scene: scene,
                camera: _attrs.camera,
            });
            APP.unitMainArwing.position = this.mainArwingPosition;


            // ----------- Define ground and sky ----------- //
            //APP.Base64Images['space']['posz'] = APP.negzsectory;
            //APP.base64Images. = APP.negzsectory;
            var cubemap = new APP.Cubemap({
                base: 'sky'
            });

            // ----------- Define decorating fixed elements ----------- //

            var groupGround = new APP.Object3D();

            //Water Base Flat
            var waterMaterial = new APP.WaterMaterial({
                texture: new APP.Texture({
                    textureLocation: {
                        key: 'water',
                        type: 'groundtexture',
                        file: 'Base64GroundTexture'
                    },
                    isRepeating: true
                })
            });

            // var corner1 = vec3.fromValues(-100, -1, 20 * lvlLengthRatio);
            // var corner2 = vec3.fromValues(-100, -1, -3000 * lvlLengthRatio);
            // var corner3 = vec3.fromValues(100, -1, -3000 * lvlLengthRatio);
            // var corner4 = vec3.fromValues(100, -1, 20 * lvlLengthRatio);

            // var corner1 = vec3.fromValues(-100, -1, 0);
            // var corner2 = vec3.fromValues(-100, -1, -60);
            // var corner3 = vec3.fromValues(100, -1, 0);
            // var corner4 = vec3.fromValues(100, -1, -60);

            var seaLevel = -1.0;
            var levelWidth = 200;
            var levelLength = 3000;
            var levelToBackLength = 10;

            var corner1 = vec3.fromValues(-levelWidth/2, seaLevel, levelToBackLength);
            var corner2 = vec3.fromValues(-levelWidth/2, seaLevel, -levelLength * lvlLengthRatio);
            var corner3 = vec3.fromValues(levelWidth/2, seaLevel, levelToBackLength);
            var corner4 = vec3.fromValues(levelWidth/2, seaLevel, -levelLength * lvlLengthRatio);

            coonsEquation = new APP.Math.Surfaces.CoonsSurface({
                C1: new APP.Math.Curves.BezierCubic({
                    // Control points.
                    P0: corner1,
                    P1: corner1,
                    P2: corner2,
                    P3: corner2
                }),
                C2: new APP.Math.Curves.BezierCubic({
                    // Control points.
                    P0: corner3,
                    P1: corner3,
                    P2: corner4,
                    P3: corner4
                }),
                C3: new APP.Math.Curves.BezierCubic({
                    // Control points.
                    P0: corner1,
                    P1: corner1,
                    P2: corner3,
                    P3: corner3
                }),
                C4: new APP.Math.Curves.BezierCubic({
                    // Control points.
                    P0: corner2,
                    P1: corner2,
                    P2: corner4,
                    P3: corner4
                })
            });

            coons = new APP.ParametricSurfaceGeometry({
                equation: coonsEquation,
                slices: 200,
                stacks: 200,
                // color: vec4.fromValues(105 / 255, 180 / 255, 200 / 255, 1.0), // Red.
                // drawControlPoints: false,
                // controlPointsColor: vec4.fromValues(0.0, 1.0, 0.0, 1.0), // Green.
                material: waterMaterial
            });

            // coons.transform.rotation.x = 90.0;

            var waterRectangle = coons;

            // var waterRectangle = new APP.RectangleWithCoordsGeometry({
            //     v0: vec3.fromValues(-100, -1, 20 * lvlLengthRatio),
            //     v1: vec3.fromValues(-100, -1, -3000 * lvlLengthRatio),
            //     v2: vec3.fromValues(100, -1, -3000 * lvlLengthRatio),
            //     v3: vec3.fromValues(100, -1, 20 * lvlLengthRatio),
            //     material: waterMaterial,
            //     isInXZplane: true,
            //     isRepeatingTexture: true
            // });

            //Grass Param Surface 2 times
            var grassSurface1 = new APP.ParametricTerrainBuilder({
                start: (-500 * lvlLengthRatio), //Z initial
                finish: (-1677 * lvlLengthRatio), //Z final
                width: 200, // X interval
                height: 0.6, //Y level
                detailLevel: 2, //Defines the amount of rectangles needed for the plane. More rectangles = better texture but less performance.
                //color: defaultColor, // No color by default.
                //slices: 2,
                //stacks: 2,
                //controlPointsColor: vec4.fromValues(1.0, 0.0, 0.0, 1.0), //red.
                maxYLevel: 1,
                minYLevel: -1,
                randomOverrride: 0.5,
                textureLocation: {
                    key: 'grass',
                    type: 'groundtexture',
                    file: 'Base64GroundTexture'
                }
                //strTextureImage: APP.Base64GroundTexture.groundtexture["grass"]
            });

            var grassSurface2 = new APP.ParametricTerrainBuilder({
                start: (-1925 * lvlLengthRatio), //Z initial
                finish: (-2490 * lvlLengthRatio), //Z final
                width: 200, // X interval
                height: 0.6, //Y level
                detailLevel: 2, //Defines the amount of rectangles needed for the plane. More rectangles = better texture but less performance.
                //color: defaultColor, // No color by default.
                //slices: 2,
                //stacks: 2,
                //controlPointsColor: vec4.fromValues(1.0, 0.0, 0.0, 1.0), //red.
                maxYLevel: 1,
                minYLevel: -1,
                randomOverrride: 0.5,
                textureLocation: {
                    key: 'grass',
                    type: 'groundtexture',
                    file: 'Base64GroundTexture'
                }
                // strTextureImage: APP.Base64GroundTexture.groundtexture["grass"]
            });

            //Entrance first patch
            var grassSurface3 = new APP.ParametricTerrainBuilder({
                start: (-1925 * lvlLengthRatio), //Z initial
                finish: (-1975 * lvlLengthRatio), //Z final
                width: 400, // X interval
                height: -3.5, //Y level
                detailLevel: 0, //Defines the amount of rectangles needed for the plane. More rectangles = better texture but less performance.
                //color: defaultColor, // No color by default.
                //slices: 2,
                //stacks: 2,
                //controlPointsColor: vec4.fromValues(1.0, 0.0, 0.0, 1.0), //red.
                maxYLevel: 1.5,
                minYLevel: 1.0,
                randomOverrride: 1,
                textureLocation: {
                    key: 'grass',
                    type: 'groundtexture',
                    file: 'Base64GroundTexture'
                }
                // strTextureImage: APP.Base64GroundTexture.groundtexture["grass"]
            });

            //Entrance 2nd patch
            var grassSurface4 = new APP.ParametricTerrainBuilder({
                start: (-500 * lvlLengthRatio), //Z initial
                finish: (-550 * lvlLengthRatio), //Z final
                width: 400, // X interval
                height: -3, //Y level
                detailLevel: 0, //Defines the amount of rectangles needed for the plane. More rectangles = better texture but less performance.
                //color: defaultColor, // No color by default.
                //slices: 2,
                //stacks: 2,
                //controlPointsColor: vec4.fromValues(1.0, 0.0, 0.0, 1.0), //red.
                maxYLevel: 1.5,
                minYLevel: 1.0,
                randomOverrride: 1,
                textureLocation: {
                    key: 'grass',
                    type: 'groundtexture',
                    file: 'Base64GroundTexture'
                }
                // strTextureImage: APP.Base64GroundTexture.groundtexture["grass"]
            });

            //Rock Param Surface 4 times (2 on each side for left-right barriers while over grass)

            //Land chunk 1
            var rockSurface1left = new APP.ParametricTerrainBuilder({
                start: (-500 * lvlLengthRatio), //Z initial
                finish: (-1677 * lvlLengthRatio), //Z final
                width: 30, // X interval
                height: 2, //Y level
                detailLevel: 3, //Defines the amount of rectangles needed for the plane. More rectangles = better texture but less performance.
                //color: defaultColor, // No color by default.
                slices: 4,
                stacks: 4,
                //controlPointsColor: vec4.fromValues(1.0, 0.0, 0.0, 1.0), //red.
                maxYLevel: 5,
                minYLevel: -5,
                randomOverrride: 1,
                textureLocation: {
                    key: 'rock',
                    type: 'groundtexture',
                    file: 'Base64GroundTexture'
                }
                //strTextureImage: APP.Base64GroundTexture.groundtexture["rock"]
            });
            rockSurface1left.transform.position.xyz = vec3.fromValues(-40, rockSurface1left.transform.position.xyz[1], rockSurface1left.transform.position.xyz[2]); //move left

            var rockSurface1right = new APP.ParametricTerrainBuilder({
                start: -500 * lvlLengthRatio, //Z initial
                finish: -1677 * lvlLengthRatio, //Z final
                width: 30, // X interval
                height: 2, //Y level
                detailLevel: 3, //Defines the amount of rectangles needed for the plane. More rectangles = better texture but less performance.
                //color: defaultColor, // No color by default.
                slices: 4,
                stacks: 4,
                //controlPointsColor: vec4.fromValues(1.0, 0.0, 0.0, 1.0), //red.
                maxYLevel: 5,
                minYLevel: -5,
                randomOverrride: 1,
                textureLocation: {
                    key: 'rock',
                    type: 'groundtexture',
                    file: 'Base64GroundTexture'
                }
                // strTextureImage: APP.Base64GroundTexture.groundtexture["rock"]
            });
            rockSurface1right.transform.position.xyz = vec3.fromValues(40, rockSurface1right.transform.position.xyz[1], rockSurface1right.transform.position.xyz[2]); //move right

            //Land chunk 2
            var rockSurface2left = new APP.ParametricTerrainBuilder({
                start: -1925 * lvlLengthRatio, //Z initial
                finish: -2490 * lvlLengthRatio, //Z final
                width: 30, // X interval
                height: 2, //Y level
                detailLevel: 3, //Defines the amount of rectangles needed for the plane. More rectangles = better texture but less performance.
                //color: defaultColor, // No color by default.
                slices: 4,
                stacks: 4,
                //controlPointsColor: vec4.fromValues(1.0, 0.0, 0.0, 1.0), //red.
                maxYLevel: 5,
                minYLevel: -5,
                randomOverrride: 1,
                textureLocation: {
                    key: 'rock',
                    type: 'groundtexture',
                    file: 'Base64GroundTexture'
                }
                // strTextureImage: APP.Base64GroundTexture.groundtexture["rock"]
            });
            rockSurface2left.transform.position.xyz = vec3.fromValues(-40, rockSurface2left.transform.position.xyz[1], rockSurface2left.transform.position.xyz[2]); //move left

            var rockSurface2right = new APP.ParametricTerrainBuilder({
                start: -1925 * lvlLengthRatio, //Z initial
                finish: -2490 * lvlLengthRatio, //Z final
                width: 30, // X interval
                height: 2, //Y level
                detailLevel: 3, //Defines the amount of rectangles needed for the plane. More rectangles = better texture but less performance.
                //color: defaultColor, // No color by default.
                slices: 4,
                stacks: 4,
                //controlPointsColor: vec4.fromValues(1.0, 0.0, 0.0, 1.0), //red.
                maxYLevel: 5,
                minYLevel: -5,
                randomOverrride: 1,
                textureLocation: {
                    key: 'rock',
                    type: 'groundtexture',
                    file: 'Base64GroundTexture'
                }
                // strTextureImage: APP.Base64GroundTexture.groundtexture["rock"]
            });
            rockSurface2right.transform.position.xyz = vec3.fromValues(40, rockSurface2right.transform.position.xyz[1], rockSurface2right.transform.position.xyz[2]); //move right

            //Rock Param Surface 4 times (Underneath the top 4 to hide imperfections)

            //Land chunk 1
            var underrockSurface1left = new APP.ParametricTerrainBuilder({
                start: (-490 * lvlLengthRatio), //Z initial
                finish: (-1687 * lvlLengthRatio), //Z final
                width: 30, // X interval
                height: -5, //Y level
                detailLevel: 0, //Defines the amount of rectangles needed for the plane. More rectangles = better texture but less performance.
                //color: defaultColor, // No color by default.
                slices: 4,
                stacks: 4,
                //controlPointsColor: vec4.fromValues(1.0, 0.0, 0.0, 1.0), //red.
                maxYLevel: 5,
                minYLevel: 0,
                randomOverrride: 0,
                textureLocation: {
                    key: 'rock',
                    type: 'groundtexture',
                    file: 'Base64GroundTexture'
                }
                // strTextureImage: APP.Base64GroundTexture.groundtexture["rock"]
            });
            underrockSurface1left.transform.position.xyz = vec3.fromValues(-40, rockSurface1left.transform.position.xyz[1], rockSurface1left.transform.position.xyz[2]); //move left

            var underrockSurface1right = new APP.ParametricTerrainBuilder({
                start: -490 * lvlLengthRatio, //Z initial
                finish: -1687 * lvlLengthRatio, //Z final
                width: 30, // X interval
                height: -5, //Y level
                detailLevel: 0, //Defines the amount of rectangles needed for the plane. More rectangles = better texture but less performance.
                //color: defaultColor, // No color by default.
                slices: 4,
                stacks: 4,
                //controlPointsColor: vec4.fromValues(1.0, 0.0, 0.0, 1.0), //red.
                maxYLevel: 5,
                minYLevel: 0,
                randomOverrride: 0,
                textureLocation: {
                    key: 'rock',
                    type: 'groundtexture',
                    file: 'Base64GroundTexture'
                }
                // strTextureImage: APP.Base64GroundTexture.groundtexture["rock"]
            });
            underrockSurface1right.transform.position.xyz = vec3.fromValues(40, rockSurface1right.transform.position.xyz[1], rockSurface1right.transform.position.xyz[2]); //move right

            //Land chunk 2
            var underrockSurface2left = new APP.ParametricTerrainBuilder({
                start: -1915 * lvlLengthRatio, //Z initial
                finish: -2500 * lvlLengthRatio, //Z final
                width: 30, // X interval
                height: -5, //Y level
                detailLevel: 0, //Defines the amount of rectangles needed for the plane. More rectangles = better texture but less performance.
                //color: defaultColor, // No color by default.
                slices: 4,
                stacks: 4,
                //controlPointsColor: vec4.fromValues(1.0, 0.0, 0.0, 1.0), //red.
                maxYLevel: 5,
                minYLevel: 0,
                randomOverrride: 0,
                textureLocation: {
                    key: 'rock',
                    type: 'groundtexture',
                    file: 'Base64GroundTexture'
                }
                // strTextureImage: APP.Base64GroundTexture.groundtexture["rock"]
            });
            underrockSurface2left.transform.position.xyz = vec3.fromValues(-40, rockSurface2left.transform.position.xyz[1], rockSurface2left.transform.position.xyz[2]); //move left

            var underrockSurface2right = new APP.ParametricTerrainBuilder({
                start: -1915 * lvlLengthRatio, //Z initial
                finish: -2500 * lvlLengthRatio, //Z final
                width: 30, // X interval
                height: -5, //Y level
                detailLevel: 0, //Defines the amount of rectangles needed for the plane. More rectangles = better texture but less performance.
                //color: defaultColor, // No color by default.
                slices: 4,
                stacks: 4,
                //controlPointsColor: vec4.fromValues(1.0, 0.0, 0.0, 1.0), //red.
                maxYLevel: 5,
                minYLevel: 0,
                randomOverrride: 0,
                textureLocation: {
                    key: 'rock',
                    type: 'groundtexture',
                    file: 'Base64GroundTexture'
                }
                // strTextureImage: APP.Base64GroundTexture.groundtexture["rock"]
            });
            underrockSurface2right.transform.position.xyz = vec3.fromValues(40, rockSurface2right.transform.position.xyz[1], rockSurface2right.transform.position.xyz[2]); //move right

            //Small grass/rock mountains  2 times (first valley)
            var grassRockMountain1left = new APP.Mesh({
                //color: vec4.fromValues(1, 0, 1, 1),
                strMeshOBJ: APP.ModelTerrainOBJ.meshMajesticMountain["mesh0"],
                textureLocation: {
                    file: 'ModelTerrainOBJ',
                    type: 'textureMajesticMountain',
                    key: 'mesh0'
                },
                //scaleX: 0.0001,
                //scaleY: 0.0001,
                scaleZ: 10 * lvlLengthRatio,

            });
            grassRockMountain1left.transform.position.xyz = vec3.fromValues(-15, -2, -500 * lvlLengthRatio); //move left

            var grassRockMountain1right = new APP.Mesh({
                //color: vec4.fromValues(1, 0, 1, 1),
                strMeshOBJ: APP.ModelTerrainOBJ.meshMajesticMountain["mesh0"],
                textureLocation: {
                    file: 'ModelTerrainOBJ',
                    type: 'textureMajesticMountain',
                    key: 'mesh0'
                },
                //scaleX: 0.0001,
                //scaleY: 0.0001,
                scaleZ: 10 * lvlLengthRatio,

            });
            grassRockMountain1right.transform.position.xyz = vec3.fromValues(15, -2, -500 * lvlLengthRatio);
            grassRockMountain1right.transform.rotation.y = 180;


            // random city

            // TTTTTTTTTTTTTTTT    TEXTURE LOAD   TTTTTTTTTTTTTTTTTTTTTT//
            var textureCity = new APP.Texture({

                textureLocation: {
                    key: 'skyscraper1',
                    type: 'groundtexture',
                    file: 'Base64GroundTexture'
                }

            });

            // var textureCity = textureLoader.initTexture();
            // TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT//
            // console.log(textureCity);

            var cptmax = _.random(90, 100);

            var buildingGeometry = new APP.BoxCenteredGeometry().geometry;

            for (var i = 0; i < cptmax; i++) {
                var boxcentered = new APP.BoxCenteredGeometry({
                    geometry: buildingGeometry, // Reuse geometry.
                    center: vec3.fromValues(_.random(-20, 20), 0, _.random(-750, -1500) * lvlLengthRatio),
                    width: Math.random() + 1,
                    height: _.random(3, 7),
                    depth: Math.random() + 1,
                    texture: textureCity
                });
                groupGround.add(boxcentered);
            }

            //************** LOAD DECORATION INTO groupGround ***********************//

            groupGround.add(waterRectangle);
            groupGround.add(grassSurface1);
            groupGround.add(grassSurface2);
            groupGround.add(grassSurface3);
            groupGround.add(grassSurface4);
            groupGround.add(rockSurface1left);
            groupGround.add(rockSurface1right);
            groupGround.add(rockSurface2left);
            groupGround.add(rockSurface2right);
            groupGround.add(underrockSurface1left);
            groupGround.add(underrockSurface1right);
            groupGround.add(underrockSurface2left);
            groupGround.add(underrockSurface2right);
            groupGround.add(grassRockMountain1left);
            groupGround.add(grassRockMountain1right);


            scene.add(groupGround);
            scene.add(APP.unitMainArwing);
            scene.cubemap = cubemap;
            
        }
    // -+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+-  LEVEL 2 : ASSTEROID FIELD (space, space cumbemap) -+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+- //
        if (this.levelNo == 2) {
            //Load Main Arwing
            //var grassRockMountain1left = new APP.Mesh({
            //    //color: vec4.fromValues(1, 0, 1, 1),
            //    strMeshOBJ: APP.ModelTerrainOBJ.meshMajesticMountain["mesh0"],
            //    textureLocation: {
            //        file: 'ModelTerrainOBJ',
            //        type: 'textureMajesticMountain',
            //        key: 'mesh0'
            //    },
            //    //scaleX: 0.0001,
            //    //scaleY: 0.0001,
            //    scaleZ: 10 * lvlLengthRatio,

            //});

            /********* Test mesh avion Amaury *************/

            //APP.groupMeshMainArwing = new APP.Object3D();


            //var mesh0 = new APP.Mesh({
            //    strMeshOBJ: APP.ModelArwingOBJ.meshArwing["mesh0"],
            //    textureLocation: {
            //        file: 'ModelArwingOBJ',
            //        type: 'textureArwing',
            //        key: 'mesh0'
            //    },
            //});
            //var mesh1 = new APP.Mesh({
            //    strMeshOBJ: APP.ModelArwingOBJ.meshArwing["mesh1"],
            //    textureLocation: {
            //        file: 'ModelArwingOBJ',
            //        type: 'textureArwing',
            //        key: 'mesh1'
            //    },
            //});
            //var mesh2 = new APP.Mesh({
            //    strMeshOBJ: APP.ModelArwingOBJ.meshArwing["mesh2"],
            //    textureLocation: {
            //        file: 'ModelArwingOBJ',
            //        type: 'textureArwing',
            //        key: 'mesh2'
            //    },
            //});

            //APP.groupMeshMainArwing.add(mesh0);
            //APP.groupMeshMainArwing.add(mesh1);
            //APP.groupMeshMainArwing.add(mesh2);
            //APP.groupMeshMainArwing.transform.rotation.y = 180;
            //APP.groupMeshMainArwing.transform.scale.xyz = vec3.fromValues(0.1, 0.1, 0.1);

            //var initialVelocityMainArwing = APP.originalVelocity;
            //var initialAccelerationMainArwing = APP.originalAcceleration;
            //APP.unitMainArwing = new APP.Unit({
            //    name: "Phoque",
            //    position: vec3.fromValues(0, 0, 0),//Position
            //    type: "arwing", // Type of unit
            //    //health: 100,//amount of elements
            //    //playerOwner: 1,
            //    groupMesh: APP.groupMeshMainArwing,
            //    //projectileTypes: ["defaultProjectile"],
            //    //damage: 10,
            //    //energy:100,
            //    velocity: initialVelocityMainArwing,
            //    acceleration: initialAccelerationMainArwing,
            //    //   scene: scene,
            //    camera: _attrs.camera,
            //});
            //APP.unitMainArwing.position = this.mainArwingPosition;


            // ----------- Define ground and sky ----------- //
            //APP.Base64Images['space']['posz'] = APP.negzsectory;
            //APP.base64Images. = APP.negzsectory;
            var cubemap = new APP.Cubemap({
                base: 'space'
            });

            // ----------- Define decorating fixed elements ----------- //

          
            //With colors and center
            var groupTestColors = new APP.Object3D();
            //primitives geo
            var dode = new APP.DodecahedronGeometry({
                center: vec3.fromValues(-4, 0, 2),
                textureLocation: {
                    key: 'alienrock2',
                    type: 'groundtexture',
                    file: 'Base64GroundTexture'
                },
            });
            var tetra = new APP.TetrahedronGeometry({
                center: vec3.fromValues(-3, 1, 2),
                textureLocation: {
                    key: 'cosmicenergy',
                    type: 'groundtexture',
                    file: 'Base64GroundTexture'
                },
            });
            var octo = new APP.OctahedronGeometry({
                center: vec3.fromValues(-2, 0, 2),
                textureLocation: {
                    key: 'alienrock1',
                    type: 'groundtexture',
                    file: 'Base64GroundTexture'
                },
            });
            var isoc = new APP.IsocahedronGeometry({
                center: vec3.fromValues(-1, 1, 2),
                textureLocation: {
                    key: 'energyrock',
                    type: 'groundtexture',
                    file: 'Base64GroundTexture'
                },
            });
            var boxcentered = new APP.BoxCenteredGeometry({
                center: vec3.fromValues(0, 1, 3),
                textureLocation: {
                    key: 'rock',
                    type: 'groundtexture',
                    file: 'Base64GroundTexture'
                },
            });
            var pyramid = new APP.PyramidGeometry({
                center: vec3.fromValues(1, -1, 3),
                textureLocation: {
                    key: 'icebrick',
                    type: 'groundtexture',
                    file: 'Base64GroundTexture'
                },
            });


            // vec3 forms
            var rectCoords = new APP.RectangleWithCoordsGeometry({
                center: vec3.fromValues(2, -1, 0),
                textureLocation: {
                    key: 'sand2',
                    type: 'groundtexture',
                    file: 'Base64GroundTexture'
                },

            });
            var rectCorner = new APP.RectangleStraightCorneredGeometry({
                center: vec3.fromValues(3, -1, 0),
                textureLocation: {
                    key: 'lava',
                    type: 'groundtexture',
                    file: 'Base64GroundTexture'
                },
            });
            var rectCenter = new APP.RectangleStraightCenteredGeometry({
                center: vec3.fromValues(4, -1, 0),
                textureLocation: {
                    key: 'icebrick',
                    type: 'groundtexture',
                    file: 'Base64GroundTexture'
                },
            });
            var ellipse = new APP.EllipseGeometry({
                center: vec3.fromValues(4, -1, 0),
                width: 7,
                textureLocation: {
                    key: 'lava2',
                    type: 'groundtexture',
                    file: 'Base64GroundTexture'
                },
            });

            var flake = new APP.SnowFlake ({

                    center:vec3.fromValues(0,0,0),//Position
                    color: [vec4.fromValues(1,1,0,1)], // No color by default.
                    detaillevel: 4,//amount of iterations (1 is a hollow triangle)
                    scaleX: 4,
                    scaleY: 4,
                    scaleZ: 1,

                });



            groupTestColors.add(dode);
            groupTestColors.add(tetra);
            groupTestColors.add(octo);
            groupTestColors.add(isoc);
            groupTestColors.add(boxcentered);
            groupTestColors.add(pyramid);
            groupTestColors.add(rectCoords);
            groupTestColors.add(rectCorner);
            groupTestColors.add(rectCenter);
            groupTestColors.add(ellipse);
            groupTestColors.add(flake);




            //************** LOAD DECORATION INTO groupGround ***********************//




            scene.add(groupTestColors);
            scene.add(APP.unitMainArwing);
            scene.ambientLightColor = vec3.fromValues(0.2, 0.4, 0.4);
            scene.cubemap = cubemap;
        }
    // -+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+-  LEVEL 3 : EGYPT (snow, snow mountains cumbemap) -+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+- //
        if (this.levelNo == 3) {
            //Load Main Arwing
            //var grassRockMountain1left = new APP.Mesh({
            //    //color: vec4.fromValues(1, 0, 1, 1),
            //    strMeshOBJ: APP.ModelTerrainOBJ.meshMajesticMountain["mesh0"],
            //    textureLocation: {
            //        file: 'ModelTerrainOBJ',
            //        type: 'textureMajesticMountain',
            //        key: 'mesh0'
            //    },
            //    //scaleX: 0.0001,
            //    //scaleY: 0.0001,
            //    scaleZ: 10 * lvlLengthRatio,

            //});

            /********* Test mesh avion Amaury *************/

            APP.groupMeshMainArwing = new APP.Object3D();


            var mesh0 = new APP.Mesh({
                strMeshOBJ: APP.ModelArwingOBJ.meshArwing["mesh0"],
                textureLocation: {
                    file: 'ModelArwingOBJ',
                    type: 'textureArwing',
                    key: 'mesh0'
                },
            });
            var mesh1 = new APP.Mesh({
                strMeshOBJ: APP.ModelArwingOBJ.meshArwing["mesh1"],
                textureLocation: {
                    file: 'ModelArwingOBJ',
                    type: 'textureArwing',
                    key: 'mesh1'
                },
            });
            var mesh2 = new APP.Mesh({
                strMeshOBJ: APP.ModelArwingOBJ.meshArwing["mesh2"],
                textureLocation: {
                    file: 'ModelArwingOBJ',
                    type: 'textureArwing',
                    key: 'mesh2'
                },
            });

            APP.groupMeshMainArwing.add(mesh0);
            APP.groupMeshMainArwing.add(mesh1);
            APP.groupMeshMainArwing.add(mesh2);
            APP.groupMeshMainArwing.transform.rotation.y = 180;
            APP.groupMeshMainArwing.transform.scale.xyz = vec3.fromValues(0.1, 0.1, 0.1);

            var initialVelocityMainArwing = APP.originalVelocity;
            var initialAccelerationMainArwing = APP.originalAcceleration;
            APP.unitMainArwing = new APP.Unit({
                name: "Phoque",
                position: vec3.fromValues(0, 0, 0),//Position
                type: "arwing", // Type of unit
                //health: 100,//amount of elements
                //playerOwner: 1,
                groupMesh: APP.groupMeshMainArwing,
                //projectileTypes: ["defaultProjectile"],
                //damage: 10,
                //energy:100,
                velocity: initialVelocityMainArwing,
                acceleration: initialAccelerationMainArwing,
                //   scene: scene,
                camera: _attrs.camera,
            });
            APP.unitMainArwing.position = this.mainArwingPosition;


            // ----------- Define ground and sky ----------- //
            //APP.Base64Images['space']['posz'] = APP.negzsectory;
            //APP.base64Images. = APP.negzsectory;
            var cubemap = new APP.Cubemap({
                base: 'sky'
            });

            // ----------- Define decorating fixed elements ----------- //

            var groupGround = new APP.Object3D();

            //snow Base Flat
            var snowMaterial = new APP.Material({
                texture: new APP.Texture({
                    textureLocation: {
                        key: 'sand',
                        type: 'groundtexture',
                        file: 'Base64GroundTexture'
                    },
                    isRepeating: true
                }),
                shader: APP.Shaders.defaultShader
            });

            // var corner1 = vec3.fromValues(-100, -1, 20 * lvlLengthRatio);
            // var corner2 = vec3.fromValues(-100, -1, -3000 * lvlLengthRatio);
            // var corner3 = vec3.fromValues(100, -1, -3000 * lvlLengthRatio);
            // var corner4 = vec3.fromValues(100, -1, 20 * lvlLengthRatio);

            // var corner1 = vec3.fromValues(-100, -1, 0);
            // var corner2 = vec3.fromValues(-100, -1, -60);
            // var corner3 = vec3.fromValues(100, -1, 0);
            // var corner4 = vec3.fromValues(100, -1, -60);

            var seaLevel = -1.0;
            var levelWidth = 200;
            var levelLength = 3000;
            var levelToBackLength = 10;

            var corner1 = vec3.fromValues(-levelWidth / 2, seaLevel, levelToBackLength);
            var corner2 = vec3.fromValues(-levelWidth / 2, seaLevel, -levelLength * lvlLengthRatio);
            var corner3 = vec3.fromValues(levelWidth / 2, seaLevel, levelToBackLength);
            var corner4 = vec3.fromValues(levelWidth / 2, seaLevel, -levelLength * lvlLengthRatio);

            coonsEquation = new APP.Math.Surfaces.CoonsSurface({
                C1: new APP.Math.Curves.BezierCubic({
                    // Control points.
                    P0: corner1,
                    P1: corner1,
                    P2: corner2,
                    P3: corner2
                }),
                C2: new APP.Math.Curves.BezierCubic({
                    // Control points.
                    P0: corner3,
                    P1: corner3,
                    P2: corner4,
                    P3: corner4
                }),
                C3: new APP.Math.Curves.BezierCubic({
                    // Control points.
                    P0: corner1,
                    P1: corner1,
                    P2: corner3,
                    P3: corner3
                }),
                C4: new APP.Math.Curves.BezierCubic({
                    // Control points.
                    P0: corner2,
                    P1: corner2,
                    P2: corner4,
                    P3: corner4
                })
            });

            coons = new APP.ParametricSurfaceGeometry({
                equation: coonsEquation,
                slices: 200,
                stacks: 200,
                // color: vec4.fromValues(105 / 255, 180 / 255, 200 / 255, 1.0), // Red.
                // drawControlPoints: false,
                // controlPointsColor: vec4.fromValues(0.0, 1.0, 0.0, 1.0), // Green.
                material: snowMaterial
            });

            // coons.transform.rotation.x = 90.0;

            var waterRectangle = coons;

            // var waterRectangle = new APP.RectangleWithCoordsGeometry({
            //     v0: vec3.fromValues(-100, -1, 20 * lvlLengthRatio),
            //     v1: vec3.fromValues(-100, -1, -3000 * lvlLengthRatio),
            //     v2: vec3.fromValues(100, -1, -3000 * lvlLengthRatio),
            //     v3: vec3.fromValues(100, -1, 20 * lvlLengthRatio),
            //     material: waterMaterial,
            //     isInXZplane: true,
            //     isRepeatingTexture: true
            // });

            //Grass Param Surface 2 times
            var rock1 = new APP.ParametricTerrainBuilder({
                start: (-5 * lvlLengthRatio), //Z initial
                finish: (-3000 * lvlLengthRatio), //Z final
                width: 200, // X interval
                height: -2, //Y level
                detailLevel: 2, //Defines the amount of rectangles needed for the plane. More rectangles = better texture but less performance.
                //color: defaultColor, // No color by default.
                //slices: 2,
                //stacks: 2,
                //controlPointsColor: vec4.fromValues(1.0, 0.0, 0.0, 1.0), //red.
                maxYLevel: 5,
                minYLevel: -1,
                randomOverrride: 0.5,
                textureLocation: {
                    key: 'sand',
                    type: 'groundtexture',
                    file: 'Base64GroundTexture'
                }
                //strTextureImage: APP.Base64GroundTexture.groundtexture["grass"]
            });


            // random pyramids

            // TTTTTTTTTTTTTTTT    TEXTURE LOAD   TTTTTTTTTTTTTTTTTTTTTT//
            var textureCity = new APP.Texture({

                textureLocation: {
                    key: 'beigebrick',
                    type: 'groundtexture',
                    file: 'Base64GroundTexture'
                }

            });

            // var textureCity = textureLoader.initTexture();
            // TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT//
            // console.log(textureCity);

            var cptmax = _.random(90, 100);

            var buildingGeometry = new APP.PyramidGeometry().geometry;

            for (var i = 0; i < cptmax; i++) {
                var pyramid = new APP.PyramidGeometry({
                    geometry: buildingGeometry, // Reuse geometry.
                    center: vec3.fromValues(_.random(-20, 20), 0, _.random(-50, -1500) * lvlLengthRatio),
                    width: Math.random() + 1,
                    height: _.random(3, 7),
                    depth: Math.random() + 1,
                    texture: textureCity
                });
                groupGround.add(pyramid);
            }

            

            //************** LOAD DECORATION INTO groupGround ***********************//


            groupGround.add(waterRectangle);
            groupGround.add(rock1);
            groupGround.add(lavaRockMountain1);
            groupGround.add(lavaRockMountain2);
            groupGround.add(lavaRockMountain3);
            groupGround.add(lavaRockMountain4);
            groupGround.add(lavaRockMountain5);


            scene.add(groupGround);
            scene.add(APP.unitMainArwing);
            scene.cubemap = cubemap;
        }
    // -+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+-  LEVEL 5 : SUN (Lava ground, sun cumbemap) -+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+- //
            if (this.levelNo == 5) {
                //Load Main Arwing
                //var grassRockMountain1left = new APP.Mesh({
                //    //color: vec4.fromValues(1, 0, 1, 1),
                //    strMeshOBJ: APP.ModelTerrainOBJ.meshMajesticMountain["mesh0"],
                //    textureLocation: {
                //        file: 'ModelTerrainOBJ',
                //        type: 'textureMajesticMountain',
                //        key: 'mesh0'
                //    },
                //    //scaleX: 0.0001,
                //    //scaleY: 0.0001,
                //    scaleZ: 10 * lvlLengthRatio,

                //});

                /********* Test mesh avion Amaury *************/

                APP.groupMeshMainArwing = new APP.Object3D();


                var mesh0 = new APP.Mesh({
                    strMeshOBJ: APP.ModelArwingOBJ.meshArwing["mesh0"],
                    textureLocation: {
                        file: 'ModelArwingOBJ',
                        type: 'textureArwing',
                        key: 'mesh0'
                    },
                });
                var mesh1 = new APP.Mesh({
                    strMeshOBJ: APP.ModelArwingOBJ.meshArwing["mesh1"],
                    textureLocation: {
                        file: 'ModelArwingOBJ',
                        type: 'textureArwing',
                        key: 'mesh1'
                    },
                });
                var mesh2 = new APP.Mesh({
                    strMeshOBJ: APP.ModelArwingOBJ.meshArwing["mesh2"],
                    textureLocation: {
                        file: 'ModelArwingOBJ',
                        type: 'textureArwing',
                        key: 'mesh2'
                    },
                });

                APP.groupMeshMainArwing.add(mesh0);
                APP.groupMeshMainArwing.add(mesh1);
                APP.groupMeshMainArwing.add(mesh2);
                APP.groupMeshMainArwing.transform.rotation.y = 180;
                APP.groupMeshMainArwing.transform.scale.xyz = vec3.fromValues(0.1, 0.1, 0.1);

                var initialVelocityMainArwing = APP.originalVelocity;
                var initialAccelerationMainArwing = APP.originalAcceleration;
                APP.unitMainArwing = new APP.Unit({
                    name: "Phoque",
                    position: vec3.fromValues(0, 0, 0),//Position
                    type: "arwing", // Type of unit
                    //health: 100,//amount of elements
                    //playerOwner: 1,
                    groupMesh: APP.groupMeshMainArwing,
                    //projectileTypes: ["defaultProjectile"],
                    //damage: 10,
                    //energy:100,
                    velocity: initialVelocityMainArwing,
                    acceleration: initialAccelerationMainArwing,
                    //   scene: scene,
                    camera: _attrs.camera,
                });
                APP.unitMainArwing.position = this.mainArwingPosition;


                // ----------- Define ground and sky ----------- //
                //APP.Base64Images['space']['posz'] = APP.negzsectory;
                //APP.base64Images. = APP.negzsectory;
                var cubemap = new APP.Cubemap({
                    base: 'sun'
                });

                // ----------- Define decorating fixed elements ----------- //

                var groupGround = new APP.Object3D();

                //Water Base Flat
                var waterMaterial = new APP.WaterMaterial({
                    texture: new APP.Texture({
                        textureLocation: {
                            key: 'lava2',
                            type: 'groundtexture',
                            file: 'Base64GroundTexture'
                        },
                        isRepeating: true
                    })
                });

                // var corner1 = vec3.fromValues(-100, -1, 20 * lvlLengthRatio);
                // var corner2 = vec3.fromValues(-100, -1, -3000 * lvlLengthRatio);
                // var corner3 = vec3.fromValues(100, -1, -3000 * lvlLengthRatio);
                // var corner4 = vec3.fromValues(100, -1, 20 * lvlLengthRatio);

                // var corner1 = vec3.fromValues(-100, -1, 0);
                // var corner2 = vec3.fromValues(-100, -1, -60);
                // var corner3 = vec3.fromValues(100, -1, 0);
                // var corner4 = vec3.fromValues(100, -1, -60);

                var seaLevel = -1.0;
                var levelWidth = 200;
                var levelLength = 3000;
                var levelToBackLength = 10;

                var corner1 = vec3.fromValues(-levelWidth / 2, seaLevel, levelToBackLength);
                var corner2 = vec3.fromValues(-levelWidth / 2, seaLevel, -levelLength * lvlLengthRatio);
                var corner3 = vec3.fromValues(levelWidth / 2, seaLevel, levelToBackLength);
                var corner4 = vec3.fromValues(levelWidth / 2, seaLevel, -levelLength * lvlLengthRatio);

                coonsEquation = new APP.Math.Surfaces.CoonsSurface({
                    C1: new APP.Math.Curves.BezierCubic({
                        // Control points.
                        P0: corner1,
                        P1: corner1,
                        P2: corner2,
                        P3: corner2
                    }),
                    C2: new APP.Math.Curves.BezierCubic({
                        // Control points.
                        P0: corner3,
                        P1: corner3,
                        P2: corner4,
                        P3: corner4
                    }),
                    C3: new APP.Math.Curves.BezierCubic({
                        // Control points.
                        P0: corner1,
                        P1: corner1,
                        P2: corner3,
                        P3: corner3
                    }),
                    C4: new APP.Math.Curves.BezierCubic({
                        // Control points.
                        P0: corner2,
                        P1: corner2,
                        P2: corner4,
                        P3: corner4
                    })
                });

                coons = new APP.ParametricSurfaceGeometry({
                    equation: coonsEquation,
                    slices: 200,
                    stacks: 200,
                    // color: vec4.fromValues(105 / 255, 180 / 255, 200 / 255, 1.0), // Red.
                    // drawControlPoints: false,
                    // controlPointsColor: vec4.fromValues(0.0, 1.0, 0.0, 1.0), // Green.
                    material: waterMaterial
                });

                // coons.transform.rotation.x = 90.0;

                var waterRectangle = coons;

                // var waterRectangle = new APP.RectangleWithCoordsGeometry({
                //     v0: vec3.fromValues(-100, -1, 20 * lvlLengthRatio),
                //     v1: vec3.fromValues(-100, -1, -3000 * lvlLengthRatio),
                //     v2: vec3.fromValues(100, -1, -3000 * lvlLengthRatio),
                //     v3: vec3.fromValues(100, -1, 20 * lvlLengthRatio),
                //     material: waterMaterial,
                //     isInXZplane: true,
                //     isRepeatingTexture: true
                // });

                //Grass Param Surface 2 times
                var rock1 = new APP.ParametricTerrainBuilder({
                    start: (-5 * lvlLengthRatio), //Z initial
                    finish: (-3000 * lvlLengthRatio), //Z final
                    width: 200, // X interval
                    height: -2, //Y level
                    detailLevel: 2, //Defines the amount of rectangles needed for the plane. More rectangles = better texture but less performance.
                    //color: defaultColor, // No color by default.
                    //slices: 2,
                    //stacks: 2,
                    //controlPointsColor: vec4.fromValues(1.0, 0.0, 0.0, 1.0), //red.
                    maxYLevel: 5,
                    minYLevel: -1,
                    randomOverrride: 0.5,
                    textureLocation: {
                        key: 'mesh0lava1',
                        type: 'textureMajesticMountain',
                        file: 'ModelTerrainOBJ'
                    }
                    //strTextureImage: APP.Base64GroundTexture.groundtexture["grass"]
                });

                //Small lava rock mountains  5 times
                var lavaRockMountain1 = new APP.Mesh({

                    textureLocation: {
                        file: 'ModelTerrainOBJ',
                        type: 'textureMajesticMountain',
                        key: 'mesh0lava'
                    },
                    //scaleX: 0.0001,
                    //scaleY: 0.0001,
                    scaleZ: 15 * lvlLengthRatio,

                });
                lavaRockMountain1.transform.position.xyz = vec3.fromValues(-15, -2, -500 * lvlLengthRatio);

                var lavaRockMountain2 = new APP.Mesh({

                    textureLocation: {
                        file: 'ModelTerrainOBJ',
                        type: 'textureMajesticMountain',
                        key: 'mesh0lava'
                    },
                    //scaleX: 0.0001,
                    //scaleY: 0.0001,
                    scaleZ: 13 * lvlLengthRatio,

                });
                lavaRockMountain2.transform.position.xyz = vec3.fromValues(-2, -2, -2000 * lvlLengthRatio);


                var lavaRockMountain3 = new APP.Mesh({

                    textureLocation: {
                        file: 'ModelTerrainOBJ',
                        type: 'textureMajesticMountain',
                        key: 'mesh0lava'
                    },
                    //scaleX: 0.0001,
                    //scaleY: 0.0001,
                    scaleZ: 12 * lvlLengthRatio,

                });
                lavaRockMountain3.transform.position.xyz = vec3.fromValues(2, -2, -799 * lvlLengthRatio);

                var lavaRockMountain4 = new APP.Mesh({

                    textureLocation: {
                        file: 'ModelTerrainOBJ',
                        type: 'textureMajesticMountain',
                        key: 'mesh0lava'
                    },
                    //scaleX: 0.0001,
                    //scaleY: 0.0001,
                    scaleZ: 10 * lvlLengthRatio,

                });
                lavaRockMountain4.transform.position.xyz = vec3.fromValues(-8, -2, -433 * lvlLengthRatio);

                var lavaRockMountain5 = new APP.Mesh({

                    textureLocation: {
                        file: 'ModelTerrainOBJ',
                        type: 'textureMajesticMountain',
                        key: 'mesh0lava'
                    },
                    //scaleX: 0.0001,
                    //scaleY: 0.0001,
                    scaleZ: 20 * lvlLengthRatio,

                });
                lavaRockMountain5.transform.position.xyz = vec3.fromValues(-2, -2, -333 * lvlLengthRatio); //move left



                //************** LOAD DECORATION INTO groupGround ***********************//


                groupGround.add(waterRectangle);
                groupGround.add(rock1);
                groupGround.add(lavaRockMountain1);
                groupGround.add(lavaRockMountain2);
                groupGround.add(lavaRockMountain3);
                groupGround.add(lavaRockMountain4);
                groupGround.add(lavaRockMountain5);


                scene.add(groupGround);
                scene.add(APP.unitMainArwing);
                scene.ambientLightColor = vec3.fromValues(0.4, 0, 0);
                scene.cubemap = cubemap;
            }
        
    // -+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+-  LEVEL 4 : FICHINA (snow, snow mountains cumbemap) -+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+- //
            if (this.levelNo == 4) {
                //Load Main Arwing
                //var grassRockMountain1left = new APP.Mesh({
                //    //color: vec4.fromValues(1, 0, 1, 1),
                //    strMeshOBJ: APP.ModelTerrainOBJ.meshMajesticMountain["mesh0"],
                //    textureLocation: {
                //        file: 'ModelTerrainOBJ',
                //        type: 'textureMajesticMountain',
                //        key: 'mesh0'
                //    },
                //    //scaleX: 0.0001,
                //    //scaleY: 0.0001,
                //    scaleZ: 10 * lvlLengthRatio,

                //});

                /********* Test mesh avion Amaury *************/

                APP.groupMeshMainArwing = new APP.Object3D();


                var mesh0 = new APP.Mesh({
                    strMeshOBJ: APP.ModelArwingOBJ.meshArwing["mesh0"],
                    textureLocation: {
                        file: 'ModelArwingOBJ',
                        type: 'textureArwing',
                        key: 'mesh0'
                    },
                });
                var mesh1 = new APP.Mesh({
                    strMeshOBJ: APP.ModelArwingOBJ.meshArwing["mesh1"],
                    textureLocation: {
                        file: 'ModelArwingOBJ',
                        type: 'textureArwing',
                        key: 'mesh1'
                    },
                });
                var mesh2 = new APP.Mesh({
                    strMeshOBJ: APP.ModelArwingOBJ.meshArwing["mesh2"],
                    textureLocation: {
                        file: 'ModelArwingOBJ',
                        type: 'textureArwing',
                        key: 'mesh2'
                    },
                });

                APP.groupMeshMainArwing.add(mesh0);
                APP.groupMeshMainArwing.add(mesh1);
                APP.groupMeshMainArwing.add(mesh2);
                APP.groupMeshMainArwing.transform.rotation.y = 180;
                APP.groupMeshMainArwing.transform.scale.xyz = vec3.fromValues(0.1, 0.1, 0.1);

                var initialVelocityMainArwing = APP.originalVelocity;
                var initialAccelerationMainArwing = APP.originalAcceleration;
                APP.unitMainArwing = new APP.Unit({
                    name: "Phoque",
                    position: vec3.fromValues(0, 0, 0),//Position
                    type: "arwing", // Type of unit
                    //health: 100,//amount of elements
                    //playerOwner: 1,
                    groupMesh: APP.groupMeshMainArwing,
                    //projectileTypes: ["defaultProjectile"],
                    //damage: 10,
                    //energy:100,
                    velocity: initialVelocityMainArwing,
                    acceleration: initialAccelerationMainArwing,
                    //   scene: scene,
                    camera: _attrs.camera,
                });
                APP.unitMainArwing.position = this.mainArwingPosition;


                // ----------- Define ground and sky ----------- //
                //APP.Base64Images['space']['posz'] = APP.negzsectory;
                //APP.base64Images. = APP.negzsectory;
                var cubemap = new APP.Cubemap({
                    base: 'mountainswinter'
                });

                // ----------- Define decorating fixed elements ----------- //

                var groupGround = new APP.Object3D();

                //snow Base Flat
                var snowMaterial = new APP.Material({
                    texture: new APP.Texture({
                        textureLocation: {
                            key: 'snow',
                            type: 'groundtexture',
                            file: 'Base64GroundTexture'
                        },
                        isRepeating: true
                    }),
                    shader:APP.Shaders.defaultShader
                });

                // var corner1 = vec3.fromValues(-100, -1, 20 * lvlLengthRatio);
                // var corner2 = vec3.fromValues(-100, -1, -3000 * lvlLengthRatio);
                // var corner3 = vec3.fromValues(100, -1, -3000 * lvlLengthRatio);
                // var corner4 = vec3.fromValues(100, -1, 20 * lvlLengthRatio);

                // var corner1 = vec3.fromValues(-100, -1, 0);
                // var corner2 = vec3.fromValues(-100, -1, -60);
                // var corner3 = vec3.fromValues(100, -1, 0);
                // var corner4 = vec3.fromValues(100, -1, -60);

                var seaLevel = -1.0;
                var levelWidth = 200;
                var levelLength = 3000;
                var levelToBackLength = 10;

                var corner1 = vec3.fromValues(-levelWidth / 2, seaLevel, levelToBackLength);
                var corner2 = vec3.fromValues(-levelWidth / 2, seaLevel, -levelLength * lvlLengthRatio);
                var corner3 = vec3.fromValues(levelWidth / 2, seaLevel, levelToBackLength);
                var corner4 = vec3.fromValues(levelWidth / 2, seaLevel, -levelLength * lvlLengthRatio);

                coonsEquation = new APP.Math.Surfaces.CoonsSurface({
                    C1: new APP.Math.Curves.BezierCubic({
                        // Control points.
                        P0: corner1,
                        P1: corner1,
                        P2: corner2,
                        P3: corner2
                    }),
                    C2: new APP.Math.Curves.BezierCubic({
                        // Control points.
                        P0: corner3,
                        P1: corner3,
                        P2: corner4,
                        P3: corner4
                    }),
                    C3: new APP.Math.Curves.BezierCubic({
                        // Control points.
                        P0: corner1,
                        P1: corner1,
                        P2: corner3,
                        P3: corner3
                    }),
                    C4: new APP.Math.Curves.BezierCubic({
                        // Control points.
                        P0: corner2,
                        P1: corner2,
                        P2: corner4,
                        P3: corner4
                    })
                });

                coons = new APP.ParametricSurfaceGeometry({
                    equation: coonsEquation,
                    slices: 200,
                    stacks: 200,
                    // color: vec4.fromValues(105 / 255, 180 / 255, 200 / 255, 1.0), // Red.
                    // drawControlPoints: false,
                    // controlPointsColor: vec4.fromValues(0.0, 1.0, 0.0, 1.0), // Green.
                    material: snowMaterial
                });

                // coons.transform.rotation.x = 90.0;

                var waterRectangle = coons;

                // var waterRectangle = new APP.RectangleWithCoordsGeometry({
                //     v0: vec3.fromValues(-100, -1, 20 * lvlLengthRatio),
                //     v1: vec3.fromValues(-100, -1, -3000 * lvlLengthRatio),
                //     v2: vec3.fromValues(100, -1, -3000 * lvlLengthRatio),
                //     v3: vec3.fromValues(100, -1, 20 * lvlLengthRatio),
                //     material: waterMaterial,
                //     isInXZplane: true,
                //     isRepeatingTexture: true
                // });

                //Grass Param Surface 2 times
                var rock1 = new APP.ParametricTerrainBuilder({
                    start: (-5 * lvlLengthRatio), //Z initial
                    finish: (-3000 * lvlLengthRatio), //Z final
                    width: 200, // X interval
                    height: -2, //Y level
                    detailLevel: 2, //Defines the amount of rectangles needed for the plane. More rectangles = better texture but less performance.
                    //color: defaultColor, // No color by default.
                    //slices: 2,
                    //stacks: 2,
                    //controlPointsColor: vec4.fromValues(1.0, 0.0, 0.0, 1.0), //red.
                    maxYLevel: 5,
                    minYLevel: -1,
                    randomOverrride: 0.5,
                    textureLocation: {
                        key: 'snow',
                        type: 'groundtexture',
                        file: 'Base64GroundTexture'
                    }
                    //strTextureImage: APP.Base64GroundTexture.groundtexture["grass"]
                });

                //Small lava rock mountains  5 times
                var lavaRockMountain1 = new APP.Mesh({

                    textureLocation: {
                        file: 'ModelTerrainOBJ',
                        type: 'textureMajesticMountain',
                        key: 'mesh0snow'
                    },
                    //scaleX: 0.0001,
                    //scaleY: 0.0001,
                    scaleZ: 15 * lvlLengthRatio,

                });
                lavaRockMountain1.transform.position.xyz = vec3.fromValues(-15, -2, -500 * lvlLengthRatio);

                var lavaRockMountain2 = new APP.Mesh({

                    textureLocation: {
                        file: 'ModelTerrainOBJ',
                        type: 'textureMajesticMountain',
                        key: 'mesh0snow'
                    },
                    //scaleX: 0.0001,
                    //scaleY: 0.0001,
                    scaleZ: 13 * lvlLengthRatio,

                });
                lavaRockMountain2.transform.position.xyz = vec3.fromValues(-2, -2, -2000 * lvlLengthRatio);


                var lavaRockMountain3 = new APP.Mesh({

                    textureLocation: {
                        file: 'ModelTerrainOBJ',
                        type: 'textureMajesticMountain',
                        key: 'mesh0snow'
                    },
                    //scaleX: 0.0001,
                    //scaleY: 0.0001,
                    scaleZ: 12 * lvlLengthRatio,

                });
                lavaRockMountain3.transform.position.xyz = vec3.fromValues(2, -2, -799 * lvlLengthRatio);

                var lavaRockMountain4 = new APP.Mesh({

                    textureLocation: {
                        file: 'ModelTerrainOBJ',
                        type: 'textureMajesticMountain',
                        key: 'mesh0snow'
                    },
                    //scaleX: 0.0001,
                    //scaleY: 0.0001,
                    scaleZ: 10 * lvlLengthRatio,

                });
                lavaRockMountain4.transform.position.xyz = vec3.fromValues(-8, -2, -433 * lvlLengthRatio);

                var lavaRockMountain5 = new APP.Mesh({

                    textureLocation: {
                        file: 'ModelTerrainOBJ',
                        type: 'textureMajesticMountain',
                        key: 'mesh0snow'
                    },
                    //scaleX: 0.0001,
                    //scaleY: 0.0001,
                    scaleZ: 20 * lvlLengthRatio,

                });
                lavaRockMountain5.transform.position.xyz = vec3.fromValues(-2, -2, -333 * lvlLengthRatio); //move left

               // ***************** Test snow  storm*********************//
                snowStormEvent1 = new APP.AnimatedEvent({
                        center: vec3.fromValues(-75, 0, -300),//Position
                        type: "snow storm", // Type of event
                        amount: 250,//amount of elements
                        elementSize:3,
                        scaleX: 300,
                        scaleY: 1,
                        scaleZ: 300,
                        velocity: vec3.fromValues(0, -0.1, 0),
                        acceleration: vec3.fromValues(0, -0.1, 0),
                        lifeSpan: 1,//time before elements die
                        texture: null,//used for particle emmiter
                        group: new APP.Object3D()
                });
                scene.add(snowStormEvent1.group);


                //************** LOAD DECORATION INTO groupGround ***********************//


                groupGround.add(waterRectangle);
                groupGround.add(rock1);
                groupGround.add(lavaRockMountain1);
                groupGround.add(lavaRockMountain2);
                groupGround.add(lavaRockMountain3);
                groupGround.add(lavaRockMountain4);
                groupGround.add(lavaRockMountain5);


                scene.add(groupGround);
                scene.add(APP.unitMainArwing);
                scene.ambientLightColor = vec3.fromValues(0, 0, 0.5);
                scene.cubemap = cubemap;
            }



    } // End APP.LevelLoader
