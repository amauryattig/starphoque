APP.Texture = function(params) {

    var defaults = {

        isRepeating: true,

        textureBase64String: undefined,

        textureLocation: { // default texture = grass
            key: 'grass',
            type: 'groundtexture',
            file: 'Base64GroundTexture'
        },

        noCache: false,

        type: 'diffuse'

    };

    var gl = this.gl = APP.WebGLContext;

    var _attrs = _.extend(defaults, _.clone(params));

    _attrs.textureLoader = new APP.TextureLoader(params);
    _attrs.webglTexture = _attrs.textureLoader.initTexture();

    var self = this;

    // Define properties for all attributes.
    _.each(_.keys(_attrs), function(prop) {
        Object.defineProperty(self, prop, {
            get: function() {
                return _attrs[prop];
            },
            set: function(value) {
                _attrs[prop] = value;
            }
        });
    });

    this.bind = function(texUnit, sampler, shader) {

    		// Bind only if texture.image is loaded.
    		gl.activeTexture(texUnit);
    		gl.bindTexture(gl.TEXTURE_2D, _attrs.webglTexture);	

    		shader.uniforms['UseTextures'].value = true;
    		shader.uniforms['texture_' + this.type].value = sampler;
    	
    }

    this.unbind = function() {

    	gl.bindTexture(gl.TEXTURE_2D, null);	

    }


}
