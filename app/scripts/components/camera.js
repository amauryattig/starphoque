//Roll, Pitch, Yaw
//Psi , Theta, PHI 

APP.__Camera = function(options) {
	/* basic camera class */
	
	var defaults = {};

	this.options = APP.Utils.copyToAndUpdate(defaults, options);

	///////////////////////
	//MAIN CAMERA VARIABLES
	this.m_cPosition = vec3.fromValues(0.0, 0.0, 2.0); //Position vector of Camera

	Object.defineProperty(this, 'position', {
		get: function() {
			return this.m_cPosition;
		},
		set: function(value) {
			this.m_cPosition = value;
		}
	});

	this.m_cTarget = vec3.create(); //Position vector (world coordinates) of the target. Changes with translation
	
	this.m_cDirection = vec3.normalize( vec3.create(), 		vec3.subtract( vec3.create() , this.m_cPosition, this.m_cTarget) ); //Direction of Camera (reversed!)
	var m_up = vec3.fromValues(0.0, 1.0, 0.0); //World's up
	this.m_cRight = vec3.normalize( vec3.create(), 			vec3.cross(vec3.create(), m_up, this.m_cDirection)); //Vector right of the Camera
	this.m_cUp = vec3.cross(vec3.create(), 					this.m_cDirection, this.m_cRight); //Vector that points up of the Camera
	
	this.m_mView; //ViewMatrix of the camera 
	this.m_mProjection; //ProjectionMatrix of the camera
	var m_perspective = true;
	
	//Camera specs
	var canvas = document.getElementById('canvas');
	var m_aspectRatio = canvas.width / canvas.height;
	var diagonal = Math.sqrt(canvas.width*canvas.width + canvas.height*canvas.height);
	var nativeRatio = $(window).width() / $(window).height();
	var viewerDistance = diagonal / ( Math.sqrt(nativeRatio*nativeRatio + 1) * canvas.height * Math.tan(1/60) );
	canvas = null; //Garbage collection
	var m_fov = Math.PI/2 //Half of the total angle
	var m_nearClip = 0.25		//Math.floor( viewerDistance / 2);
	var m_farClip = m_nearClip*100000;
	this.m_zoom = 1.0;
	
	//VertigoEffect
	var m_vertigoEffect = false;
	var m_vertigoDistance = 1.0;
	var m_vertigoBaseFov = Math.PI/2;
	var m_vertigoIntensity = 1.76;
	
	///////////////////////////
	//INTERNAL CAMERA VARIABLES
	
	//Speed & acceleration
	var m_maxTranslationSpeed = 0.2; //For moving around!
	var m_maxRotationalSpeed = 3.0; //For looking around!
	var m_maxTranslationAcceleration = 0.005; //For less - 'BLAMO' - sudden movements.
	var m_maxRotationalAcceleration = 0.1;
	var m_friction = 3.0; //Friction operates in absence of movement; slows speeds to 0. Multiplies max acceleration.
	var m_xSpeed = 0.0;
	var m_ySpeed = 0.0;
	var m_zSpeed = 0.0;
	var m_yawSpeed = 0.0;
	var m_pitchSpeed = 0.0;
	var m_rollSpeed = 0.0;
	var m_XrotSpeed = 0.0; //These are distinct variables for 'rotating around the target'
	var m_YrotSpeed = 0.0;
	
	var m_zoomSensitivity = 0.1;
	var m_maxZoom = 20.0;
	var m_minZoom = 0.05;
	var m_maxZoomRatioSpeed = 1.5; //Here, the speed is in terms of proportion from the previous frame. So a 1.1x effect.
	
	//Slerping interpolation (for angles)
	var m_interpolingAngles = false;
	var m_targetQuat = quat.create(); //This counts as a sort of 'extra rotation' matrix, applied to the ViewMatrix.
	var m_startQuat;
	var m_time = 0.0;
	
	//Gradual movement
	var m_gradualMovement = false;
	var m_wayToGo; //Chemin à parcourir de m_startPosition avant d'atteindre la position cible
	var m_startPosition;
	var m_time2 = 0.0;
	
	//////////////////
	//CAMERA FUNCTIONS
	
	//GETTERS & SETTERS
	this.getAspectRatio = function() { return m_aspectRatio };
	this.setAspectRatio = function(a) { if (typeof(a) === 'number') m_aspectRatio = a; else alert("CAMERA parameter error!"); };
	this.resetAspectRatio = function() {
		var canvas = document.getElementById('canvas');
		var m_aspectRatio = canvas.width / canvas.height;
	};
	this.getFov = function() { return m_fov };
	this.setFov = function(f) {
		if (typeof(f) === 'number') { 
			m_fov = f;
			if (m_perspective) this.perspectiveProjection();
			else this.orthographicProjection();
		}
		else alert("CAMERA parameter error!");
	};
	this.setVertigoIntensity = function(i) { if (typeof(i)  === 'number') m_vertigoIntensity = i; else alert("CAMERA parameter error!"); };
	this.isAnimating = function() { return m_gradualMovement || m_interpolingAngles; }
	
	///////////////////////
	//MAIN CAMERA FUNCTIONS
	
	//Mathematical manipulations
	this.lookAtTarget = function(v3, gradual) {
		/* This function sets a goal angle to the camera, which it will smoothly rotate around to achieve
		 * If 'gradual' is false, it will give the View matrix instantly.
		 */
		
		//return mat4.lookAt(mat4.create(), this.m_cPosition, this.m_cDirection, this.m_cUp ); 
		
		var fix = true; //The main target is changed if a new target is specified.
		//This technicality is used in order to avoid constantly rotating around the given target when translating
		
		if ( typeof(gradual) === 'undefined' ) gradual = false;
		if ( typeof(v3) === 'undefined' ) v3 = this.m_cTarget;
		if ( fix !== true && fix !== false) alert("CAMERA parameter error");
		if ( gradual !== true && gradual !== false) alert("CAMERA parameter error");
		
		//Case 1: Rapid calculation
		if ( gradual === false && m_interpolingAngles === false ) {
			this.m_cTarget = vec3.clone(v3);
		
			var positionMatrix = this.makePositionMatrix();
			var orMatrix = this.makeOrientationMatrix();
			
			this.m_mView = mat4.multiply( mat4.create(), positionMatrix, orMatrix );
			this.m_mView = mat4.invert( mat4.create(), this.m_mView );
			
			return this.m_mView;
		}
		
		//Case 2: Already moving to
		if ( m_interpolingAngles === true ) return this.m_mView;
		
		//Case 3: Initiate movement.
		//Store the orientationMatrix from the current orientation vectors in 'startQuad'
		var positionMatrix = this.makePositionMatrix();
		var orStartMatrix = this.makeOrientationMatrix();
		
		//Calculate temporary view matrix from actual values
		this.m_mView = mat4.multiply( mat4.create(), positionMatrix, positionMatrix );
		this.m_mView = mat4.invert( mat4.create(), this.m_mView );
		
		var orStartMtx3 = mat3.fromMat4(mat3.create(), orStartMatrix);
		var startQuat = quat.normalize( quat.create(), quat.fromMat3(quat.create(), orStartMtx3) );
		
		//Store the orientationMatrix from the new orientation vectors in 'targetQuad'
		this.m_cTarget = vec3.add(vec3.create(), vec3.create(), v3);
		this.m_cDirection = vec3.normalize( vec3.create(), 	vec3.subtract(vec3.create(), this.m_cPosition, this.m_cTarget) );
		this.m_cRight = vec3.normalize( vec3.create(), 		vec3.cross(vec3.create(), m_up, this.m_cDirection));
		this.m_cUp = vec3.cross(vec3.create(), this.m_cDirection, this.m_cRight);
		
		var orTargetMatrix = this.makeOrientationMatrix();
		var orTargetMtx3 = mat3.fromMat4(mat3.create(), orTargetMatrix);
		var targetQuat = quat.fromMat3(quat.create(), orTargetMtx3);
		
		//Initiate animation/interpolation
		this.beginSlerpingInterpolation(startQuat, targetQuat);
		
		return this.m_mView;
		
		};
	this.positionByTarget = function(v3 ,distance, gradual) {
		/* Moves the Camera around so that it is next to the target */
		if ( typeof(gradual) === 'undefined' ) gradual = true;
		if ( typeof(v3) === 'undefined' ) v3 = this.m_cFixedTarget;
		if ( typeof(distance) === 'undefined' ) distance = 2.0;
		if ( typeof(distance) !== 'number') alert("CAMERA parameter error");
		if ( gradual !== true && gradual !== false) alert("CAMERA parameter error");
		
		//Find the path to 'travel' in order to make it to the position
		var differenceVector = vec3.create();
		differenceVector[0] = v3[0] - this.m_cPosition[0];
		differenceVector[1] = v3[1] - this.m_cPosition[1];
		differenceVector[2] = v3[2] - this.m_cPosition[2];
		
		//Shorten that path by 'distance' so as not to be right inside the target
		var removeVec = vec3.normalize(vec3.create(), differenceVector);
		vec3.scale(removeVec, removeVec, distance);
		vec3.subtract(differenceVector, differenceVector, removeVec);
		
		if (gradual === false)
			vec3.add(this.m_cPosition, this.m_cPosition, differenceVector);
		else
			this.beginGradualMovement(this.m_cPosition, differenceVector);
		
		//this.lookAtTarget(v3, gradual);
	}
	this.orthographicProjection = function() {
		/* Makes the projection matrix orthographic */
		
		//While the fov doesn't really matter here, we'll use it so the "near" results are similar between
		//either types of projection.
		var t = 2 / this.m_zoom;
		var r = m_aspectRatio * t;
		//Since the frustum is symmetrical, we can simplify and ignore l and b, and the 3rd and 7th elements.
		var e = m_farClip - m_nearClip; //Focal distance
		var mtx = mat4.create();
		
		mtx[0]=1/r; 			//1							//2		//3
		/*4*/		 			mtx[5]=1/t;					//6		//7
		/*8*/ 					/*9*/ 			mtx[10] = -2/e; 	mtx[11] = -(m_farClip+m_nearClip)/e;
		/*12*/		 			/*13*/ 						/*14*/	mtx[15] = 1.0;
		
		
		
		this.m_mProjection = mat4.transpose( mat4.create(), mtx );
		m_perspective = false;
		return this.m_mProjection;
	}
	this.perspectiveProjection = function() {
		/* Makes the projection matrix perspective */
		
		//return mat4.perspective(mat4.create(), m_fov, m_aspectRatio, m_nearClip, m_farClip); 
		
		//First, we have the Near and Far planes, which are calculated according to the 'viewer position'.
		//For an agreeable time, we want to calculate the top/bottom according to the FOV angle.
		//tan( FOV ) = top/near ... THINK SOH CAH TOA!!! This is simple.
		if ( m_vertigoEffect === true ) {
			var newDistance = vec3.distance(this.m_cTarget, this.m_cPosition);
			var ratio = m_vertigoDistance / newDistance;
			m_fov = m_vertigoIntensity * Math.atan( ratio );
		}
		var t = Math.tan( m_fov/2 ) * m_nearClip / this.m_zoom;
		var r = m_aspectRatio * t;
		//Since the frustum is symmetrical, we can simplify and ignore l and b, and the 3rd and 6th components.
		var e = m_farClip - m_nearClip; //Focal distance
		
		var mtx = mat4.create();
		
		mtx[0] = m_nearClip/r; 	//1			//2						//3
		/*4*/		 			mtx[5] = m_nearClip/t;	//6			//7	
		/*8*/ 					/*9*/ 			mtx[10] = -(m_farClip+m_nearClip)/e; mtx[11] = -2*m_farClip*m_nearClip/e;
		/*12*/		 			/*13*/ 			mtx[14] = -1.0; 				mtx[15] = 0;

		
		
		this.m_mProjection = mat4.transpose( mat4.create(), mtx );
		m_perspective = true;
		return this.m_mProjection;
	}
	
	//Dynamic effects
	this.beginSlerpingInterpolation = function(startQuat, targetQuat) {
		m_startQuat = startQuat;
		m_targetQuat = targetQuat;
		m_time = 0.0;
		m_interpolingAngles = true;
	}
	this.beginGradualMovement = function(vec1, vec2) {
		m_startPosition = vec3.add(vec3.create(), vec3.create(), vec1);
		m_wayToGo = vec3.add(vec3.create(), vec3.create(), vec2);
		m_time2 = 0.0;
		m_gradualMovement = true;
	}
	this.setVertigoEffect = function(activate) {
		//This function activates the vertigo effect centered on the actual target and parametered at the actual distance
		if ( activate !== true && activate !== false) alert("CAMERA parameter error");
		
		if ( activate === true ) {
			m_vertigoEffect = true;
			m_vertigoDistance = vec3.distance(this.m_cTarget, this.m_cPosition);
			m_vertigoBaseFov = m_fov;
			this.perspectiveProjection();
		}
		else if ( activate === false ) {
			m_vertigoEffect = false;
			this.setFov( m_vertigoBaseFov );
		}
	}
	
	//User interaction
	this.cameraEventManager = function() {
		/* Deals with change with the mousewheel and mouse buttons and movement
		 * In normal circumstances, the wheel is the zoom.
		 * When the left mouse button is held, moving the mouse around serves to rotate the camera around the target
		 * When the right mouse button is held, moving the mouse around serves to change the pitch (Y) and yaw (X).
		 * The arrow keys are used to move around the X and Z axis.
		 */
		var mouseXoffset = 0.0;
		var mouseYoffset = 0.0;
		var mouseXoffset2 = 0.0;
		var mouseYoffset2 = 0.0;
		 
		if (m_interpolingAngles === false && m_gradualMovement === false) {
			if (APP.userInput.mouseRightPressed) {
				//mouseXoffset = APP.userInput.mouseX;
				//mouseYoffset = APP.userInput.mouseY;
			}
			else if (APP.userInput.mouseLeftPressed) {
				//mouseXoffset2 = APP.userInput.mouseX;
				//mouseYoffset2 = APP.userInput.mouseY;
			}
			
			var mouseYscroll = 0.0;
			mouseYscroll = APP.userInput.mouseScrollDeltaY;
			this.processMouseScroll(mouseYscroll);
		}
		
		//Normalize the mouse Position
		if (mouseXoffset !== 0 || mouseYoffset !== 0 ) {
			var canvas = document.getElementById('canvas');
			mouseXoffset = (mouseXoffset - canvas.width/2)/canvas.width/2;
			mouseYoffset = (mouseYoffset - canvas.height/2)/canvas.height/2;
		}
		
		this.processUncenteredMouseMovement(mouseXoffset, mouseYoffset);
		this.processCenteredMouseMovement(mouseXoffset2, mouseYoffset2);
		
		this.processKeyboard();
		
		
		var tempDir = vec3.normalize(vec3.create(), vec3.subtract(vec3.create(), this.m_cPosition, this.m_cTarget) );
		
		//Center on target when space is pressed (also when Enter is pressed)
		if ( (APP.userInput.keyPressed[32] || APP.userInput.keyPressed[13]) && !this.isAnimating() && (
			tempDir[0] != this.m_cDirection[0] ||
			tempDir[1] != this.m_cDirection[1] ||
			tempDir[2] != this.m_cDirection[2] ) ) {
			var target = this.m_cFixedTarget;
			this.lookAtTarget(target, true);
		}
		
		//Center AND close in when Enter is pressed
		if ( APP.userInput.keyPressed[13] && m_gradualMovement === false &&
			vec3.distance(this.m_cPosition, this.m_cTarget) != 2.0) {
			this.positionByTarget( this.m_cTarget );
		}
		
		if (m_interpolingAngles === true) this.slerpingInterpolation();
		if (m_gradualMovement === true) this.gradualMovement();
	}
	
	////////////////////
	//INTERNAL FUNCTIONS
	
	//Matrix/Quaternion functions
	this.rotateFromEulerToQuaternion = function(v3,yaw,pitch,roll) {
		//Converts the euler angles to a quaternion, and then generate the orientation vectors.
		var q = makeQuatFromEulerAngles(quat.create(), yaw, pitch, roll);
		return vec3.transformQuat(vec3.create(),v3,q)
	}
	this.rotateAroundTarget = function(yaw,pitch,target) {
		//Converts the euler angles to a quaternion, then create a rotation/translation matrix
		//which transforms the position vectors around the target.
		
		//Step 1: rotate the orientation vectors
		//Recreating orientation vectors based on target
		var yquat = quat.setAxisAngle(quat.create(), this.m_cRight, -pitch/180*Math.PI);
		var xquat = quat.setAxisAngle(quat.create(), this.m_cUp, yaw/180*Math.PI);
		this.m_cUp = vec3.transformQuat(vec3.create(), this.m_cUp, yquat);
		this.m_cDirection = vec3.transformQuat(vec3.create(), this.m_cDirection, yquat);
		this.m_cRight = vec3.transformQuat(vec3.create(), this.m_cRight, xquat);
		this.m_cDirection = vec3.transformQuat(vec3.create(), this.m_cDirection, xquat);
		
		//Step 2: Center the position matrix around the target, and rotate the position
		//Position - target = camera position relative to target
		var differenceVec = vec3.subtract(vec3.create(), this.m_cPosition, target);
		differenceVec = vec3.transformQuat(vec3.create(), differenceVec, xquat);
		differenceVec = vec3.transformQuat(vec3.create(), differenceVec, yquat);
		this.m_cPosition = vec3.add(vec3.create(), target, differenceVec); //Reapplying the rotated difference
	}
	this.makePositionMatrix = function() {
		var positionMatrix = mat4.create();
		positionMatrix[12] = this.m_cPosition[0];
		positionMatrix[13] = this.m_cPosition[1];
		positionMatrix[14] = this.m_cPosition[2];
		
		return positionMatrix;
	}
	this.makeOrientationMatrix = function() {
		var orMatrix = mat4.create();
	
		orMatrix[0] = this.m_cRight[0]; 	orMatrix[1] = this.m_cRight[1]; 	orMatrix[2] = this.m_cRight[2];
		orMatrix[4] = this.m_cUp[0]; 		orMatrix[5] = this.m_cUp[1]; 		orMatrix[6] = this.m_cUp[2];
		orMatrix[8] = this.m_cDirection[0];	orMatrix[9] = this.m_cDirection[1];	orMatrix[10] = this.m_cDirection[2];
		
		return orMatrix;
	}
	var makeQuatFromEulerAngles = function(out,yaw,pitch,roll) {
		if ( typeof(yaw) !== 'number') alert("CAMERA parameter error");
		if ( typeof(pitch) !== 'number') alert("CAMERA parameter error");
		if ( typeof(roll) !== 'number') alert("CAMERA parameter error");
		
		out = quat.create();
		
		pitch = pitch/180*Math.PI;
		yaw = yaw/180*Math.PI;
		roll = roll/180*Math.PI;
		
		//out[0] = Math.cos((yaw - roll)/2) * Math.sin(pitch/2);
		//out[1] = Math.sin((yaw - roll)/2) * Math.sin(pitch/2);
		//out[2] = Math.sin((yaw + roll)/2) * Math.cos(pitch/2);
		//out[3] = Math.cos((yaw + roll)/2) * Math.cos(pitch/2);
		
		var c1 = Math.cos(yaw/2);
		var s1 = Math.sin(yaw/2);
		var c2 = Math.cos(roll/2);
		var s2 = Math.sin(roll/2);
		var c3 = Math.cos(pitch/2);
		var s3 = Math.sin(pitch/2);
		var c1c2 = c1*c2;
		var s1s2 = s1*s2;
		out[0] = c1c2*s3 + s1s2*c3;
		out[1] = s1*c2*c3 + c1*s2*s3;
		out[2] = c1*s2*c3 - s1*c2*s3;
		out[3] = c1c2*c3 - s1s2*s3; //x, y, z, w
		
		return out;
	}
	
	//User interaction functions
	this.processUncenteredMouseMovement = function(xoffset, yoffset, constrainPitch) {
		/* Takes in mouse inputs and change the camera's yaw and pitch as one.
		 * Does NOT make the camera go around the target object.
		 * Deals with friction when unchanged
		 */
		if ( typeof(constrainPitch) === 'undefined' ) constrainPitch = false;
		if ( typeof(xoffset) !== 'number' || typeof(yoffset) !== 'number' || (constrainPitch !== true && constrainPitch !== false) )
			alert("CAMERA parameter error");
			
        xoffset *= -1*m_maxRotationalSpeed;
        yoffset *= m_maxRotationalSpeed;
		var rollSpeed = 0;
		
		var zquat = quat.create();
		if ( APP.userInput.keyPressed[82] && !this.isAnimating() ) //r
			rollSpeed = -m_maxRotationalSpeed/2;
		if ( APP.userInput.keyPressed[69] && !this.isAnimating() ) //e
			rollSpeed = m_maxRotationalSpeed/2;
		if ( APP.userInput.keyPressed[87] && !this.isAnimating() ) //w
			m_pitchSpeed = -m_maxRotationalSpeed/2;
		if ( APP.userInput.keyPressed[83] && !this.isAnimating() ) //s
			m_pitchSpeed = m_maxRotationalSpeed/2;
		if ( APP.userInput.keyPressed[65] && !this.isAnimating() ) //a
			m_yawSpeed = m_maxRotationalSpeed/2;
		if ( APP.userInput.keyPressed[68] && !this.isAnimating() ) //d
			m_yawSpeed = -m_maxRotationalSpeed/2;
		
		//Smoothness in movements, includes maximum acceleration bounds as well as friction
		var dir;
		var fric = m_friction * m_maxRotationalAcceleration;
		
		if (xoffset === 0 || yoffset === 0) { //Friction when inactive
			if (m_yawSpeed != 0) {
				if ( Math.abs(m_yawSpeed) < fric ) m_yawSpeed = 0;
				else {
					dir = -1*Math.sign(m_yawSpeed);
					m_yawSpeed += dir*fric;
				}
			}
			if (m_pitchSpeed != 0) {
				if ( Math.abs(m_pitchSpeed) < fric ) m_pitchSpeed = 0;
				else {
					dir = -1*Math.sign(m_pitchSpeed);
					m_pitchSpeed += dir*fric;
				}
			}
		}
		else {
			m_yawSpeed = xoffset;
			m_yawSpeed = Math.min(m_maxRotationalSpeed, m_yawSpeed);
			m_yawSpeed = Math.max(-m_maxRotationalSpeed, m_yawSpeed);
			m_pitchSpeed = yoffset;
			m_pitchSpeed = Math.min(m_maxRotationalSpeed, m_pitchSpeed);
			m_pitchSpeed = Math.max(-m_maxRotationalSpeed, m_pitchSpeed);
		}
		
		if ( m_yawSpeed != 0 || m_pitchSpeed != 0 || rollSpeed != 0 ) {
			// Update internal angles, which affect the position of the orientation vectors
			var yquat = quat.setAxisAngle(quat.create(), this.m_cRight, -m_pitchSpeed/180*Math.PI);
			var xquat = quat.setAxisAngle(quat.create(), this.m_cUp, m_yawSpeed/180*Math.PI);
			var zquat = quat.setAxisAngle(quat.create(), this.m_cDirection, rollSpeed/360*Math.PI)
			this.m_cUp = vec3.transformQuat(vec3.create(), this.m_cUp, yquat);
			this.m_cDirection = vec3.transformQuat(vec3.create(), this.m_cDirection, yquat);
			this.m_cRight = vec3.transformQuat(vec3.create(), this.m_cRight, xquat);
			this.m_cDirection = vec3.transformQuat(vec3.create(), this.m_cDirection, xquat);
			this.m_cUp = vec3.transformQuat(vec3.create(), this.m_cUp, zquat);
			this.m_cRight = vec3.transformQuat(vec3.create(), this.m_cRight, zquat);
		}
    }
	this.processCenteredMouseMovement = function(xoffset, yoffset) {
		/* Makes the camera go around the target object (offcentered X and Y rotations) 
		 * Deals with friction when unchanged
		 */
		if ( typeof(xoffset) !== 'number' || typeof(yoffset) !== 'number')
			alert("CAMERA parameter error");
			
		xoffset *= -1*m_maxRotationalAcceleration;
        yoffset *= m_maxRotationalAcceleration;
		var keyInter = false
		
		if ( APP.userInput.keyPressed[74] && !this.isAnimating() ) { //j
			m_XrotSpeed = -m_maxRotationalSpeed/2;
			keyInter = true;
		}
		else if ( APP.userInput.keyPressed[76] && !this.isAnimating() ) { //l
			m_XrotSpeed = m_maxRotationalSpeed/2;
			keyInter = true;
		}
		if ( APP.userInput.keyPressed[73] && !this.isAnimating() ) { //i
			m_YrotSpeed = -m_maxRotationalSpeed/2;
			keyInter = true;
		}
		else if ( APP.userInput.keyPressed[75] && !this.isAnimating() ) {//k
			m_YrotSpeed = m_maxRotationalSpeed/2;
			keyInter = true;
		}
		
		//Smoothness in movements, includes maximum acceleration bounds as well as friction
		var dir;
		var fric = m_friction * m_maxRotationalAcceleration;
		
		if ( xoffset === 0 && yoffset === 0 && !keyInter) { //Friction when inactive
			if (m_XrotSpeed != 0) {
				if ( Math.abs(m_XrotSpeed) < fric ) m_XrotSpeed = 0;
				else {
					dir = -1*Math.sign(m_XrotSpeed);
					m_XrotSpeed += dir*fric;
				}
			}
			if (m_YrotSpeed != 0) {
				if ( Math.abs(m_YrotSpeed) < fric ) m_YrotSpeed = 0;
				else {
					dir = -1*Math.sign(m_YrotSpeed);
					m_YrotSpeed += dir*fric;
				}
			}
		}
		else {
			m_XrotSpeed += xoffset;
			m_XrotSpeed = Math.min(m_maxRotationalSpeed, m_XrotSpeed);
			m_XrotSpeed = Math.max(-m_maxRotationalSpeed, m_XrotSpeed);
			m_YrotSpeed += yoffset;
			m_YrotSpeed = Math.min(m_maxRotationalSpeed, m_YrotSpeed);
			m_YrotSpeed = Math.max(-m_maxRotationalSpeed, m_YrotSpeed);
		}
		if ( m_XrotSpeed != 0.0 || m_YrotSpeed != 0.0) {
			this.rotateAroundTarget(m_XrotSpeed, m_YrotSpeed, this.m_cTarget);
		}
	}
	this.processMouseScroll = function(yoffset) {
		/* Processes the mouse wheel's y movements, which normally affect zoom
		 * Deals with friction when unchanged
		 */
		if ( typeof(yoffset) !== 'number') alert("CAMERA parameter error");
		
		yoffset *= m_zoomSensitivity;
		yoffset = Math.min(m_maxZoomRatioSpeed, yoffset);
		yoffset = Math.max(-m_maxZoomRatioSpeed, yoffset);
		
		if ( yoffset > 0 )
			if ( (1 + yoffset) * this.m_zoom >= m_maxZoomRatioSpeed * this.m_zoom ) this.m_zoom *= m_maxZoomRatioSpeed;
			else this.m_zoom *= 1 + yoffset;
		else if ( yoffset < 0 )
			if ( this.m_zoom / Math.abs(yoffset - 1) <= this.m_zoom / m_maxZoomRatioSpeed ) this.m_zoom /= m_maxZoomRatioSpeed;
			else this.m_zoom /= Math.abs(yoffset - 1);
		
        if (this.m_zoom <= m_minZoom)
            this.m_zoom = m_minZoom;
        if (this.m_zoom >= m_maxZoom)
            this.m_zoom = m_maxZoom;
	}
	this.processKeyboard = function() {
		/* Deals with X, Y and Z translation with the arrow keys (among other things)
		 * Deals with friction when unchanged
		 */
		var xv = false, yv = false, zv = false;
		var xinv = false, yinv = false, zinv = false;
				
		if (APP.userInput.keyPressed[33] && !this.isAnimating() ) { // Page Up
		  m_zSpeed -= m_maxTranslationAcceleration;
		  m_zSpeed = Math.max(-m_maxTranslationSpeed, m_zSpeed); 
		  zv = true;
		  if (m_zSpeed > 0) zinv = true;
		}
		else if (APP.userInput.keyPressed[34] && !this.isAnimating()) { // Page Down
		  m_zSpeed += m_maxTranslationAcceleration;
		  m_zSpeed = Math.min(m_maxTranslationSpeed, m_zSpeed);
		  zv = true;
		  if (m_zSpeed < 0) zinv = true;
		}
		if (APP.userInput.keyPressed[37] && !this.isAnimating()) { // Left Arrow
		  m_xSpeed -= m_maxTranslationAcceleration;
		  m_xSpeed = Math.max(-m_maxTranslationSpeed, m_xSpeed);
		  xv = true;
		  if (m_xSpeed > 0) xinv = true;
		}
		else if (APP.userInput.keyPressed[39] && !this.isAnimating()) { // Right Arrow
		  m_xSpeed += m_maxTranslationAcceleration;
		  m_xSpeed = Math.min(m_maxTranslationSpeed, m_xSpeed);
		  xv = true;
		  if (m_xSpeed < 0) xinv = true;
		}
		if (APP.userInput.keyPressed[38] && !this.isAnimating()) { // Up Arrow
		  m_ySpeed += m_maxTranslationAcceleration;
		  m_ySpeed = Math.min(m_maxTranslationSpeed, m_ySpeed); 
		  yv = true;
		  if (m_ySpeed < 0) yinv = true;
		}
		else if (APP.userInput.keyPressed[40] && !this.isAnimating()) { // Down Arrow
		  m_ySpeed -= m_maxTranslationAcceleration;
		  m_ySpeed = Math.max(-m_maxTranslationSpeed, m_ySpeed); 
		  yv = true;
		  if (m_ySpeed > 0) yinv = true;
		}
		
		//Friction
		var dir;
		var fric = m_friction * m_maxTranslationAcceleration;
		if ( (xv === false && m_xSpeed != 0) || xinv) {
			if ( Math.abs(m_xSpeed) < fric ) m_xSpeed = 0;
			else {
				dir = -1*Math.sign(m_xSpeed);
				m_xSpeed += dir*fric;
			}
		}
		if ( (yv === false && m_ySpeed != 0) || yinv) {
			if ( Math.abs(m_ySpeed) < fric ) m_ySpeed = 0;
			else {
				dir = -1*Math.sign(m_ySpeed);
				m_ySpeed += dir*fric;
			}
		}
		if ( (zv === false && m_zSpeed != 0) || zinv) {
			if ( Math.abs(m_zSpeed) < fric ) m_zSpeed = 0;
			else {
				dir = -1*Math.sign(m_zSpeed);
				m_zSpeed += dir*fric;
			}
		}
		
		vec3.scaleAndAdd(this.m_cPosition, this.m_cPosition, this.m_cRight, m_xSpeed);
		vec3.scaleAndAdd(this.m_cPosition, this.m_cPosition, this.m_cUp, m_ySpeed);
		vec3.scaleAndAdd(this.m_cPosition, this.m_cPosition, this.m_cDirection, m_zSpeed);
	}
	
	//Animation functions
	this.gradualMovement = function() {
		//Makes a time-based interpolation from the start to the target distance
		
		var dist = vec3.scale(vec3.create(), m_wayToGo, m_time2); //Multiply the distance to travel by the percentage of time waited
		
		vec3.add(this.m_cPosition, m_startPosition, dist);
		
		if ( m_time2 >= 1.0 ) m_gradualMovement = false;
		else m_time2 += 0.01;
		if ( m_time2 > 1.0 ) m_time2 = 1;
	}
	this.slerpingInterpolation = function() {
	//Makes a time-based interpolation from the start to the target angle
		var interpolatedQuat = quat.slerp( quat.create(), m_startQuat, m_targetQuat, m_time);
		
		var interpolatedOrMatrix = mat4.fromQuat( mat4.create(), interpolatedQuat); //Since the quad is the mat4 of the old orientation vectors
		
		var positionMatrix = this.makePositionMatrix();
		
		interpolatedOrMatrix = mat4.transpose(mat4.create(), interpolatedOrMatrix);
		
		this.m_mView = mat4.multiply(mat4.create(), positionMatrix, interpolatedOrMatrix);
		this.m_mView = mat4.invert( mat4.create(), this.m_mView );
		
		if ( m_time >= 1.0 ) m_interpolingAngles = false;
		else m_time += 0.01;
	}
}

// Extend with events module.
APP.Camera = Compose(APP.__Camera, APP.Events);
