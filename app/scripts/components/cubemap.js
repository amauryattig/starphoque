// Inspired from : cubemapviewer.js, see http://www.humus.name.

APP.__Cubemap = function(params) {
    // Constructor default sky
    var defaults = {
        base: 'sky',
    };

    var _attrs = _.extend(defaults, _.clone(params));
    var base = _attrs.base;

    // Internal shader programs.

    var vs_text =
        "attribute vec3 vPos;\
        uniform mat4 mvp;\
        varying vec3 tex_coord;\
        void main(){\
            vec4 pos = mvp * vec4(vPos, 1);\
            gl_Position = pos.xyww;\
            tex_coord = vPos;\
    }";

    var fs_text =
        "precision highp float;\
        uniform samplerCube samp;\
        varying vec3 tex_coord;\
        void main(){\
            gl_FragColor = textureCube(samp, tex_coord);\
    }";

    var gl = APP.WebGLContext;
    var mvp;
    var sampler;
    var cubemap;
    var isLoaded = false;

    // We use a Shader object just to compile the sources.
    var shader = new APP.Shader({
        autocompile: false,
        sourceIsTemplate: false
    });

    var prog = shader.compileProgram(vs_text, fs_text).prog;

    var posBuffer = gl.createBuffer();

    var points = [-1.0, 1.0, -1.0, -1.0, -1.0, -1.0,
        1.0, -1.0, -1.0,
        1.0, -1.0, -1.0,
        1.0, 1.0, -1.0, -1.0, 1.0, -1.0,

        -1.0, -1.0, 1.0, -1.0, -1.0, -1.0, -1.0, 1.0, -1.0, -1.0, 1.0, -1.0, -1.0, 1.0, 1.0, -1.0, -1.0, 1.0,

        1.0, -1.0, -1.0,
        1.0, -1.0, 1.0,
        1.0, 1.0, 1.0,
        1.0, 1.0, 1.0,
        1.0, 1.0, -1.0,
        1.0, -1.0, -1.0,

        -1.0, -1.0, 1.0, -1.0, 1.0, 1.0,
        1.0, 1.0, 1.0,
        1.0, 1.0, 1.0,
        1.0, -1.0, 1.0, -1.0, -1.0, 1.0,

        -1.0, 1.0, -1.0,
        1.0, 1.0, -1.0,
        1.0, 1.0, 1.0,
        1.0, 1.0, 1.0, -1.0, 1.0, 1.0, -1.0, 1.0, -1.0,

        -1.0, -1.0, -1.0, -1.0, -1.0, 1.0,
        1.0, -1.0, -1.0,
        1.0, -1.0, -1.0, -1.0, -1.0, 1.0,
        1.0, -1.0, 1.0
    ];


    var _deleteImagesInRam = function() {
        // Remove all sky textures from the object.
        // They are not necessary anymore.
        delete APP.Base64Images[base];
    };

    // This function will run only after being called 6 times.
    var onAllImagesLoaded = _.after(6, function(texture) {
        gl.bindTexture(gl.TEXTURE_CUBE_MAP, texture);
        gl.generateMipmap(gl.TEXTURE_CUBE_MAP);

        var posAttrLoc = gl.getAttribLocation(prog, "vPos");
        gl.enableVertexAttribArray(posAttrLoc);

        gl.bindBuffer(gl.ARRAY_BUFFER, posBuffer);

        var vertices = new Float32Array(points);
        gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);

        gl.vertexAttribPointer(posAttrLoc, 3, gl.FLOAT, false, 0, 0);

        mvp = gl.getUniformLocation(prog, "mvp");
        sampler = gl.getUniformLocation(prog, "samp");

        gl.bindTexture(gl.TEXTURE_CUBE_MAP, null);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);


        isLoaded = true;

        // _deleteImagesInRam();
    });

    function loadCubeMap(base) {

        var texture = gl.createTexture();

        gl.bindTexture(gl.TEXTURE_CUBE_MAP, texture);

        // gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_WRAP_R, gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_LINEAR);
        // gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
        gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_MAG_FILTER, gl.LINEAR);

        gl.bindTexture(gl.TEXTURE_CUBE_MAP, null);


        var faces = [
            ["posx", gl.TEXTURE_CUBE_MAP_POSITIVE_X],
            ["negx", gl.TEXTURE_CUBE_MAP_NEGATIVE_X],
            ["posy", gl.TEXTURE_CUBE_MAP_POSITIVE_Y],
            ["negy", gl.TEXTURE_CUBE_MAP_NEGATIVE_Y],
            ["posz", gl.TEXTURE_CUBE_MAP_POSITIVE_Z],
            ["negz", gl.TEXTURE_CUBE_MAP_NEGATIVE_Z]
        ];

        for (var i = 0; i < faces.length; i++) {

            var face = faces[i][1];
            var image = new Image();

            image.onload = function(texture, face, image) {
                return function() {
                    gl.bindTexture(gl.TEXTURE_CUBE_MAP, texture);
                    gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, false);
                    gl.texImage2D(face, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);

                    onAllImagesLoaded(texture);

                    gl.bindTexture(gl.TEXTURE_CUBE_MAP, null);
                }
            }(texture, face, image);

            image.src = APP.Base64Images[base][faces[i][0]];

        }

        return texture;
    }

    cubemap = loadCubeMap(base);


    this.draw = function(args) {
        if (isLoaded) { // All images are loaded.

            gl.depthFunc(gl.LEQUAL); // Every object will be drawn over the cube.

            gl.useProgram(prog); // Switch to internal shader program.

            var viewMatrix = args.viewMatrix;
            var projMatrix = args.projMatrix;

            var posAttrLoc = gl.getAttribLocation(prog, "vPos");
            gl.bindBuffer(gl.ARRAY_BUFFER, posBuffer);
            gl.vertexAttribPointer(posAttrLoc, 3, gl.FLOAT, false, 0, 0);

            // Bind cube map texture.
            gl.activeTexture(gl.TEXTURE0);
            gl.bindTexture(gl.TEXTURE_CUBE_MAP, cubemap);
            gl.uniform1i(sampler, null);

            var vNoTranslation = APP.Math.removeTranslation(viewMatrix); // No translation.
            var mat = mat4.mul(mat4.create(), projMatrix, vNoTranslation); // Create MVP matrix.
            gl.uniformMatrix4fv(mvp, false, mat); // Bind MVP matrix to uniform.
            gl.drawArrays(gl.TRIANGLES, 0, 36); // Draw the cube.

            gl.flush();

            gl.bindBuffer(gl.ARRAY_BUFFER, null);
            gl.bindTexture(gl.TEXTURE_CUBE_MAP, null); // Unbind texture.

            gl.depthFunc(gl.LESS);
            // gl.enable(gl.BLEND);

            args.defaultShader.useProgram(); // Switch back to main shader program.

        }
    }
    this.getbase = getbase;

    function getbase() {
        return base;
    }

    function setbase(newbase) {
        base = newbase;
    }
}

APP.Cubemap = Compose(APP.Object3D, APP.__Cubemap);

APP.Cubemap.getbase = function() {
    return base;
}

APP.Cubemap.setbase = function(newbase) {
    base = newbase;
}
