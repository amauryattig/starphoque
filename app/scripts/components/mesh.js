// this.attributes contains :
// - strMeshOBJ and strTextureOBJ  -> string version of OBJ and string version of texture for the Mesh.
// - color.
// Pass an object to the constructor containing those attributes.
// They will be copied to 'this.attributes'
APP.__Mesh = function(params) {

    var defaultColor = vec4.fromValues(0.0, 0.0, 0.0, 0.0);
    var defaults = {
        color: defaultColor, // No color by default.
        strMeshOBJ: "",
        // strTextureOBJ: APP.Base64GroundTexture.groundtexture["rock"],
        scaleX: 1,
        scaleY: 1,
        scaleZ: 1,
    };

    var _attrs = _.extend(defaults, _.clone(params));

    //Parameters
    var color = _attrs.color;
    var strMeshOBJ = _attrs.strMeshOBJ;
    var strTextureOBJ = _attrs.strTextureOBJ;
    var scaleX = _attrs.scaleX;
    var scaleY = _attrs.scaleY;
    var scaleZ = _attrs.scaleZ;
    var texLocation = _attrs.textureLocation;


    //var color = this.attributes.color;
    //var strMeshOBJ = this.attributes.strMeshOBJ;
    //var strTextureOBJ = this.attributes.strTextureOBJ;

    var geometry;
    var materials = [];


    // --------------------------------- OBJ PARSE -------------------------------------//
    //  console.log(strMeshOBJ);
    var estEnTriplets = true;
    var tempCursorStartLine = 0;
    var tempCursorEndLine = strMeshOBJ.indexOf("\n");
    var currentLine = "";
    var nbSauts = 1;
    var currentLineParsedList = [];
    var vertexList = [];
    var textureList = [];
    var normalList = [];
    var indicesList = [];
    var indicesListVertex = [];
    var indicesListTextures = [];
    var indicesListNormals = [];
    var colorList = [];
    var lineList = strMeshOBJ.split("\n");
    //  console.log("nb Sauts: " + nbSauts + "start end :" + tempCursorStartLine + " - " + tempCursorEndLine + "\n");
    for (var l = 0; l < lineList.length - 1; l++) { //n lit toute la string avec le mod�le OBJ

        currentLine = lineList[l];

        //Avec la ligne courante, on la classe selon sa premi�re lettre et on la divise selon l'utilit� de chaque nombre

        if (currentLine.indexOf("v ") > -1) {
            currentLineParsedList = currentLine.replace(/[^0-9-. ]/g, ' ').split(' ').filter(Number);
            vertexList.push(vec3.fromValues(parseFloat(currentLineParsedList[0]) * scaleX, parseFloat(currentLineParsedList[1]) * scaleY, parseFloat(currentLineParsedList[2]) * scaleZ));
            colorList.push(vec4.fromValues(color[0] * Math.random(), color[1] * Math.random(), color[2] * Math.random(), color[3]));

        }
        if (currentLine.indexOf("vt ") > -1) {
            currentLineParsedList = currentLine.replace(/[^0-9-. ]/g, ' ').split(' ').filter(Number);
            textureList.push(vec2.fromValues(currentLineParsedList[0], 1 - currentLineParsedList[1]));
        }
        if (currentLine.indexOf("vn ") > -1) {
            currentLineParsedList = currentLine.replace(/[^0-9-. ]/g, ' ').split(' ').filter(Number);
            normalList.push(vec3.fromValues(currentLineParsedList[0], currentLineParsedList[1], currentLineParsedList[2]));
        }
        //Traitement fes faces "f". De forme a)indexV/indexVT/indexVN ou b)indexV/indexVT ou c)indexV/indexVN
        if (currentLine.indexOf("f ") > -1) {
            currentLineParsedList = currentLine.replace(/[^0-9-. ]/g, ' ').split(' ').filter(Number);
            if (currentLineParsedList.length < 9) { //S'il y a moins de 9 nombres par ligne alors les faces ne sont pas en triplets
                estEnTriplets = false;
            } else {
                estEnTriplets = true
            }

            for (var i = 0; i < currentLineParsedList.length; i++) {
                //Traitement du premier �l�ment de la face
                indicesList.push(currentLineParsedList[i]);
                indicesListVertex.push(currentLineParsedList[i] - 1);

                //Traitement du deuxi�me �l�ment de la face selon situation b) ou c)
                i++;
                if (textureList.length == 0 && estEnTriplets == false) {
                    indicesListNormals.push(currentLineParsedList[i] - 1);
                } else {
                    indicesListTextures.push(currentLineParsedList[i] - 1);
                }

                //Traitement du troisi�me �l�ment de la face s'il existe
                if (estEnTriplets == true) {
                    i++;
                    indicesListNormals.push(currentLineParsedList[i] - 1);
                }
            }
        }
        // console.log(vec3.fromValues(currentLineParsedList[0] / scaleX, currentLineParsedList[1] / scaleY, currentLineParsedList[2] / scaleZ));
    }
    //  console.log(normalList);
    //Triage des coordonn�es dans le m�me ordre que les faces
    var orderedVertexList = [];
    for (var j = 0; j < indicesListVertex.length - 1; j++) {
        orderedVertexList.push(vertexList[indicesListVertex[j]]);
    }
    var orderedTextureList = [];
    for (var j = 0; j < indicesListTextures.length - 1; j++) {
        orderedTextureList.push(textureList[indicesListTextures[j]]);
    }
    var orderedNormalList = [];
    for (var j = 0; j < indicesListNormals.length - 1; j++) {
        orderedNormalList.push(normalList[indicesListNormals[j]]);
    }
    // ----------------------------------------------------------------------------------------//
    // console.log("vert: "+orderedVertexList.length +"text:  "+ orderedTextureList.length + "normal: "+orderedNormalList.length);

    //Remplir la liste des normales et des indices si elle est vide
    //if (orderedNormalList.length == 0) {
    //    for (var n = 0; n < orderedVertexList.length; n++) {
    //        orderedNormalList.push(vec3.fromValues(0,0,0));
    //    }
    //}
    //console.log("vert: " + orderedVertexList.length + "text:  " + orderedTextureList.length + "normal: " + orderedNormalList[3333]);


    var gl = this.gl = APP.WebGLContext;
    var strTextureImage = strTextureOBJ;

    // TTTTTTTTTTTTTTTT    TEXTURE LOAD   TTTTTTTTTTTTTTTTTTTTTT//
    var texture = new APP.Texture({
        textureLocation: texLocation,
        isRepeating: false
    });
    // TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT//


    //Cr�ation de la g�om�trie
    geometry = new APP.Geometry({
        vertices: orderedVertexList,
        // colors: [],
        // indices: [],
        normals: orderedNormalList,
        uvs: orderedTextureList,
        //  drawMode: this.gl.TRIANGLES,
        //  boundingSphere: new APP.Math.Sphere(),
        //  shader: APP.Shaders.defaultShader
    });

    //geometry.computeNormals();

    // Private drawing function.
    this._drawMesh = function(args) {


        if (this.isFrustumCulled(args.frustum)) {

            var shader = args.defaultShader;
            var sampler = 0;

            // Bind uniforms.
            shader.uniforms.setValues({
                'ModelMatrix': this.worldMatrix,
                'NormalMatrix': APP.Math.computeNormalMatrix3(this.worldMatrix)
            });

            //************** SET UP DRAW TEXTURE **********************//

            texture.bind(gl.TEXTURE0, sampler, shader);

            // Draw elements.
            geometry.draw(gl.TRIANGLES);

            //Clean up
            texture.unbind();
        }

    }

    this.draw = function(args) {
        this._drawMesh(args);

        // Call draw on each children.
        _.invoke(this.children, 'draw', args);

    }

}

APP.Mesh = Compose(APP.Object3D, APP.Events, APP.__Mesh);
