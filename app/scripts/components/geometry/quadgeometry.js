// Inherits from Object3D.
// this.attributes contains :
// - pt1, pt2, pt3 and pt4 -> points defining the Quad.
// - color.
// Pass an object to the constructor containing those attributes.
// They will be copied to 'this.attributes'
APP.__QuadGeometry = function() {
    var color = this.attributes.color;
    var pt1 = this.attributes.pt1,
        pt2 = this.attributes.pt2,
        pt3 = this.attributes.pt3,
        pt4 = this.attributes.pt4;

    // Perform frustum culling on this object.
    this.frustumCull = true;

    this.geometry = new APP.Geometry({
        vertices: [pt1, pt2, pt3, pt4],
        colors: color ? [color, color, color, color] : [],
        indices: [0, 1, 2, 2, 1, 3]
    });

    this.geometry.computeNormals();

    var gl = this.gl = APP.WebGLContext;

    // Private drawing function.
    this._drawQuad = function(args) {

        var frustum = args.frustum;

        if (this.isFrustumCulled(frustum)) {

            // Compute normal matrix.
            var normalMatrix = APP.Math.computeNormalMatrix3(this.worldMatrix);

            var shader = args.defaultShader;

            // Bind uniform model matrix.
            shader.uniforms['ModelMatrix'].value = this.worldMatrix;
            shader.uniforms['UseTextures'].value = false; // Use color
            shader.uniforms['UseLighting'].value = true; // Use lighting
            shader.uniforms['NormalMatrix'].value = normalMatrix;

            // Draw elements.
            this.geometry.draw();

        }

    }

    this.draw = function(args) {
        this._drawQuad(args);

        // Call draw on each children.
        _.invoke(this.children, 'draw', args);
    }

}

APP.QuadGeometry = Compose(APP.Object3D, APP.Events, APP.__QuadGeometry);
