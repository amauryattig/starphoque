// Inherits from Object3D.
// this.attributes contains :
// - pt1, pt2, pt3, pt4, pt11, pt22, pt33 and pt44 -> points defining the Cube.
// - color.
// Pass an object to the constructor containing those attributes.
// They will be copied to 'this.attributes'
APP.__CubeGeometry = function() {
    var color1 = this.attributes.color1;
    var color2 = this.attributes.color2;
    var color3 = this.attributes.color3;
    var color4 = this.attributes.color4;
    var color5 = this.attributes.color5;
    var color6 = this.attributes.color6;

    var pt1 = this.attributes.pt1,
        pt2 = this.attributes.pt2,
        pt3 = this.attributes.pt3,
        pt4 = this.attributes.pt4,
        pt11 = this.attributes.pt11,
        pt22 = this.attributes.pt22,
        pt33 = this.attributes.pt33,
        pt44 = this.attributes.pt44;


    this.geometry = new APP.Geometry({
        vertices: [pt1, pt2, pt3, pt4, pt11, pt22, pt33, pt44],
        colors: color1 ? [color1, color2, color2, color3, color3, color4, color4, color5, color5, color6] : [],
        indices: [0, 1, 2, 2, 1, 3,
            3, 2, 4, 4, 2, 5,
            4, 5, 6, 6, 5, 7,
            7, 0, 6, 6, 0, 1,
            1, 6, 3, 3, 6, 4,
            0, 2, 7, 7, 2, 5
        ]
    });

    var gl = this.gl = APP.WebGLContext;

    // Private drawing function.
    this._drawCube = function(args) {

        if (this.isFrustumCulled(args.frustum)) {

            var shader = args.defaultShader;

            // Bind uniform model matrix.
            shader.uniforms['ModelMatrix'].value = this.worldMatrix;
            shader.uniforms['UseTextures'].value = false;
            shader.uniforms['UseLighting'].value = false;

            // Draw elements.
            this.geometry.draw(gl.TRIANGLES);
        }
    }

    this.draw = function(args) {
        this._drawCube(args);

        // Call draw on each children.
        _.invoke(this.children, 'draw', args);
    }

}

APP.CubeGeometry = Compose(APP.Object3D, APP.Events, APP.__CubeGeometry);
