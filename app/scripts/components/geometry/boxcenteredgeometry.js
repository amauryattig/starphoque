APP.__BoxCenteredGeometry = function(params) {

    var defaultColor = vec4.fromValues(0.0, 0.0, 0.0, 0.0);

    var defaults = {
        center: vec3.fromValues(0, 0, 0),
        width: 1,
        height: 1,
        depth: 1,
        color: defaultColor, // No color by default.
        // strTextureImage: APP.Base64GroundTexture.groundtexture["rock"],
        texture: undefined,
        textureLocation: {
            key: 'skyscraper1',
            type: 'groundtexture',
            file: 'Base64GroundTexture'
        },
        geometry: undefined,
        material:undefined,
    };

    //var ptcenter = vec3.create();
    //this.transform.position.xyz = this.attributes.center;
    //var width = this.attributes.width; //x
    //var height = this.attributes.height; //y
    //var depth = this.attributes.depth; //z

    var _attrs = _.extend(defaults, _.clone(params));


    //Vertices
    var center = _attrs.center;
    var height = _attrs.height;
    var width = _attrs.width;
    var depth = _attrs.depth;
    var color = _attrs.color;
    var textureLocation = _attrs.textureLocation;
    var texture = _attrs.texture;
    this.material = _attrs.material;
    var geometry = _attrs.geometry;

    // Create new geometry if undefined.
    if (_.isUndefined(geometry)) {

        // <f = front | b = back> <b = bottom | t = top> <l = left | r = right>
        var halfw = width / 2,
            halfh = height / 2,
            halfd = depth / 2;

        var t0 = vec2.fromValues(1, 0);
        var t1 = vec2.fromValues(0, 0);
        var t2 = vec2.fromValues(1, 1);
        var t3 = vec2.fromValues(0, 1);


        var fbl = vec3.fromValues(center[0] + halfw, center[1] - halfh, center[2] - halfd);
        var fbr = vec3.fromValues(center[0] - halfw, center[1] - halfh, center[2] - halfd);
        var ftl = vec3.fromValues(center[0] + halfw, center[1] + halfh, center[2] - halfd);
        var ftr = vec3.fromValues(center[0] - halfw, center[1] + halfh, center[2] - halfd);
        var bbl = vec3.fromValues(center[0] + halfw, center[1] - halfh, center[2] + halfd);
        var bbr = vec3.fromValues(center[0] - halfw, center[1] - halfh, center[2] + halfd);
        var btl = vec3.fromValues(center[0] + halfw, center[1] + halfh, center[2] + halfd);
        var btr = vec3.fromValues(center[0] - halfw, center[1] + halfh, center[2] + halfd);


        var frontBL = vec3.clone(fbl), // 0
            frontBR = vec3.clone(fbr), // 1
            frontTL = vec3.clone(ftl), // 2
            frontTR = vec3.clone(ftr); // 3

        var topBL = vec3.clone(ftl), // 4
            topBR = vec3.clone(ftr), // 5
            topTL = vec3.clone(btl), // 6 
            topTR = vec3.clone(btr); // 7

        var leftBL = vec3.clone(bbl), // 8
            leftBR = vec3.clone(fbl), // 9
            leftTL = vec3.clone(btl), // 10
            leftTR = vec3.clone(ftl); // 11

        var rightBL = vec3.clone(fbr), // 12
            rightBR = vec3.clone(bbr), // 13
            rightTL = vec3.clone(ftr), // 14
            rightTR = vec3.clone(btr); // 15

        var bottomBL = vec3.clone(bbl), // 16
            bottomBR = vec3.clone(bbr), // 17
            bottomTL = vec3.clone(fbl), // 18
            bottomTR = vec3.clone(fbr); // 19

        var backBL = vec3.clone(bbr), // 20
            backBR = vec3.clone(bbl), // 21
            backTL = vec3.clone(btr), // 22
            backTR = vec3.clone(btl); // 23

        var indices = _.chain([0, 1, 2, 3, 4, 5])
            .reduce(function(acc, face, i) {

                var baseIndex = (face * 4);

                // CW (Clock wise) winding order for vertices
                // for each face
                var f1p1 = baseIndex,
                    f1p2 = baseIndex + 2,
                    f1p3 = baseIndex + 3,
                    f2p1 = baseIndex,
                    f2p2 = baseIndex + 3,
                    f2p3 = baseIndex + 1;
                var f1 = [f1p1, f1p2, f1p3],
                    f2 = [f2p1, f2p2, f2p3];

                acc.push(f1, f2);

                return acc;
            }, [])
            .flatten()
            .value();

        var self = this;

        var colors = [];

        _.times(24, function() {
            colors.push(color);
        });

        verticesList = [
            frontBL, frontBR, frontTL, frontTR,
            topBL, topBR, topTL, topTR,
            leftBL, leftBR, leftTL, leftTR,
            rightBL, rightBR, rightTL, rightTR,
            bottomBL, bottomBR, bottomTL, bottomTR,
            backBL, backBR, backTL, backTR
        ];
        //UVS
        var uvList = [];
        //t0, t2, t1, t3,
        //t1, t3, t1, t3,
        //t0, t2, t1, t3,
        //t0, t2, t1, t3,
        //t0, t2, t0, t2,
        //t0, t2, t1, t3];
        for (var i = 0; i < verticesList.length / 4; i++) {
            uvList.push(vec2.fromValues(t0[0] + 2 * i, t0[1] + 2 * i));
            uvList.push(vec2.fromValues(t1[0] + 2 * i, t1[1] + 2 * i));
            uvList.push(vec2.fromValues(t2[0] + 2 * i, t2[1] + 2 * i));
            uvList.push(vec2.fromValues(t3[0] + 2 * i, t3[1] + 2 * i));
        }

        //normals
        var normalList = []
        for (var i = 0; i < verticesList.length / 4; i++) {
            normalList.push(vec3.fromValues(0, 0, 0));
            normalList.push(vec3.fromValues(0, 0, 0));
            normalList.push(vec3.fromValues(0, 0, 0));
            normalList.push(vec3.fromValues(0, 0, 0));
        }

        this.geometry = new APP.Geometry({

            vertices: verticesList,

            colors: colors,

            indices: indices,
            normals: normalList,
            uvs: uvList


        });

        // console.log(uvList);
        this.geometry.computeNormals();

    } else {

        // Reuse geometry.
        this.geometry = geometry;

        this.transform.position.xyz = center;
        this.transform.scale.xyz = vec3.fromValues(width, height, depth);

    }


    var gl = this.gl = APP.WebGLContext;

    if (_.isUndefined(texture)) {
        // TTTTTTTTTTTTTTTT    TEXTURE LOAD   TTTTTTTTTTTTTTTTTTTTTT//
        texture = new APP.Texture({
            textureLocation: textureLocation,
            isRepeating: true
        });
        // texture = textureLoader.initTexture();
        // TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT//
    }

    // Private drawing function.
    this._drawBoxCenteredGeometry = function(args) {

        if (this.isFrustumCulled(args.frustum)) {


            
            var normalMatrix = APP.Math.computeNormalMatrix3(this.worldMatrix);

            if (_.isUndefined(this.material)) {

                var shader = args.defaultShader;
                var sampler = 0;

                //************** SET UP DRAW TEXTURE **********************//
                texture.bind(gl.TEXTURE0, 0, shader);

                // Bind uniforms.
                shader.uniforms['ModelMatrix'].value = this.worldMatrix;
                // shader.uniforms['UseTextures'].value = true; // Use color
                shader.uniforms['UseLighting'].value = true; // Use lighting
                shader.uniforms['NormalMatrix'].value = normalMatrix;
                // shader.uniforms['texture_diffuse'].value = 0;

                //*********************************************************//

                // Draw elements.
                this.geometry.draw(gl.TRIANGLES);

                //Clean up
                texture.unbind();

            } else {

                this.material.bind({
                    'ModelMatrix': this.worldMatrix,
                    'ViewMatrix': args.viewMatrix,
                    'ProjMatrix': args.projMatrix,
                    'UseLighting': true,
                    'NormalMatrix': normalMatrix,
                    'd_ambient': args.ambientLightColor,
                    'd_diffuse': args.directionalLight.color,
                    'd_specular': args.directionalLight.specular,
                    'd_direction': args.directionalLight.direction
                });


                // Draw elements.
                this.geometry.draw(gl.TRIANGLES);

                this.material.unbind(args.defaultShader);

            }

        }
        
    }

    this.draw = function(args) {
        this._drawBoxCenteredGeometry(args);

        // Call draw on each children.
        _.invoke(this.children, 'draw', args);
    }
}


APP.BoxCenteredGeometry = Compose(APP.Object3D, APP.Events, APP.__BoxCenteredGeometry);
