// Inherit from Object3D.
// this.attributes contains :
// - pt1, pt2 and pt3 -> points defining the triangle.
// - color/texture.
// Pass an object to the constructor containing those attributes.
// They will be copied to 'this.attributes'
APP.__Triangle = function() {


    // Private drawing function.
    this._drawTriangle = function() {
        // Select vao as active object.  
        this.ext.bindVertexArrayOES(this.vao);
        this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.vertexBuffer);

        // Compute normal matrix.
        // var normalMatrix = APP.Math.computeNormalMatrix3(this.worldMatrix);

        var shader = args.defaultShader;

        // Bind uniform model matrix.
        shader.uniforms['ModelMatrix'].value = this.worldMatrix;
        shader.uniforms['UseTextures'].value = false; // Use color
        shader.uniforms['UseLighting'].value = false; // Use lighting
        // shader.uniforms['NormalMatrix'].value = normalMatrix;

        // Draw the triangle.
        this.gl.drawArrays(this.gl.TRIANGLES, 0, 3);

        // Unbind vao.  
        this.ext.bindVertexArrayOES(null);
    }

    this.draw = function(args) {
        this._drawTriangle(args);

        // Call draw on each children.
        _.invoke(this.children, 'draw', args);
    }


    /**
     * Jax.Geometry.Triangle#intersect(O, D, cp[, segmax]) -> Boolean
     * - O (vec3): origin
     * - D (vec3): direction
     * - cp (vec4): collision point [xyz] and distance [w] (output)
     * - segmax (Number): the maximum length of the ray; optional
     *
     * Tests for intersection with a ray, given an origin (O) and
     * direction (D). The +cp+ array receives the exact X, Y, Z position of the
     * collision; its W (fourth) element contains the distance from the origin
     * to the point of the collision, relative to the magnitude of D.
     * Allows testing against a finite segment by specifying the maximum length of
     * the ray in +segmax+, also relative to the magnitude of D.
     **/
    this.intersectRay = function(O, D, cp, segmax) {
        var p = this._p = this._p || new Jax.Geometry.Plane();
        p.set(this.a, this.b, this.c);
        var denom = vec3.dot(p.normal, D);
        if (Math.abs(denom) < Math.EPSILON) return false;
        var t = -(p.d + vec3.dot(p.normal, O)) / denom;
        if (t <= 0) return false;
        if (segmax != undefined && t > segmax) return false;
        // cp = O + t*D
        vec3.set(D, cp);
        vec3.scale(cp, t, cp);
        vec3.add(O, cp, cp);
        if (this.pointInTri(cp)) {
            cp[3] = t;
            return true;
        }
        return false;
    }



}
APP.Triangle = Compose(APP.Object3D, APP.__Triangle, function() {



    // Initialization. This function is executed along with the ctor function.

    this.gl = APP.WebGLContext;
    var positionAttrib = APP.ShaderProgram.vertexPositionAttribute;

    // Get the Vertex Array Object extension and create/bind a VAO  
    this.ext = this.gl.getExtension("OES_vertex_array_object");
    this.vao = this.ext.createVertexArrayOES();

    // Select vao as active object.  
    this.ext.bindVertexArrayOES(this.vao);

    // Create buffers. 
    this.vertexBuffer = this.gl.createBuffer();
    this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.vertexBuffer);
    // Get the vertices from the three vec3 objects and concatenate inside one array.
    var vertices = APP.Utils.Float32ConcatMultiple([this.attributes.pt1, this.attributes.pt2, this.attributes.pt3]);
    this.gl.bufferData(this.gl.ARRAY_BUFFER, vertices, this.gl.STATIC_DRAW);

    // Set pointer to vertex attribute.
    this.gl.vertexAttribPointer(positionAttrib, 3, this.gl.FLOAT, false, 0, 0);
    this.gl.enableVertexAttribArray(positionAttrib);

    // Unbind vao.  
    this.ext.bindVertexArrayOES(null);


});
