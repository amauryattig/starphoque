APP.__IsocahedronGeometry = function (params) {
    var defaultColor = vec4.fromValues(0.0, 0.0, 0.0, 0.0);
    var defaults = {
        center: vec3.fromValues(0, 0, 0),
        scale: 0.8,
        colorList: [defaultColor], // No color by default.
        isRepeatingTexture: false,
        textureLocation: {
            key: 'energyrock',
            type: 'groundtexture',
            file: 'Base64GroundTexture'
        },
        texture: undefined,
        material: undefined,
        geometry: undefined
    };

    var _attrs = _.extend(defaults, _.clone(params));
    this.isRepeatingTexture = _attrs.isRepeatingTexture;
    this.textureLocation = _attrs.textureLocation;
    var texture = _attrs.texture;
    this.material = _attrs.material;
    var geometry = _attrs.geometry;

    if (_.isUndefined(geometry)) {
    //Vertices
    var center = _attrs.center;
    var scale = _attrs.scale;
    var colorList = _attrs.colorList;
    var strTextureImage = _attrs.strTextureImage;


    var t = (1 + Math.sqrt(5)) / 2;

    var X =0.525731112119133606 ;
    var Z =0.850650808352039932;
 
    var v0 = vec3.fromValues(-X* scale + center[0], 0.0* scale + center[1], Z* scale + center[2]);
    var v1 = vec3.fromValues(X* scale + center[0], 0.0* scale + center[1], Z* scale + center[2]);
    var v2 = vec3.fromValues(-X* scale + center[0], 0.0* scale + center[1], -Z* scale + center[2]);
    var v3 = vec3.fromValues(X* scale + center[0], 0.0* scale + center[1], -Z* scale + center[2]);
    var v4 = vec3.fromValues(0.0* scale + center[0], Z* scale + center[1], X* scale + center[2]);
    var v5 = vec3.fromValues(0.0* scale + center[0], Z* scale + center[1], -X* scale + center[2]);
    var v6 = vec3.fromValues(0.0* scale + center[0], -Z* scale + center[1], X* scale + center[2]);
    var v7 = vec3.fromValues(0.0* scale + center[0], -Z* scale + center[1], -X* scale + center[2]);
    var v8 = vec3.fromValues(Z* scale + center[0], X* scale + center[1], 0.0* scale + center[2]);
    var v9 = vec3.fromValues(-Z* scale + center[0], X* scale + center[1], 0.0* scale + center[2]);
    var v10 = vec3.fromValues(Z* scale + center[0], -X* scale + center[1], 0.0* scale + center[2]);
    var v11 = vec3.fromValues(-Z* scale + center[0], -X* scale + center[1], 0.0* scale + center[2]);

    // textures
    var t0 = vec2.fromValues(0, 0);
    var t1 = vec2.fromValues(0, 0.3);
    var t2 = vec2.fromValues(0, 0.6);
    var t3 = vec2.fromValues(0, 0.9);
    var t4 = vec2.fromValues(0.3, 0);
    var t5 = vec2.fromValues(0.3, 0.3);
    var t6 = vec2.fromValues(0.3, 0.6);
    var t7 = vec2.fromValues(0.3, 0.9);
    var t8 = vec2.fromValues(0.6, 0);
    var t9 = vec2.fromValues(0.6, 0.3);
    var t10 = vec2.fromValues(0.6, 0.6);
    var t11 = vec2.fromValues(0.6, 0.9);


    this.geometry = new APP.Geometry({

        
    vertices : [v0,v1,v2,v3,v4,v5,v6,v7,v8,v9,v10,v11],
    colors: colorList,
    indices:[

         0,4,1, 0,9,4, 9,5,4, 4,5,8, 4,8,1,
      8,10,1, 8,3,10, 5,3,8, 5,2,3, 2,7,3,
      7,10,3, 7,6,10, 7,11,6, 11,0,6, 0,1,6,
      6,1,10, 9,0,11, 9,11,2, 9,2,5, 7,2,11

    ],

    uvs:  [t0,t1,t2,t3,t4,t5,t6,t7,t8,t9,t10,t11]
        
        
    });

    this.geometry.computeNormals();
    var gl = this.gl = APP.WebGLContext;

    if (_.isUndefined(this.material) && _.isUndefined(texture)) {
        // TTTTTTTTTTTTTTTT    TEXTURE LOAD   TTTTTTTTTTTTTTTTTTTTTT//
        texture = new APP.Texture({
            textureLocation: this.textureLocation,
            isRepeating: this.isRepeatingTexture
        });
        // TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT//
    }
    } else {

        // Reuse geometry.
        this.geometry = geometry;

        this.transform.position.xyz = center;
        this.transform.scale.xyz = vec3.fromValues(scale * this.transform.scale.x, scale * this.transform.scale.y, scale * this.transform.scale.z);

    }
    // Private drawing function.
    this._drawIsocahedronGeometry = function (args) {

        if (this.isFrustumCulled(args.frustum)) {


            var normalMatrix = APP.Math.computeNormalMatrix3(this.worldMatrix);

            if (_.isUndefined(this.material)) {

                var shader = args.defaultShader;
                var sampler = 0;

                //************** SET UP DRAW TEXTURE **********************//
                texture.bind(gl.TEXTURE0, 0, shader);

                // Bind uniforms.
                shader.uniforms['ModelMatrix'].value = this.worldMatrix;
                // shader.uniforms['UseTextures'].value = true; // Use color
                shader.uniforms['UseLighting'].value = true; // Use lighting
                shader.uniforms['NormalMatrix'].value = normalMatrix;
                // shader.uniforms['texture_diffuse'].value = 0;

                //*********************************************************//

                // Draw elements.
                this.geometry.draw(gl.TRIANGLES);

                //Clean up
                texture.unbind();

                if (_attrs.drawControlPoints) {
                    self._controlPointsGeometry.draw(gl.LINE_STRIP);
                }

            } else {

                this.material.bind({
                    'ModelMatrix': this.worldMatrix,
                    'ViewMatrix': args.viewMatrix,
                    'ProjMatrix': args.projMatrix,
                    'UseLighting': true,
                    'NormalMatrix': normalMatrix,
                    'd_ambient': args.ambientLightColor,
                    'd_diffuse': args.directionalLight.color,
                    'd_specular': args.directionalLight.specular,
                    'd_direction': args.directionalLight.direction
                });


                // Draw elements.
                this.geometry.draw(gl.TRIANGLES);

                this.material.unbind(args.defaultShader);

            }
        }
    }

    this.draw = function (args) {
        this._drawIsocahedronGeometry(args);

        // Call draw on each children.
        _.invoke(this.children, 'draw', args);
    }
}


APP.IsocahedronGeometry = Compose(APP.Object3D, APP.Events, APP.__IsocahedronGeometry);










