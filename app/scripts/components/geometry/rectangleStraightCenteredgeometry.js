// Inherit from Object3D.
// this.attributes contains :
// - centerPosNW,width,height -> parameters defining the Rectrangle.
// - color/texture.
// Pass an object to the constructor containing those attributes.
// They will be copied to 'this.attributes'


APP.__RectangleStraightCenteredGeometry = function(params) {
    var defaultColor = vec4.fromValues(0.0, 0.0, 0.0, 0.0);
    var defaults = {
        center: vec3.fromValues(0, 0, 0),
        width: 1,
        height: 1,
        colorList: [defaultColor], // No color by default.
        isRepeatingTexture: false,
        textureLocation: {
            key: 'rock',
            type: 'groundtexture',
            file: 'Base64GroundTexture'
        },
        texture: undefined,
        material: undefined,
        geometry: undefined
    };

    var _attrs = _.extend(defaults, _.clone(params));
    this.isRepeatingTexture = _attrs.isRepeatingTexture;
    this.textureLocation = _attrs.textureLocation;
    var texture = _attrs.texture;
    this.material = _attrs.material;
    var geometry = _attrs.geometry;

    if (_.isUndefined(geometry)) {
    //Vertices
    var center = _attrs.center;
    var height = _attrs.height;
    var width = _attrs.width;
    var colorList = _attrs.colorList;



    var v0 = vec3.fromValues(center[0] + width / 2, center[1] + height / 2, center[2]);
    var v1 = vec3.fromValues(center[0] - width / 2, center[1] + height / 2, center[2]);
    var v2 = vec3.fromValues(center[0] + width / 2, center[1] - height / 2, center[2]);
    var v3 = vec3.fromValues(center[0] - width / 2, center[1] - height / 2, center[2]);


    var t0 = vec2.fromValues(0, 0);
    var t1 = vec2.fromValues(0, 1);
    var t2 = vec2.fromValues(1, 0);
    var t3 = vec2.fromValues(1, 1);


    this.geometry = new APP.Geometry({
        vertices: [v0, v1, v2, v3],
        //   colors: colorList,
        colors: [vec4.fromValues(0, 0, 0, 0)],
        indices: [0, 1, 2, 1, 2, 3],
        normals: [vec3.fromValues(0, 0, 1),
            vec3.fromValues(0, 0, 1),
            vec3.fromValues(0, 0, 1),
            vec3.fromValues(0, 0, 1)
        ],
        uvs: [t2,
            t0,
            t3,
            t1,
        ]

    });

    //  this.geometry.computeNormals();
    var gl = this.gl = APP.WebGLContext;

    if (_.isUndefined(this.material) && _.isUndefined(texture)) {
        // TTTTTTTTTTTTTTTT    TEXTURE LOAD   TTTTTTTTTTTTTTTTTTTTTT//
        texture = new APP.Texture({
            textureLocation: this.textureLocation,
            isRepeating: this.isRepeatingTexture
        });
        // TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT//
    }
    } else {

        // Reuse geometry.
        this.geometry = geometry;

        this.transform.position.xyz = center;
        this.transform.scale.xyz = vec3.fromValues(width, height, 0);

    }
    // Private drawing function.
    this.__RectangleStraightCenteredGeometry = function(args) {

        if (this.isFrustumCulled(args.frustum)) {


            var normalMatrix = APP.Math.computeNormalMatrix3(this.worldMatrix);

            if (_.isUndefined(this.material)) {

                var shader = args.defaultShader;
                var sampler = 0;

                //************** SET UP DRAW TEXTURE **********************//
                texture.bind(gl.TEXTURE0, 0, shader);

                // Bind uniforms.
                shader.uniforms['ModelMatrix'].value = this.worldMatrix;
                // shader.uniforms['UseTextures'].value = true; // Use color
                shader.uniforms['UseLighting'].value = true; // Use lighting
                shader.uniforms['NormalMatrix'].value = normalMatrix;
                // shader.uniforms['texture_diffuse'].value = 0;

                //*********************************************************//

                // Draw elements.
                this.geometry.draw(gl.TRIANGLES);

                //Clean up
                texture.unbind();

                if (_attrs.drawControlPoints) {
                    self._controlPointsGeometry.draw(gl.LINE_STRIP);
                }

            } else {

                this.material.bind({
                    'ModelMatrix': this.worldMatrix,
                    'ViewMatrix': args.viewMatrix,
                    'ProjMatrix': args.projMatrix,
                    'UseLighting': true,
                    'NormalMatrix': normalMatrix,
                    'd_ambient': args.ambientLightColor,
                    'd_diffuse': args.directionalLight.color,
                    'd_specular': args.directionalLight.specular,
                    'd_direction': args.directionalLight.direction
                });


                // Draw elements.
                this.geometry.draw(gl.TRIANGLES);

                this.material.unbind(args.defaultShader);

            }
        }
    }

    this.draw = function(args) {
        this.__RectangleStraightCenteredGeometry(args);

        // Call draw on each children.
        _.invoke(this.children, 'draw', args);
    }

}


APP.RectangleStraightCenteredGeometry = Compose(APP.Object3D, APP.Events, APP.__RectangleStraightCenteredGeometry);
