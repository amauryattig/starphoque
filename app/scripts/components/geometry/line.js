// Inherit from Object3D.
// this.attributes contains :
// - pt1 and pt2 -> points defining the line.
// - color eventually.
// Pass an object to the constructor containing those attributes.
// They will be copied to 'this.attributes'

var before = Compose.before;

APP.__Line = function() {
    // Private drawing function.
    this._drawLine = function(args) {
            // Select vao as active object.  
            this.ext.bindVertexArrayOES(this.vao);
            this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.vertexBuffer);

            // Set line width with this command does nothing...
            // this.gl.lineWidth(10);

            var shader = args.defaultShader;

            // Bind uniform model matrix.
            shader.uniforms['ModelMatrix'].value = this.worldMatrix;
            shader.uniforms['UseTextures'].value = false;
            shader.uniforms['UseLighting'].value = false;

            // Draw the line.
            this.gl.drawArrays(this.gl.LINES, 0, 2);

            // Unbind vao.  
            this.ext.bindVertexArrayOES(null);
        },

        this.draw = function(args) {
            this._drawLine(args);

            // Call draw on each children.
            _.invoke(this.children, 'draw');
        }
}

APP.Line = Compose(APP.Object3D, APP.__Line, function() {
    // Initialization. This function is executed along with the ctor function.

    this.gl = APP.WebGLContext;
    var positionAttrib = APP.ShaderProgram.vertexPositionAttribute;

    // Get the Vertex Array Object extension and create/bind a VAO  
    this.ext = this.gl.getExtension("OES_vertex_array_object");
    this.vao = this.ext.createVertexArrayOES();

    // Select vao as active object.  
    this.ext.bindVertexArrayOES(this.vao);

    // Create buffers. 
    this.vertexBuffer = this.gl.createBuffer();
    this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.vertexBuffer);
    // Get the vertices from the two vec3 objects and concatenate inside one array.
    var vertices = APP.Utils.Float32Concat(this.attributes.pt1, this.attributes.pt2);
    this.gl.bufferData(this.gl.ARRAY_BUFFER, vertices, this.gl.STATIC_DRAW);

    // Set pointer to vertex attribute.
    this.gl.vertexAttribPointer(positionAttrib, 3, this.gl.FLOAT, false, 0, 0);
    this.gl.enableVertexAttribArray(positionAttrib);

    // Unbind vao.  
    this.ext.bindVertexArrayOES(null);

});
