// Base class for all 3d objects.
// Mixin the events module.

APP.__Object3D = Compose(function(attributes) {

    var defaults = {
        position: new APP.Math.Vec3(),
        rotation: new APP.Math.Euler(), // Euler angles
        quat: quat.create(),
        scale: new APP.Math.Vec3(1.0, 1.0, 1.0),
        isVisible: true,
        frustumCull: false, // Do not run frustum cull algorithm against this object by defautlt.
        geometry: undefined
    };

    // All object's attributes.
    var _attrs = APP.Utils.copyToAndUpdate(defaults, attributes);

    Object.defineProperty(this, 'attributes', {
        get: function() {
            return _attrs;
        }
    });

    var self = this;

    _.each(['isVisible', 'frustumCull'], function(prop) {
        Object.defineProperty(self, prop, {
            get: function() {
                return _attrs[prop];
            },
            set: function(value) {
                _attrs[prop] = value;
            }
        });
    });

    // Pick only the transformation related attributes and put them in this.transform.
    this.transform = _.pick(_attrs, ['position', 'rotation', 'quat', 'scale']);

    // Shortcut to the method in Utils.Math
    var composeMats = APP.Math.composeMatrices;

    // Those matrices are like a matrix strack :
    // the world matrix compose all the transformations from the ancestors
    // of the current object, and the local matrix represents the local 
    // transformations.
    this.worldMatrix = mat4.identity(mat4.create());
    this.localMatrix = mat4.identity(mat4.create());


    // Unique id for the object.
    var _id = {
        id: _.uniqueId()
    };
    // Make the object read only.
    Object.freeze(_id);

    Object.defineProperty(this, 'id', {
        get: function() {
            return _id.id;
        }
    });

    // The scene graph is an n-ary tree, but with reference
    // to the parent in the actual data structure.
    this.parent = undefined;
    this.children = [];

    this.add = function(child) {
        this.children.push(child);
        // Remove child from its actual parent.
        if (!_.isUndefined(child.parent)) child.parent.removeChildWithId(child.id);
        // Assign |this| as new parent of child.
        child.parent = this;
        // Chainable call.
        return this;
    }

    this.removeChildWithId = function(id) {
        // Split children into 2 partitions : one with the child
        // and the other with the rest.
        var partition = _.partition(this.children, function(child) {
            return child.id == id;
        });

        if (partition[0].length > 0) {
            // Child is found;
            // remove parent from the child.
            partition[0][0].parent = undefined;
            this.children = partition[1];
        }
        return this;
    }

    // Compose all object transformations in the local matrix.
    this.updateLocalMatrix = function() {
        this.localMatrix =
            composeMats(this.transform.quat, this.transform.scale, this.transform.position);
        return this;
    }

    // Compose the world matrix from the parent's world matrix and the local matrix.
    // The call is forwarded to children.
    this.updateWorldMatrix = function() {
        this.updateLocalMatrix();

        if (_.isUndefined(this.parent)) {
            // This is the root of the tree.
            // The local transformations are equivalent to the world tranfosmations.
            this.worldMatrix = this.localMatrix;
        } else {
            // Update the world matrix by composing the local matrix with the
            // parent's world matrix, into the object's world matrix.
            mat4.mul(this.worldMatrix, this.parent.worldMatrix, this.localMatrix);
        }

        // Update children too.
        _.invoke(this.children, 'updateWorldMatrix');

        return this;
    }

    this.traverse = function(callback) {
        callback(this);

        _.each(this.children, function(c) {
            c.traverse(callback);
        });
    }

    this.isFrustumCulled = function(frustum) {
        if (this.frustumCull && this.isVisible && !_.isUndefined(frustum) && !_.isUndefined(this.geometry) && !_.isUndefined(this.geometry.boundingSphere)) {

                var sphere = this.geometry.boundingSphere.clone();
                // We need to apply the object's tranformations
                // to its bounding sphere in order to detect properly
                // if the sphere is in the frustum.
                sphere.applyTransformations(this.worldMatrix);

                return frustum.containsSphere(sphere);

        } else {
            // By default the object is considered inside the frustum.
            return true;
        }
    }

    this.draw = function(args) {
        // Draw nothing by default.
        // Call draw on each children.
        _.invoke(this.children, 'draw', args);
    }

});

APP.Object3D = Compose(APP.__Object3D, APP.Events, function() {
    var self = this;

    // The object listen to changes in the rotation or quat and sync both.
    this.listenTo(this.transform.rotation, 'change', function() {
        self.transform.quat = APP.Math.makeQuatFromEuler(self.transform.rotation);
    });

    this.listenTo(this.transform.quat, 'change', function() {
        self.transform.rotation = APP.Math.makeEulerFromQuat(self.transform.quat);
    });
});
