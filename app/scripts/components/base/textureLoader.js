APP.textureCache = {};

APP.TextureLoader = function(params) {

    var gl = this.gl = APP.WebGLContext;

    var defaults = {
        texture: undefined,
        // image: new Image(),
        isRepeating: true,
        textureBase64String: undefined,
        textureLocation: {
            key: 'grass',
            type: 'groundtexture',
            file: 'Base64GroundTexture'
        }, //APP.Base64GroundTexture.groundtexture["grass"]
        noCache: false,
        generateMipmaps: true
    };

    //Parameters
    var _attrs = _.extend(defaults, _.clone(params));

    // var texture = _attrs.texture;
    // var image = _attrs.image;
    var isRepeating = _attrs.isRepeating;
    var textureLocation = _attrs.textureLocation;
    var texString = _attrs.textureBase64String;
    var noCache = _attrs.noCache;


    //############################  FONCTIONS INITIALISER LES TEXTURES ##################################//

    var self = this;

    function handleLoadedTexture(tex) {
        console.log('image loaded.');

        // Create a tex.
        gl.bindTexture(gl.TEXTURE_2D, tex);

        if (_attrs.generateMipmap) gl.generateMipmap(gl.TEXTURE_2D);

        gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, false);
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, tex.image);

        if (isRepeating == true) {
            // Set the parameters so we can render any size image.

            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.REPEAT);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.REPEAT);
        } else {
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
        }

        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);

        gl.bindTexture(gl.TEXTURE_2D, null);

        if (_.isUndefined(texString) && !_.isUndefined(textureLocation)) {

            // Remove base 64 string from memory. Not needed anymore.
            var file = textureLocation.file,
                type = textureLocation.type,
                key = textureLocation.key;

            delete APP[file][type][key];

        }

    }

    function getImageKey(texLocation) {
        return [
            texLocation.file,
            texLocation.type,
            texLocation.key
        ].join('.');
    }

    this.initTexture = function() {

        var imageKey;
        var texture;

        if (_.isObject(textureLocation)) imageKey = getImageKey(textureLocation);

        if (noCache || _.isUndefined(APP.textureCache[imageKey])) {

            // Not in cache
            texture = gl.createTexture();

            // use cyan as the default color when waiting for texture to load.
            var initialColor = new Uint8Array([0, 255, 255, 255]);

            gl.bindTexture(gl.TEXTURE_2D, texture);
            gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, 1, 1, 0,
                gl.RGBA, gl.UNSIGNED_BYTE, initialColor);

            gl.bindTexture(gl.TEXTURE_2D, null);


            texture.image = new Image();
            texture.isLoaded = false;

            texture.image.onload = function() {
                handleLoadedTexture(texture);
                texture.isLoaded = true;
            }

            if (!_.isUndefined(texString)) {

                texture.image.src = texString;

            } else {

                var file = textureLocation.file,
                    type = textureLocation.type,
                    key = textureLocation.key;

                texture.image.src = APP[file][type][key];

            }

            if (!noCache) {

                APP.textureCache[imageKey] = texture;

            }


        } else {

            texture = APP.textureCache[imageKey];

        }

        return texture;

    }

}
