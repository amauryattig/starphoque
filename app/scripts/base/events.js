// Simple events module that can be mixed in any object.
// Provide pub/sub functionnality.
// Loosely based on BackboneJS events functionnality.

APP.Events = function() {
    var self = this;

    this._events = {};

    // Subscribe to an event.
    this.listenTo = function(obj, eventName, callback) {
        if (_.isUndefined(obj) ||  _.isUndefined(obj._events)) return;

        // Check if there is an event with this name registered.
        // If not, create an array for the entry 'event' in the _events object.
        // The variable events reference this array.
        var events = obj._events[eventName] || (obj._events[eventName] = []);

        events.push({
            listener: self,
            callback: _.bind(callback, self), // Bind the |this| object of the callback to 'self'.
        });

        return this;
    }

    this.stopListeningTo = function(obj, eventName) {
    	var events = obj._events[eventName];
    	if (!_.isUndefined(events)) {
    		// Remove all the event objects from obj where the listener is the current object.
			obj._events[eventName] = _.reject(events, function(e) {
				return e.listener == self;
			});
    	}

    	return this;
    }

    // Publish an event.
    this.trigger = function(name, args) {
    	// Get listeners of the event 'name'.
        var listeners = this._events[name];
        // For each listener, execute the callback.
        _.each(listeners, function(l) {
            l.callback(args);
        });

        return this;
    }

    this.clone = function() {
        return _.clone(this);
    }

}
