/*!
 *
 *  Web Starter Kit
 *  Copyright 2014 Google Inc. All rights reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 *
 */

window.onload = function() {
    'use strict';

    var querySelector = document.querySelector.bind(document);

    // var navdrawerContainer = querySelector('.navdrawer-container');
    // var body = document.body;
    // var appbarElement = querySelector('.app-bar');
    // var menuBtn = querySelector('.menu');
    // var main = querySelector('main');

    // function closeMenu() {
    //     body.classList.remove('open');
    //     appbarElement.classList.remove('open');
    //     navdrawerContainer.classList.remove('open');
    // }

    // function toggleMenu() {
    //     body.classList.toggle('open');
    //     appbarElement.classList.toggle('open');
    //     navdrawerContainer.classList.toggle('open');
    //     navdrawerContainer.classList.add('opened');
    // }

    // main.addEventListener('click', closeMenu);
    // menuBtn.addEventListener('click', toggleMenu);
    // navdrawerContainer.addEventListener('click', function(event) {
    //     if (event.target.nodeName === 'A' || event.target.nodeName === 'LI') {
    //         closeMenu();
    //     }
    // });

    function setViewport(gl) {
        var canvas = querySelector("#canvas");

        canvasWidth = canvas.clientWidth;
        gl.canvas.width = canvasWidth;
        canvasHeight = canvas.clientHeight;
        gl.canvas.height = canvasHeight;

        gl.viewport(0, 0, gl.drawingBufferWidth, gl.drawingBufferHeight);
    }

    //Instantiate the user input manager
    APP.userInput = new APP.UserInput();

    // Init WebGL context from canvas element.
    (function initGL() {
        try {
            gl = canvas.getContext('webgl');

            if (!gl) {
                gl = canvas.getContext('experimental-webgl', {
                    antialias: true
                });
            }

            if (gl) {
                // Make WebGL context available as a global variable.
                APP.WebGLContext = gl;

                // Get the Vertex Array Object extension and create/bind a VAO  
                APP.WebGLExtensions = {};
                APP.WebGLExtensions.VAO = gl.getExtension("OES_vertex_array_object");

                setViewport(gl);

                gl.clearColor(0.0, 0.0, 0.0, 1.0);
                gl.clear(gl.COLOR_BUFFER_BIT);
            }
        } catch (e) {}
        if (!gl) {
            $('#content').html("Could not initialise WebGL, sorry :-(");
        }
    })();

    var canvasWidth,
        canvasHeight;

    var camera,
        scene,
        square,
        renderer,
        cubemap;

    // GGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGG    GAME VARIABLES  GGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGG //
    var levelLoader;
    APP.levelIsLoaded = false;
    //APP.groupMeshMainArwing = null;
    //APP.unitMainArwing=null;
    APP.vitesseCroisiere = -0.05;
    APP.originalAcceleration = vec3.fromValues(0, 0, 0);
    APP.originalVelocity = vec3.fromValues(0, 0, APP.vitesseCroisiere);
    APP.openingAnimateOn = false;
    // GGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGG //

    var canvasElem = querySelector("#canvas");
    var contentElem = querySelector("#content");

    canvasElem.addEventListener('contextmenu', function(e) {
        if (e.button === 2) {
            e.preventDefault();
            return false;
        }
    }, false);



    // Needs to be instantiated in the setup function,
    // because it needs the main shader program.
    // TODO : This could probably be done differently, by moving the shader
    // initialization outside the App.start() method.
    var quad;
    var ellipse;
    var cube;
    var bezier, bezierEquation;
    var coons, coonsEquation;
    var boxcentered;
    var pyramid;
    var mesh;

    var group1,
        group2,
        group3,
        group4,
        group5,
        groupboxcentered,
        groupBezier,
        groupCoons,
        grouppyramid,
        groupMesh,
        grid,
        snowStormEvent1,
        groupTestColors,
        groupTestUser;
    groupTestUser = new APP.Object3D();
    // % DÉCLARATIONS POUR LE JEU % //
    var increasingFactor = 0;
    var decreasingFactor = 0;
    var vitesseCroisiere = APP.vitesseCroisiere;
    // *********     *********      *********      KEYBOARD PUSH MOVEMENT FUNCTIONS     *********         *********         *********//
    APP.isBarrelRolling = false;
    APP.isMovingLeft = false;
    APP.isMovingRight = false;
    APP.isMovingUp = false;
    APP.isMovingDown = false;
    APP.isAccelerating = false;
    APP.isDecelerating = false;
    window.addEventListener("keydown", checkKeyPressed, false);

    function checkKeyPressed(eMovePressed) {
        // OOOOOOOO  "b" = BARREL ROLL OOOOOOOO //
        if (eMovePressed.keyCode == "66") {
            increasingFactor = 0;
            decreasingFactor = 0;
            APP.isBarrelRolling = true;
        }

        // <- <- <-  "numpad 4" = MOVE LEFT <- <- <- //

        if (eMovePressed.keyCode == "100") {

            increasingFactor = 0;
            decreasingFactor = 0;
            //       mesh.transform.rotation.z += mulByTimeFactor(0.1);
            //     mesh.transform.position.xyz = vec3.fromValues(-0.05,0,0);
            APP.isMovingLeft = true;
        }

        // -> -> ->  "numpad 6" = MOVE RIGHT -> -> -> //

        if (eMovePressed.keyCode == "102") {

            increasingFactor = 0;
            decreasingFactor = 0;
            //       mesh.transform.rotation.z += mulByTimeFactor(0.1);
            //     mesh.transform.position.xyz = vec3.fromValues(-0.05,0,0);
            APP.isMovingRight = true;
        }

        // /\ /\ /\  "numpad 8" = MOVE UP /\ /\ /\ //

        if (eMovePressed.keyCode == "104") {

            increasingFactor = 0;
            decreasingFactor = 0;
            //       mesh.transform.rotation.z += mulByTimeFactor(0.1);
            //     mesh.transform.position.xyz = vec3.fromValues(-0.05,0,0);
            APP.isMovingUp = true;
        }

        // \/ \/ \/  "numpad 5" = MOVE DOWN \/ \/ \/ //

        if (eMovePressed.keyCode == "101") {

            increasingFactor = 0;
            decreasingFactor = 0;
            //       mesh.transform.rotation.z += mulByTimeFactor(0.1);
            //     mesh.transform.position.xyz = vec3.fromValues(-0.05,0,0);
            APP.isMovingDown = true;
        }

        // ++ ++ ++  "numpad 9" = ACCELERATING ++ ++ ++ //

        if (eMovePressed.keyCode == "105") {

            increasingFactor = 0;
            decreasingFactor = 0;
            //       mesh.transform.rotation.z += mulByTimeFactor(0.1);
            //     mesh.transform.position.xyz = vec3.fromValues(-0.05,0,0);
            APP.isAccelerating = true;
        }

        // -- -- --  "numpad 7" = DECELERATING -- -- -- //

        if (eMovePressed.keyCode == "103") {

            increasingFactor = 0;
            decreasingFactor = 0;
            //       mesh.transform.rotation.z += mulByTimeFactor(0.1);
            //     mesh.transform.position.xyz = vec3.fromValues(-0.05,0,0);
            APP.isDecelerating = true;
        }
    }

    // ********* KEYBOARD UNPUSH MOVEMENT FUNCTIONS *********//
    window.addEventListener("keyup", checkKeyUnPressed, false);

    function checkKeyUnPressed(eMoveUnPressed) {

            // <- <- <-  "numpad 4" = MOVE LEFT <- <- <- //
            if (eMoveUnPressed.keyCode == "100") {
                APP.isMovingLeft = false;
            }

            // -> -> ->  "numpad 6" = MOVE RIGHT -> -> -> //
            if (eMoveUnPressed.keyCode == "102") {
                APP.isMovingRight = false;
            }

            // /\ /\ /\  "numpad 8" = MOVE UP /\ /\ /\ //
            if (eMoveUnPressed.keyCode == "104") {
                APP.isMovingUp = false;
            }

            // \/ \/ \/  "numpad 5" = MOVE DOWN \/ \/ \/ //
            if (eMoveUnPressed.keyCode == "101") {
                APP.isMovingDown = false;
            }

            // ++ ++ ++  "numpad 9" = ACCELERATING ++ ++ ++ //
            if (eMoveUnPressed.keyCode == "105") {
                APP.isAccelerating = false;
            }

            // -- -- --  "numpad 7" = DECELERATING -- -- -- //
            if (eMoveUnPressed.keyCode == "103") {
                APP.isDecelerating = false;
            }
        }
    // *********     *********      *********      *********       *********     *********         *********         *********//

    // % //

    function makeGrid() {

        var lines = [];

        var n = 100.0;
        var zStart = n / 2;
        var zEnd = -n / 2;
        var h = 0.5;
        var xStart = -n / 2;
        var xEnd = n / 2;
        var w = 0.5;
        var y = -2.0;

        // Horizontal lines.
        for (var i = 0; i < (n / h) + 1; ++i) {
            lines.push(new APP.Line({
                pt1: vec3.fromValues(xStart, y, zStart - (i * h)),
                pt2: vec3.fromValues(xEnd, y, zStart - (i * h))
            }));
        }

        // Vertical lines.
        for (var j = 0; j < (n / w) + 1; ++j) {
            lines.push(new APP.Line({
                pt1: vec3.fromValues(xStart + (j * w), y, zStart),
                pt2: vec3.fromValues(xStart + (j * w), y, zEnd)
            }));
        }

        return lines;
    }

    var addgrid = true;


    //=============================
    // Display statistics for the app., top left corner.

    var stats = new Stats();
    stats.setMode(0); // 0: fps, 1: ms

    // align top-left
    stats.domElement.style.position = 'absolute';
    stats.domElement.style.left = '0px';
    stats.domElement.style.top = '0px';

    document.body.appendChild(stats.domElement);

    //==============================

    // Create a new app object. A parameter object is passed
    // containing the canvas element and the functions (closures)
    // setup and draw.
    // Closure : function with a referencing environnement. This 
    // environnement allows the function to access variables that 
    // are not in its scope, like the scene object.

    var now = _.now();
    var lastTime = now;
    var fpms = 60.0 / 1000.0;

    function mulByTimeFactor(value) {
        // Allow a better control over animation speed, especially when 
        // the frame rate decreases : the transition is smoother and the
        // animation speed stays the same.
        var elapsed = now - lastTime;
        var timeFactor = elapsed * fpms;

        return value * timeFactor;

    }

    var app = new APP.App({

        stats: stats,

        canvas: canvasElem,

        setup: function() {

            //Init camera and renderer first.
            camera = new APP.Camera();
            renderer = new APP.Renderer();
            renderer.init(camera);

            scene = new APP.Scene();
            cubemap = new APP.Cubemap({
                base: 'sky'
            });
             scene.add(cubemap);

            var ambientColor = [0.2, 0.2, 0.2],
                directionalLightColor = [0.9, 0.9, 0.9],
                directionalLightSpecularColor = [0.0, 0.0, 0.0],
                directionalLightDirectionX = 0.0,
                directionalLightDirectionY = -1.0,
                directionalLightDirectionZ = 0.0;

            scene.ambientLightColor = vec3.fromValues(
                ambientColor[0],
                ambientColor[1],
                ambientColor[2]
            );
            scene.directionalLight = {
                color: vec3.fromValues(
                    directionalLightColor[0],
                    directionalLightColor[1],
                    directionalLightColor[2]
                ),
                specular: directionalLightSpecularColor,
                direction: vec3.fromValues(
                    directionalLightDirectionX,
                    directionalLightDirectionY,
                    directionalLightDirectionZ
                )
            }


            // Grid.
            if (addgrid) {
                var lines = makeGrid();
                APP.grid = new APP.Object3D();
                _.each(lines, function(line) {
                    APP.grid.add(line);
                });
                APP.grid.isVisible = false;

               // scene.add(APP.grid);
            }


            ///////////////////////////////////////////////////// TEST GEOMETRIES AMAURY ////////////////////////////////////////////////////////////

            ////With colors and center
            //groupTestColors = new APP.Object3D();
            ////primitives geo
            //var dode = new APP.DodecahedronGeometry({
            //    center: vec3.fromValues(-4, 0, 2),
            //    colorList: [vec4.fromValues(0, 0, 1, 1)],
            //});
            //var tetra = new APP.TetrahedronGeometry({
            //    center: vec3.fromValues(-3, 1, 2),
            //     colorList: [vec4.fromValues(0, 1, 0, 1)]
            //});
            //var octo = new APP.OctahedronGeometry({
            //    center: vec3.fromValues(-2, 0, 2),
            //     colorList: [vec4.fromValues(0, 1, 1, 1)]
            //});
            //var isoc = new APP.IsocahedronGeometry({
            //    center: vec3.fromValues(-1, 1, 2),
            //      colorList: [vec4.fromValues(1, 0, 0, 1)]
            //});
            //var boxcentered = new APP.BoxCenteredGeometry({
            //    center: vec3.fromValues(0, 1, 3),
            //      colorList: [vec4.fromValues(1, 0,1, 1)]
            //});
            //var pyramid = new APP.PyramidGeometry({
            //    center: vec3.fromValues(1, -1, 3),
            //      colorList: [vec4.fromValues(1, 1, 0, 1)]
            //});


            //// vec3 forms
            //var recctCoords = new APP.RectangleWithCoordsGeometry({
            //    center: vec3.fromValues(2, -1, 0),
            //     colorList: [vec4.fromValues(1, 1, 0.5, 1)],
            //   // strTextureImage: APP.ModelElsaOBJ.textureElsa["mesh0"]

            //});
            //var recctCorner = new APP.RectangleStraightCorneredGeometry({
            //    center: vec3.fromValues(3, -1, 0),
            //     colorList: [vec4.fromValues(1, 0.5, 0, 1)],
            //   // strTextureImage: APP.ModelElsaOBJ.textureElsa["mesh0"]
            //});
            //var recctCenter = new APP.RectangleStraightCenteredGeometry({
            //    center: vec3.fromValues(4, -1, 0),
            //     colorList: [vec4.fromValues(0.5, 1, 0, 1)]
            //});
            //var ellipse = new APP.EllipseGeometry({
            //    center: vec3.fromValues(4, -1, 0),
            //    width: 7,
            //      colorList: [vec4.fromValues(0.5, 1, 0, 1)]
            //});

            //var flake = new APP.SnowFlake ({

            //        center:vec3.fromValues(0,0,0),//Position
            //        color: [vec4.fromValues(1,1,0,1)], // No color by default.
            //        detaillevel: 4,//amount of iterations (1 is a hollow triangle)
            //        scaleX: 4,
            //        scaleY: 4,
            //        scaleZ: 1,

            //    });



            //groupTestColors.add(dode);
            //groupTestColors.add(tetra);
            //groupTestColors.add(octo);
            //groupTestColors.add(isoc);
            //groupTestColors.add(boxcentered);
            //groupTestColors.add(pyramid);
            //groupTestColors.add(recctCoords);
            //groupTestColors.add(recctCorner);
            //groupTestColors.add(recctCenter);
            //groupTestColors.add(ellipse);
            //groupTestColors.add(flake);



            ////scene.add(groupTestColors);



            //***************** Test Level loader *********************//


            //APP.LevelLoader ( {
            //        scene:scene,//Is scene has not been initialized in main, create a new one
            //        levelNo: 1,
            //    });

            //***************** Test snow  storm*********************//
            //snowStormEvent1 = new APP.AnimatedEvent({
            //        center: vec3.fromValues(-75, 0, -150),//Position
            //        type: "snow storm", // Type of event
            //        amount: 250,//amount of elements
            //        elementSize:3,
            //        scaleX: 300,
            //        scaleY: 1,
            //        scaleZ: 300,
            //        velocity: vec3.fromValues(0, -0.1, 0),
            //        acceleration: vec3.fromValues(0, -0.1, 0),
            //        lifeSpan: 1,//time before elements die
            //        texture: null,//used for particle emmiter
            //        group: new APP.Object3D()
            //});
            //scene.add(snowStormEvent1.group);

            //levelLoader = APP.LevelLoader({
            //    scene: scene, //Is scene has not been initialized in main, create a new one
            //    levelNo: 1,
            //});



            // ********* Test mesh avion Amaury *************/

            // var decreasingFactor = 45;

            //groupMesh = new APP.Object3D();


            //var mesh = new APP.Mesh({

            //    strMeshOBJ: APP.ModelArwingOBJ.meshArwing["mesh0"],
            //    strTextureOBJ: APP.ModelArwingOBJ.textureArwing["mesh0"],
            //});
            //var mesh1 = new APP.Mesh({

            //    strMeshOBJ: APP.ModelArwingOBJ.meshArwing["mesh1"],
            //    strTextureOBJ: APP.ModelArwingOBJ.textureArwing["mesh1"],
            //});
            //var mesh2 = new APP.Mesh({

            //    strMeshOBJ: APP.ModelArwingOBJ.meshArwing["mesh2"],
            //    strTextureOBJ: APP.ModelArwingOBJ.textureArwing["mesh2"],

            //});

            //groupMesh.add(mesh);
            //groupMesh.add(mesh1);
            //groupMesh.add(mesh2);
            //groupMesh.transform.rotation.y = 180;
            //groupMesh.transform.scale.xyz = vec3.fromValues(0.1, 0.1, 0.1);

            //scene.add(groupMesh);
            /***********************************************************/
            //APP.originalVelocity = vec3.fromValues(0, 0, vitesseCroisiere);
            //APP.APP.unitMainArwing = new APP.Unit({
            //        name : "Phoque",
            //        position: vec3.fromValues(0, 0, 0),//Position
            //        type: "arwing", // Type of unit
            //        //health: 100,//amount of elements
            //        //playerOwner: 1,
            //    groupMesh: groupMesh,
            //        //projectileTypes: ["defaultProjectile"],
            //        //damage: 10,
            //        //energy:100,
            //    velocity: APP.originalVelocity,
            //        acceleration: vec3.fromValues(0, 0, 0),
            //     //   scene: scene,
            //    camera:camera,
            //    });
            //scene.add(APP.APP.unitMainArwing);
            ///////////////////////////////////////////////// TEST AMAURY DONE /////////////////////////////////////

            // -------------------------------------  GUI ---------------------------------------------//
            // This object is used for controling the scene's parameters.
            // The dat.GUI object use it as a model in an MVC style.


            var _sceneParameters = {
                _projection: APP.PERSPECTIVE,
                _vertigo: false,
                _drawControlPoints: false,
                _animate: true,
                _gridSwitch:addgrid,

                get projection() {
                    return this._projection;
                },

                set projection(value) {
                    this._projection = value;
                    this.trigger('change:projection', {
                        projection: this._projection // Pass new type of projection with event.
                    });
                },

                get vertigo() {
                    return this._vertigo;
                },

                set vertigo(value) {
                    this._vertigo = value;
                    this.trigger('change:vertigo', {
                        vertigo: this._vertigo // Pass new value for vertigo with event.
                    });
                },

                get drawControlPoints() {
                    return this._drawControlPoints;
                },

                set drawControlPoints(value) {
                    this._drawControlPoints = value;
                    this.trigger('change:drawControlPoints', {
                        drawControlPoints: this._drawControlPoints
                    });
                },

                get animate() {
                    return this._animate;
                },

                set animate(value) {
                    this._animate = value;
                    this.trigger('change:gridSwitch', {
                        gridSwitch: this._gridSwitch
                    });
                },

                get gridSwitch() {
                    return this._gridSwitch;
                },

                set gridSwitch(value) {
                    this._gridSwitch = value;
                    this.trigger('change:gridSwitch', {
                        gridSwitch: this._gridSwitch
                    });
                }

            };

            _.extend(_sceneParameters, {
                // Ambient and directional lights.
                ambientColor: APP.Utils.convertWebGLColorToRGB(ambientColor),
                directionalLightColor: APP.Utils.convertWebGLColorToRGB(directionalLightColor),
                specularColor: APP.Utils.convertWebGLColorToRGB(directionalLightSpecularColor),
                directionalLightX: directionalLightDirectionX,
                directionalLightY: directionalLightDirectionY,
                directionalLightZ: directionalLightDirectionZ
            })

            APP.sceneParameters = Compose.call(_sceneParameters);

            // The sceneParameters object will be able to trigger events.
            Compose.call(APP.sceneParameters, new APP.Events());

            // The renderer listens to the events triggered by the sceneParameters object.
            renderer.listenTo(APP.sceneParameters, 'change:projection', function(e) {
                renderer.options.projection = e.projection;
            });

            // The camera also listens to sceneParameters events.
            camera.listenTo(APP.sceneParameters, 'change:vertigo', function(e) {
                camera.setVertigoEffect(e.vertigo);
            });

            // Traverse the scene and search for BezierCubicGeometry and ParametricSurface object.
            // If one is found, set its drawControlPoints property to the new value.
            scene.listenTo(APP.sceneParameters, 'change:drawControlPoints', function(e) {
                scene.traverse(function(o) {
                    if (o instanceof APP.BezierCubicGeometry || o instanceof APP.ParametricSurfaceGeometry) {
                        o.drawControlPoints = e.drawControlPoints;
                    }
                });
            });

            // Activate/deactivate animation.
            this.listenTo(APP.sceneParameters, 'change:animate', function(e) {
                this.animate = e.animate;
            });

            // Activate/deactivate grid.
            this.listenTo(APP.sceneParameters, 'change:gridSwitch', function (e) {
                if (e.gridSwitch == false) {
                    scene.removeChildWithId(APP.grid.id);
                } else if(e.gridSwitch == true){
                    scene.add(APP.grid);
                }
                
            });

            // Finally we can instantiate a dat.GUI object.
            var gui = APP.gui = new dat.GUI();
            gui.add(APP.sceneParameters, 'projection', [APP.PERSPECTIVE, APP.ORTHOGONAL]);
            gui.add(APP.sceneParameters, 'vertigo');
            gui.add(APP.sceneParameters, 'drawControlPoints');
            gui.add(APP.sceneParameters, 'animate');
            gui.add(APP.sceneParameters, 'gridSwitch');

            ////////////// GUI AMAURY ////////////////

            //var guiGame = APP.guiGame = new dat.GUI();
            var worldEditorGUI = function () {

                this.grid = true;
                this.chooseCubeMap = "sky";
                this.loadCubeMap = function() { 
                    scene.cubemap = new APP.Cubemap({
                        base: this.chooseCubeMap
                    }); 
                };
                this.stringMeshOBJ = "# Blender v2.62 (sub 0) OBJ File: 'starship.blend'\n# www.blender.org\nmtllib starship.mtl\no Mesh_starship\nv -0.215379 -0.018131 0.254635\nv -0.130827 -0.030620 -0.229209\nv -0.605928 0.226597 0.656825\nv -0.457512 -0.151435 0.173292\nv -0.301224 -0.276743 0.205711\nv -0.762492 -0.313925 0.985007\nv 0.219197 -0.018172 0.253044\nv 0.605928 0.226597 0.656825\nv 0.134193 -0.030673 -0.229209\nv 0.457512 -0.151435 0.173292\nv 0.001113 0.122996 0.372016\nv 0.012398 0.313925 0.213513\nv 0.001113 -0.050108 -0.985007\nv 0.301224 -0.276743 0.205711\nv 0.762492 -0.313925 0.985007\nvt 0.687128 0.734903\nvt 0.647056 0.713176\nvt 0.727495 0.712881\nvt 0.803370 0.558166\nvt 0.819020 0.647724\nvt 0.731080 0.483722\nvt 0.767232 0.889060\nvt 0.789595 0.949528\nvt 0.719622 0.977000\nvt 0.581876 0.736858\nvt 0.566226 0.826416\nvt 0.521408 0.811360\nvt 0.533119 0.828372\nvt 0.577937 0.843428\nvt 0.505647 0.917873\nvt 0.866665 0.826555\nvt 0.911483 0.801881\nvt 0.895593 0.849749\nvt 0.825057 0.558461\nvt 0.870363 0.483722\nvt 0.822743 0.647724\nvt 0.840763 0.976377\nvt 0.793144 0.889060\nvt 0.863116 0.916531\nvt 0.716450 0.963562\nvt 0.672338 0.978323\nvt 0.656604 0.889060\nvt 0.625285 0.883059\nvt 0.652756 0.972560\nvt 0.581174 0.897821\nvt 0.930543 0.835986\nvt 0.915361 0.788125\nvt 0.959472 0.812791\nvt 0.959683 0.489722\nvt 0.874303 0.633968\nvt 0.930754 0.483722\nvt 0.649513 0.736858\nvt 0.697382 0.745914\nvt 0.642631 0.881104\nvt 0.892341 0.635923\nvt 0.993610 0.771113\nvt 0.948792 0.786169\nvt 0.474469 0.009653\nvt 0.548450 0.122690\nvt 0.307351 0.080678\nvt 0.252763 0.082693\nvt 0.012270 0.122925\nvt 0.086477 0.011623\nvt 0.247367 0.995500\nvt 0.310909 0.997262\nvt 0.687128 0.483722\nvt 0.803956 0.655680\nvt 0.832884 0.649680\nvt 0.889335 0.799926\nvt 0.591765 0.881104\nvt 0.584882 0.736858\nvt 0.639625 0.872342\nvt 0.800950 0.736858\nvt 0.744499 0.887104\nvt 0.700387 0.872342\nvn 0.002747 -0.641946 0.766745\nvn 0.588526 0.804300 0.082085\nvn -0.774133 0.361102 -0.519927\nvn 0.453233 -0.885530 0.102060\nvn 0.193934 -0.743086 0.640479\nvn 0.271729 0.091046 -0.958057\nvn -0.594033 0.800046 0.083968\nvn 0.777665 0.356782 -0.517633\nvn -0.458759 -0.882484 0.103739\nvn -0.199995 -0.741166 0.640839\nvn -0.270659 0.089597 -0.958497\nvn -0.538745 -0.763868 -0.355331\nvn 0.811241 -0.356906 0.463148\nvn -0.480937 0.876740 -0.005192\nvn 0.637297 0.469581 0.611021\nvn -0.642687 0.511493 0.570375\nvn -0.823307 0.545207 -0.157847\nvn 0.846183 0.507623 -0.162150\nvn 0.000000 -0.999667 0.025787\nvn 0.538745 -0.763868 -0.355331\nvn -0.815160 -0.343915 0.466087\nvn 0.486085 0.873877 -0.007700\nusemtl OrangeRedSG\ns off\nf 11/1/1 1/2/1 7/3/1\nusemtl BlueCyanSG\nf 1/4/2 2/5/2 3/6/2\nf 2/7/3 4/8/3 3/9/3\nf 2/10/4 1/11/4 4/12/4\nf 4/13/5 1/14/5 3/15/5\nf 4/16/6 1/17/6 5/18/6\nf 7/19/7 8/20/7 9/21/7\nf 9/22/8 8/23/8 10/24/8\nf 10/25/9 7/26/9 9/27/9\nf 10/28/10 8/29/10 7/30/10\nf 14/31/11 7/32/11 10/33/11\nusemtl GrayWhiteSG\nf 5/34/12 6/35/12 4/36/12\nf 5/37/13 1/38/13 6/39/13\nf 6/40/14 1/41/14 4/42/14\nf 11/43/15 7/44/15 12/45/15\nf 12/46/16 1/47/16 11/48/16\nf 13/49/17 1/47/17 12/46/17\nf 7/44/18 13/50/18 12/45/18\nf 7/3/19 1/2/19 13/51/19\nf 14/52/20 10/53/20 15/54/20\nf 14/55/21 15/56/21 7/57/21\nf 15/58/22 10/59/22 7/60/22\n";
                this.stringTextureOBJ = "data:image/jpg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/4QBuRXhpZgAATU0AKgAAAAgAAwESAAMAAAABAAEAAAExAAIAAAAHAAAAModpAAQAAAABAAAAOgAAAABHb29nbGUAAAADkAAABwAAAAQwMjIwoAIABAAAAAEAAAFAoAMABAAAAAEAAAFAAAAAAAAA/9sAQwACAQECAQECAgICAgICAgMFAwMDAwMGBAQDBQcGBwcHBgcHCAkLCQgICggHBwoNCgoLDAwMDAcJDg8NDA4LDAwM/9sAQwECAgIDAwMGAwMGDAgHCAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwM/8AAEQgBAAEAAwEiAAIRAQMRAf/EAB8AAAEFAQEBAQEBAAAAAAAAAAABAgMEBQYHCAkKC//EALUQAAIBAwMCBAMFBQQEAAABfQECAwAEEQUSITFBBhNRYQcicRQygZGhCCNCscEVUtHwJDNicoIJChYXGBkaJSYnKCkqNDU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6g4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2drh4uPk5ebn6Onq8fLz9PX29/j5+v/EAB8BAAMBAQEBAQEBAQEAAAAAAAABAgMEBQYHCAkKC//EALURAAIBAgQEAwQHBQQEAAECdwABAgMRBAUhMQYSQVEHYXETIjKBCBRCkaGxwQkjM1LwFWJy0QoWJDThJfEXGBkaJicoKSo1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoKDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uLj5OXm5+jp6vLz9PX29/j5+v/aAAwDAQACEQMRAD8A8P8AHHjfXF8ca9/xPtcH/EyugB/aM3H758fxcemAO1Zq+NNejY/8TzXvT/kIz/8AxVO8cnHj3XdpP/ITusH6TPX1J4O/ZS8F6n+yTpvhG60dbj42eN/Cl/8AEfSL5XJltbS2lh+zadtyQTdWyXUoOM5jHByK/F8Jha2JqzUJWtd77u9kvVn+sGecRZZkGDws8XS5vauMdIr3VZOU5aaRitZPex8st4517y/+Q9rn/gym/wDiq7z9kXxZrF1+1l8K421jWJFk8ZaQrq99KykG9hBBBboe4IPFcv8ABzSPAuva9Nf+P/E1/wCHfC+nWR1Cb+zLL7XqGsbWX/RLbLBI5HVifMfKqATjOK+hPi58OtD+EH/BZXS/DHhnT4dH0LR/iDoMNnZwg+XAvm2jHGSerMST1J5NVg8PW5YYiUtOdK1/P/gHLxNnWAX1nJY0ffeHrVOfktG0UlZOyu/fTdr266n4a/Fb4z+MrX4o+Jo4fFviiJI9WulVRq0+FHnP/tVz5+OXjgH/AJHLxX/4Nrj/AOKqH4wH/i7Xir/sL3f/AKOev3W/aD/Yls9H/wCCpFz8HfDf/BOXwv4g+Cr+INP02bxpb6VrunlLC4SI3F2moCcWq+TvlOQNv7vB55r9m6H+VdT4mfhePjn44P8AzOXir/wbXH/xde1/8E1fjH4u1T/gov8AAC3uvFXiO4hm+JPh2OaObVJ2SRTqlsCGBbBGOufWrukfA39nPT/25firpPib4q6xa/A3wDqOoTaJf6Lpw1HWfG9pDeiK2srJsrAs00TFhcykRBUL7TkLX0F+0x8FfCf7On/Bz94Z8D+BtBs/DPhPw58W/CNtpumWm7ybVPO01iBuJOSxZiSSSST3oIlsZPxC+JPiWD4ieIo18SeJFWPWb4ALq1wq7RcyAAAScY9O1ZKfEzxMy/8AIyeJm9xrFz/8crovDfwpvvjv+1ba+B9MmjtNR8Y+MX0aC5lQvHatcX7R+ayggsEDFioIztxkdR9JfF/9oz4F/sx/tDa18N9H/Zx8CeN/h/4J1eXQNV1fxFeXk/inxCbeUxXV1HdJLHFbuXDmNFiKjaPu5+X5eNNNXbsfgUMG6rnVqVVCPNbW+r8kuh8lt8TfEin/AJGTxOG/7DFz/wDHK9Q/YY+IniC9/bl+CcNxr3iGW3m+IOgRzJJqlw6Oh1KAEMpfBBzyDwRVO68A/BHXf2o/FFnb/EfxF4a+CumG71LTtam0WS71u5tY1V1soYMr/pLktGkkpC/JvdedtfQ3xi+GXg34N/8ABa34K6L8PdDTw74TOu+Ab2xsjzKBM1lKzTNk75WL5ductk1pCjy2ne+qNMLga0Gq7ndKcVu3e73XkfC/xE+KXidPiJ4ijj8S+JlWPV71UVdWucKPtD9Pn/DAFYp+K/ipR/yNXifn/qMXP/xddDoWk2uv/tV2en30KXNjfeNo7W5hcfLNE+pqjofZlYg+xNfZP7Zv7RnwH/Zn/bd+IHw11b9k34VX3gPwf4gm0me80i/1LT9da0XbuljlExjEyhiwXaFYrtyucj+hKmMVLkpwp8zcb9NEvU/vepjlh3TowpObcb3XLolZdfU+Fh8VfFY/5mrxR12/8he56/8Afda/w8+K3iaX4g+HY38TeJZFk1azDBtWucOv2iP/AG+R25r2z9o/9h/4f/ss/wDBQPx18M/HXxJ1Dw58N/CsTavaa9b6UdS1TUrB4I7m2tYIVKo11IsvliRyIwUZyMHbWj/wUx+Fvgv4M/8ABRTQ9C+HegJ4a8IjTPCt/p9gfmkUT2ttKWlOTulbdl2ydzZNEsfQq/uox+KLa08hvM8PVao04/FByvbpp+J9P/tW+M9as/2q/ihDb6zrUUaeL9VRUTUZlVQL2YAAb8ADsAAAK4JfHuvnP/E+8Qcf9ROf/wCLro/20Jfs/wC018XJFbay+LNaYN6YvJq+v/2q/hnN8Iv2g/Enhvwb+yDoPirwzYyRrZ6muharcC6DRIzHfE+xsMxHGOnNf55RyepjK+Jre0cVGdtpSd5OVrKN2l7r+8zlmUMJSo03BNyjfeMbWUf5mu58Ov498QKSP7d8Q/L1/wCJnPx/4/Wj4O8d68/jTQvM17XfLGqWgKtqM7KwNxGCCN/II4I7it/RtM8GeIvjD4ovPiFJN8MfD+lmW6udB0TT5JrsyRskbadaJIw8l+pLzHCYPXt2f7Z/w90X4V/8FBNe8OeHdPt9J0PS/EGmw2lpbjEcK4tjxnPOSSfUknvXBRy+sqf1pSXKpqNtb9dWnts99T0I42nKt9W5Gm4uW2nRb9d+h8O/tm/EjxJb/tmfGCOPxJ4ijij8b60iRpqc6oii/nwAN2AB0AGAK85HxM8TeXkeJPEbZ9dUuP8A4uuo/bgkaL9sX41Mn3l8ba8R9ft09ffP7THwjn+C/jjRNE+H/wCw34X+I3h+48LaLqB8Qf8ACN63efb7meyjknzLbyrFkSMQQORnmv7AR+sVOIKGV4bB0HRU5VYXV5QglyqN9ZNK+u25+bx+JviQcf8ACSeJRu541W4/+LrW+H3xK8Ry/EDw/G/iTxEytqtngNqtwVP+kJnI3dD0I969L/Z18M/Cn45ftK6lqnxPuJPh74VbWLT7N4L8L2E01xqks90IRp1rJLJ/o0UbYMksjFghIUbjkM/bJ8CaP8Mv+Cl/jrw74f02z0nRdC+IDWVjYWqbIbSFLuMLGg7AVS7nqVc3w9WvPLvYuM/ZOd2lZaLRPq9d1ocV+3d8RvEll+3V8breHxP4jghh+IOvxxRx6pcBI1GpXIAADgAAcAAAD0FeVR/EzxQOB4s8UHnH/IWuev8A38r374g/s63v7Xn/AAWT8cfDHT77+yp/Gnxc1rTHvvK8z7FCdUummmCZG4pErsFyMkAZFdL8S/25/gP8Ffidqnhrwb+y/wDCnxJ8MdBvpNLTUPFV5fy+JPEkUEjxSXJvlmRLWSXazIFhcJkZ9B+708YqVOFGnSc5cik7WSStpq+r7H+V88LOpOdSVTlXM16u/kfLn/Cz/FAHzeLPFA/7i1z/APHK1vAvxL8S/wDCe+G93ibxPKsmrWQcNq1xtYfaI+Dl+QeRznrXdeLPAHwJ+IX7ZGq6V4Q+JGq+FfgdcTteWXiHV9GlvL/TIPI857NbdW3TzCQGGNmYBsqzZGc+nf8ABSD4WfDj4T/tS/BGP4V6HfaH4N8ReB/Cev20eoEHUbw3czSG4uypKm5ddu8r8oOQuABV1MdRm4UFBpzi3qrWstm+/kZ0sHVi/ac+kZJPXfX8hv7cnxA8RWH7b3xqjj8Sa/Dbx+PteVY49TuFSNRqNwAFXfgKAAABjA46V5inxG8Sg/8AIyeI+M/8xO47f9tK9W/ab06DV/8Agp14+s7qFLi1vvjDe2txC4ys0UmvujofZkZgfY16l+2h/wAEyvihpX7cHjyz8D/Avx8fh3b+KdmkjS/D076e9jvi/wBW4Ugx48zkHHWv4rxGGrVZTqQbfvWsvNvX8D+p6eIo04QhLS8b6+VtPmfK3/CyfEgbb/wkniLd0P8AxNLgf+1K1/h38RPEdx8RvDsf/CSeINr6tZZ36pcbT/pCcY34II4Oc9a90/bW+A3wn8B/8FNPi14S1rxAvwk+GXhW4luFGk6S+oXHlRWkMn2Ozg3AefMzMFZzsUksQQNpP+ChXwy8I/B//goFoeh+AdF/4R7wn9h8LXljZuMzRieC2lZpTuIaZy2XbJy2TzS+p1qd583wys9ddOw6eKpTtCzvJcy7DfDXgzRviP8AtJx6H4g1q18O+HdS8RSw6vqVy+yOxtDcMZnLDGDsDAe5FfQHj3/gq1rVl+2BN428PeEfhzJouhaskWjTSeF7ZtUOlQN5KRx3Z/eKzW4bYQRtD44HXx3xb+yX8WbjxhrjL8K/iZJHJqVyyFfC96ysDK5BGI+Qcggj1HrWef2Rfi5Jn/i1PxM+Y5JPhW+5/wDIdclGrjaClCjBp812+V3fb7vI/vzNcs4UzqdKtmuKpVIxpcii6kEo83xS0knzNJLXRJaLVlb9sbwd4U8K/Hfx1ZfD/WLHWvCN5dyXeiz2ZPlrBODKsHPRoi5THYKOle7/ABf+IGh/Fb/gs7Y+I/DmpWusaHqnxC0GazvrZt0NyvnWikqe+GUg14m/7IfxcdFX/hVPxK2rwB/wit/gf+Qq7z9lb9lf4qaP+1L8MtQvvhj8RLW0svF+k3FzcXHhq9jigjS8iZ5Hdo8KqgEkngAE8AVVGOJnVs6TSlOMno9LX/DU584/sanlrrSzCFSdDC1aKvUg3PmjD3nZ/F+7W2mrPw7+MX/JWfFX/YXu/wD0c9fst+3L4j0f4tf8FRtc+NXgf/goV8L/AId+AL7W7G/itdN8Ta1dalYwwRQrLssYbc28rFonIi8zy33YY/MRX5p/FH/gmx+0ZqXxL8RT2/wB+NlxFPqly8cieBtUZZFaVyGBEOCCOR161gn/AIJlftJL1/Z7+OC9v+RE1T/4xX65HY/zClJXLH/BT74/eD/2p/8AgoF8WviH8P8AS20fwf4t8Q3F/pkDwC3eRDgNO0Y+40rh5SvUGU55zX1h+1J8YfCvx8/4Oi9B8YeCte03xR4Y1v4v+FJbDUtOl8y3ulWfTkYo3RsMrDPTIr5HH/BMv9pM/d/Z9+OJH/Yiap/8Yr2L/gnj/wAE7/2gfBX7fvwL1fWPgR8ZNL0nS/iFoF3e3l34L1KC3tYY9St2keSRoQqIqjJZjgAZPFVdEXTR6N4e+K1/8Bv2rrXxvp1vHeah4O8ZNrUFs7bEuTb37S+UWwdocKVzzjdnB6V9LfGr9lX4O/tMftCeIviP4Z/aU+GXg/4e+ONVl17ULHxA11b+J/DhuXM1zbCxWNluJFd5AhWQKcjkgAt4v4+/YV+ON14916aH4K/GCaGbVryWORPBmoskim4kKspEJyCDnPQgj1rLX9hX48k/8kT+M3XP/Il6n1/78180ozWjhfqfhNOdelz0qtBzjzX2as7+XTyMH9sDX/APjH41eMrj4T6FeaD8Pp5ZLfQbC8nlnuGgWLYszGQl1aVtzhCSVDY65r6k+KvxK8P/ABY/4LN/AvWfDOsWevaSNY+H9ibqzfzI1mg+wRTR57sjqVI7EEV88j9g747Af8kR+Mn/AIRWp/8AxivS/wBin9i740+F/wBtP4N6pqnwb+LWn6Xpvj3Qru8u7vwfqMMFtDHqMLvJI7RBVRVBZicAAE5GKdOM+f4Hq0GFlipT9m6bSlOMno1az6abanzH4f1W30D9q6yvbyaO2s7PxvHc3M0hwkESamru7HsqqpJPoDX2V+27+yz8Gf2jf25fiB8SNY/au+Cem/Dzxl4hn1WeDSZb/UteS0OzdHHAlv5ZmYAqD5hUEg/NjFfMPxC/YY+OF18QPEE8PwV+LkkcurXckbx+D9RZXUzuVYEQ9D1B7giso/sJ/HQoM/BH4xdMf8iZqR4/781+/VqdOtyVIVlF8tvsvR+p/eFSjRrclWFfkahy6cruna+5f/4KP/tUW/7b/wC1148+I2m6bcaPomvOtrothOv763sre3W3tw4BwGZYwxUZA3EZOMn0j/goz8VPDvxk/wCCgPhjWvCeuad4g0n+xvCNg13aS+ZH9ogs7WKaPP8AeR1KsOxFeUH9hT467cf8KT+MAH/YmaiP/aNangD9hf442vj3w/NN8F/i5HDDqto7u/g7UQqKs8ZJJMPQDkn0BqpU8NTjGUKi9yLSV11SN/Y4SlGEoVF+7hKK1WzS/wAj7M/bRha4/aa+LkaKzO/izWQqjqf9Nm4r6w/a/ih+OP7SXifxP4U/ai8B6BoOryxtZ2D+KtSt2t9sUaMPLiTYuWBPB/izXiP7Un7MfxM1v9qD4l3ll8N/H91Z3fivVLiC4t/D93LFNG95KyujBMMpBBDAkEEEZzXCr+yf8VFjwvwy+JI56Dwze/8Axuv8/fbYvDVcRR+rylGc7395axcrWtbuzn9nh69KjUjWUXGNvsvdRvpJvscn8ZfDf/COeJ9esF17T/FbRuynWLCZ57fUGYBjIryYeQ7iQSwB3A8nrXtP7bfjzRfiV/wUI8Q+IPD+qWms6LqHiLTntry1fzIZwPsyna3fBBHHpXBf8MmfFVfu/C74jtznJ8M3vH/kOtDwh+yv8U7Hxjos0nww+IyqupWjsW8N3Y2hZ0JJJTgADJPQV5lGnjFF0I0ZKMpxls9Lc3V/4juqVMNzRrSqpyjGS3jrez6eh8UftxRGb9sL41R/3vGuvDp1P26evc/+Cln7X2reI/2kNNuvh18UPEUnh218HeH7P/iReIbqC0iuobCJJ0xG6rvWQENxnPWue/bA/Ys+M2v/ALXnxZv9P+DvxV1DT77xprNzbXVt4Qv5obiJ76ZkdHWIqyMpBBBIIIOTmvOf+GGfjkx5+Cvxf6Y58Gal/wDGa/sCMbq7R+wYOOUYhYTF160L0qfLytxa95R3v2t+JyXwp1KHS/i/4Uvry6FvZ2mvWFzczzMdsUa3cTyOzH0ALEn0NesftkeONI+Jv/BTLx54h8Oaha61oeteP2vbLULR90N3C94hV0PcEd65U/sL/HAn/kivxe/8I7Uv/jFa3w//AGG/jba/EDQJJPgz8W4Y/wC1LRpJH8HakFjUTxksT5OAAOST70+Sx346vljxEsdGvC6pygkpRtq07/ei743/AGjrv9kX/gs342+JtpYf2vJ4J+Let6k9iJRE17D/AGndLNCrkEKzxu4BIIDYJ4r13wT8M9F+Gvxw8SfEb9m79s74Y/C3wr4x88l9eu7jS/EWhWk8ouWsZrJreUTtE+0K0bjfsXlep8p/be/Yi+NniX9tj4zappvwV+LmpabqXj3Xbq0u7XwfqM0NxC+oTukkbpCyujKysGUkEEHJzXmP/DBPx5Vf+SF/Gj/wiNU/+MV+1vDYXE0YSVZRbiote601po0+zuf5b+3rUa017JyXM2t1Z38u56F/wVu/ad8O/tbftj3fijwt4h1LxXpVn4f0rQTrt/YNYSazcWkHlz3QhYllSSQlwDg/MeOATP8Atx/E/wAO/FT4zfs5yeHNasNaj8P/AA38EaBqDWkm9bK+tgEnt39JEYgEdjXmh/YJ+PGP+SFfGjgYx/whGp//ABitbwT+wj8drbxv4fmm+CPxkjhj1azlkeTwXqaKirOhZmYw8ADnPbBNb/V8HRpU1Cr/AAoyS1Wt1/X+Riq2JnWlKUGueSb0elmeqftO6jDpH/BTzx9eXEqW9rZ/GK8uZ5nOFhij19ndyewVVYk+gNaX7e/xyk8d/wDBQz4keIPDPjfVNQ8Mah4u+2WF3Y6zcrZvamSI7k2yBdgAboMYzxWn+2n+yJ8WvEv7Zvxi1DTfhX8TL+wvvHWuXFtcW3ha9mhuo31Cd0kjdYyGRlYEEEgg55zXmf8AwxV8ZsD/AIs58VvTH/CH6h0/79V/GVapXhKVOMX8V9L9L/5n9SUadK0JSktI26dbHT/8FaviBofxl/bn+M3iHwrqljr+g69etJYXtk/mQXa/Y4kyjd/mBGcfSuw/b7+JWgfF39vXw3rHhbWdP1/S/wCy/CloLm0k8yPzobW1iljz/eVwVI7Ec15SP2L/AI0AcfB34sDH/Uoah/8AGq1PAP7GHxjj+IHh+Sb4P/FSOOHVrJnd/COobY0E6ZYkxcAAEk9BjNL2lepKS5X78rvR+f8AmP2dCCjaS92Nt11t/kfnj8afi34ot/jH4sjj8TeIY0j1q9VVGpT/ACjz36fNXMn4xeLv+hq8Sfjqc/8A8VUnxvP/ABevxf7a3e/+j3r9KPhT/wAE5vhbq/8AwTl0T4aXvh2Gf9qb4n/D/VvjVoOpmWT7VZafZywNY6QkX3X+22MGo3AJGQUT7wddv6pofm3PLufmmPjD4uI/5GrxJ/4M5v8A4qvbP+Ca/wAU/E1//wAFFfgDFceJdemhm+JHh1HSTUJWWRTqluCCC2CPrUn7DPxo+Bn7PXw0+JPif4jfD21+K3xKVbC08CeHtbM6eHIw7Sm9u73yJEkkdEEIjj3qDuckk4K/Xfjr4H+BfDf7WH/BOv4seHfh/pvwe8V/GrxLper6/wCB9OuJpLGzhg12yistUtknZpYYb5GlZY2YgCDIZsliO1hSqStued/EL4heIIPiH4gjTxD4iRU1e9VcavcqqgXMgAA38YHAHaso/ETxIBk+JPE+OP8AmMXXfp/y09j+VVfi/I1v4w8XSRsVePU9RZSOxFxLX60fG/8AZCtfDP8AwUHm+Gvh79hPw7r3wpudbstNbxbbQ6rZvHZTJCbi6F353kJ5JeQg4x+728Hmvm6dKU72fXz6n4Rg8vxGNlUlCo48srfaervba9lofk+fiR4i3Y/4SXxN0/6DFz/8XXqX7Cvj3xBeftzfBG3m8QeJJI5viDoEcscmr3LK6f2jACrAuQVIJByCD0rqPC3i/wCAv7OXjz43XDaHbfGqTSNY/sr4a2+rSzHQLqxFzMJ9Qu5IHUzssIiEagqr4ds/MNvsPiD4SeE/Dn7TX7C/xA0PwRZ/C/Xvip4i03VNa8JWVzNJZWiQa1ZRWd/bpMWliju42kYRsTxDwTyTpSovmTctnt87FYXA1oyVT2t3GSurv+a2+3yPgT4j/ETxJF8Q/Eir4i8Rqser3i4XVbgKf9Ikxxv6e3ascfErxJj/AJGLxIe//IVuD/7PXWeE/hLffHz9rS18B6XPFaah4z8Z/wBiW9xIhkjtmuL5ovNZQQWCBixAIztxkV9PfGD9p/4C/srftI658MPD/wCzV8P/ABx8OfBWry6BrGs+Jby9m8VeI2t5TDdXaXaSpFauXD+WqwleB93dhf6BlXhTUKcKXO+W7St+v3H95Sr06PJRp0faTcb6KKsku7tq30PjU/EvxGB/yMXiP3/4mlx/8XXqH7DXxD167/bf+C8M3iDxBJHP490KN0bU7h0dW1G3BBG/BBHUHOfSuz8FeLf2a/gj8c/i5rlvoetfFvwxp8Sr8L9D8QW09pZ6jLNJh5NSaNhLstozhFODMTk7DXt8vw+8H6p4s/Yb+MGj/DzSfhB4l+JHxFhtL7w7pMtw2l6tY2eqWAt9Vt4p2aSFHMpQjcyvuUgnGTzYrGU+V05UWlJaN97N279DnxuPoqPs5UbKSavZb8re2+n5mx408ba3F4315U1vxAF/tS8UImp3A2gXEgAAD9AOAOwrNfx1raD5tc8RD1zq1x9P79ayadDq3x3WzuI0mt7zxV9nmjflZI31HY6n2Kkj8a+hv2jPi58K/gd+014w8G3fwB8B3nhXw7rEljPc2V5fWuqC3G0vLG/mMglCnIGAG24yM5r/ADxo4KddVK8q3JFT5dXLVu76X7HoVcZ7Jwpwpc7cbu3LolZdbX3PmP8A4TzWtu7+3fEOPUatcf8Axdangvxrrb+N9CEmua8V/tSzQq2qTtuBuIwRgvyCMgjuK9p8c/DD4f8A7Fn7RnxM0nxTplx8QDoVtA/gywvi8djqRucSrNePEQxEMTDAXAkOT8vGIvjd4Q8O6n8NvgT8RNN8G6b8O9a8aazPbz6Pps0v2LUrS3ubcQ38KSEtEpMm0jJD7gQSOa3p5TiYc0p1vepv3o3baSko77at6a7GazKlOUVGm+Wa0dlq+Xm2vfbfTc+B/wBtHx3r8H7ZXxejXXtchjh8ca2EQanOqoBfzjAAbAx2AxjpXnC+PvETFR/wkOvZboBqlxz/AOPV1v7dkxT9sD41vH8rL4115lPcf6fPiv0A/bk+D/8AwoD9qzxj4P8Ah/8AsH+G/GXhHRbiFNO1ceHvEN2L5XgjdmEkEwibDsy/KMDGOtf1mox2t+B+vVM8wuWUMJh5UFOVSnzauEV7qhfWbSv7yta5+aJ8f6+w/wCQ9r7en/EzuP8A4qtf4feOtcl8feHY217XWEmrWa4bUpyMG4TPBbkHPIr134efFz4V/DXW/i14l8dfCDSb7xy13BZ+FPAd5Fe2/hzQyXcXr3I8wTs0QVAkTuvJkywOMdf+2R8LfCei2n7M/j7Q/A9l8Mdc+Klo2qax4TsbmWSytY4NQt4rS/gWbMsUV0jSMEYkYiyCeSWox7L7jpxmd0fbfUqmEcfaRkoztGzlyczSs7tJac219Dwj9vLx9r0H7dHxut7fxF4ihig+IWvxxxRancKiKNSuQFA8zAAAAAAAHtXlP/CwfESgf8VL4m9f+Qrcf/HK7b/godO1r+2z+0FJG2108deJGU5+6RqF1g/nX35+1f8ABz/hRfxH0Pw/8O/2DfDvxO8P3HhTRdRPiBfDviC++23VxZRyTjzLWTyiwkOMKAeea/c5Y+lhKNGDgm5Rv9lbJdXZX1P8wFh69edSoqjSUrdX+R+Zo+IPiFz/AMjN4m4OP+Qrcdf+/la3gDx94hHj3w2P+Ek8RP5usWa4bVrjH/HzH1+fnPTv1r2D4Z/tJfBn4b+F/iL4w174P+FfE3xI1jX4o/D/AIT1ZL2Pwl4R0vYz3MoCTLLNKJQsQikcFFAYMfnB6n/goD8FfCvwx+NHwD17w94NHwx1T4k+G9H8Ua/4JW5lni8NXcl8Y18oSnzI4544xKIpPmTBHfjSrjKbqexlStzJ2do2ulfprbztuRSw9blVZVbpNXV3te39IZ+3J4016y/be+M8cPiDxBHGvj/XlRI9VuFSNRqVwAoAfAAAAAGMewry8+O/ESn/AJGTxJ/wHV7n/wCOV758RfgFe/tT/wDBXLxn8ObG8/s2bxh8WNZ0573y9/2OI6ncNNPsyNzJGrkLkAkAdDW74/8A2yPgv8JviRqXh3wX+zJ8NvFvw90G9k077Z4iub+68SeI4YJGie6+2JIEtnlIYqFhdU3DPoP4xqU51JynOpyrmaW+rv0t6n9PqpCEIwjDmfKn20879z5mPj/xFn/kZfEn46vc/wDxytLwN478QSeP/DwOv+Jvl1ezJxq1184+0R/7fII45r2XSdc/Zz+GP7VPxC1m003xJ8Rfhvp+my3fgjw/qsNxZrqOoy+X5VnfyD94LeEmX58HzQqcnBJ6/wCM/hXwX8SP2QPg58ZNP+HXhv4S+K9d+I58N22maBLcLpnivTovJmOoxQXDuyeTPiEsrMrFuW5ULMcNUSc/aaxe1/O19ivb03KMfZu0ra6W1V7B+1P4s1qH9qf4nJDrmuxqnjHV1VF1CYKo+2SgBQGwMdsY4rhR451zA/4qLXuen/Ezn/8Ai69A+PdhDqv7dXjKznjWW3vPiRdQzRkHEkcmrFGU9+VYjj1r1b9qT/gnb8StI/a68ZWfg34R+LG8Dp4jEemfY9OeS0NnvTlGzkpjdznpmvOlhcRVlUnRu0ptWV+rfb0P9B8DxFkuXYfBYPMFCDnh41FKTjFWioJq8t5Pm2XZnzV/wm2vD/mYPEP/AIMrj/4utPwX4w15fHGhhvEHiLd/adrlX1K4/wCeyZ438jHGDXvvx70z4S/swftrfG2x1LwTb65Z+G5ktPBvhh5Jk0l52WMyNdOjeZ5USEsEB+Zi3IrN+Png7w1qfws+AvxH07wTpfw11rx1rV3DdaLpk0hsNQtLa4tvJ1KCOZmeNWZ9hAYq+QQTwa0nl9WHM3VV4bq72vbt+ZzR4wwGKWHgsDKNPFR9ybULc0qTqctr8ztFWckmlLQ/HL4e/Drwn8Wf+CgUHhvx14os/BXgvVvGcsOu65dMVj02x+1OZ5AQD8/lhgvGC5UHjmvur4sf8HKviDw3/wAFB7jxt4N+F/wRuvBfhTxAtn4cvpPA9p/wkLaBbSeRDDHqBAmhL2qlVxgR+ZtAwMV8efGP/gm3+0VqPxZ8UXVv8A/jXNDc6xdyxyJ4G1NlkUzOVYHycEEc8etc3/w7N/aR/wCjfvjh1z/yImqf/GK/YT/LjmR95/AP4DfsdfD39u/4/wDjwfFX4I+JPC/h/UEvvg/4Y8Wz39v4Y1Ke8BuQ+oJDbSSvbWG8Qi38vE7KVZkUZbzPxHFr3jj/AILJ/s8/Efxd+0L8M/j/AOLPH3xR0SS/ufCN1eyJoiQapYiKF0ubWBYoispWJIgVAhfOON3yyf8AgmZ+0ix3f8M+/G/rn/kRNU/+MV7F/wAE8P8Agnn8fvBf7f3wL1bWPgZ8ZNL0vTPiF4fu7y9u/BmpQQWsEepW7PI8jQhVRVGSxwABntSewm01Y2vi/A1z4u8XxqvmNJqeoqF/vE3EvFfpZ+1l49034wf8FC9a+JnhD9tjwX4I8F3WsWOo2tta+I9blvLSGCOFZQlgluLd2YpJhN5V9+G6kH4u8e/sJfHC98d+IJ4fgp8YJo5tXvZI5IvBmotG6G4kIYEQnIOcg+mKyv8AhhD47bCv/CkfjL/4RWp//Ga+ejzwuuV/8MfhuGxGJwrqQVFyTlf7S1V7bbrXU+sPhd4r/Z5+P/7avx4+N3iDWPh3oln/AG2bv4eeEfHD3FppetXrjc2pahHBDK5gVgJBbhf3jysHK7Rngpzqniz/AIKlfAvxp4i+Nnw/+N3ijxd8RdFe+vPDM9zIukiHUrIRxPHNBEscTCUiJIwQBFJnnk+GD9gz47t/zRH4yDnPHgnU/wD4zXpX7Fv7Fnxr8M/tp/BvU9U+Dvxc07S9M8eaFd3d3deENRhgtYo9QgZ5XdogqoqglmJAABORiqjKc7R5Ot/1OmnjMRXcaLocvvXuuZa3vdrZ9j5p8KfFy++AP7Wtr470u2jvdQ8F+MzrcFs7bEuWt75pfKYgHAcKVz23d6+pfjn+yF8E/wBqD9onxF8TfDP7TXwt8H/DXx1q0viPUrHxC1zb+KfDJuXM1zaixWJ1uJEd5BGVkCtx2GW8C+IX7DPxwvPiB4hmj+C3xckjm1a7kR08Hai6OpnkKsD5PQjkHuCKyR+wt8dsD/iyvxf45GfBupH/ANo1++ShSqxhOFZRajbRr8U+p/c06dGsoVaddQly2dnF3TS3T6rofSH7OHhb9lD4r/tpfFbxfe33hfwT8LfCSwt4B8HeM9WvIbPxddESKj3kyRzzJaqYvNki2sxFxGh4DVN43uNY+Jf/AAUo+B/xC8TfHr4V/GHXtc+Iugaetj4Pe9Efh60iv4Giijhnt4UhtVyUVULHccnJJNfM5/YT+O2P+SJ/F76f8IbqX/xmvTf2Kv2KfjRoH7aPwd1DUPhD8WLGysfHOiXNzc3XhK/ihtokv4GeR5GiCqiqpJLEAAE5GKwrYWlByrKvtGy2elvzb1OStgqVNyr/AFi9o2Sbi9Ld99evU+h11ODSPj1HeXMiw2tn4r+0TyN92ONNS3ux9goJ/CvoT9o74M/Df44ftQ+NPF198e/hrp/hHxFrEl/JFYm8vdUNq2A0aQiFUMpAIGHYDIPPSvIfF37K3xUvPGetTQ/C/wCIxVtSunVl8OXjKwNxIVZSI+QQQQe9Z5/ZS+LGOfhb8SD/ANyze/8Axuv8+cO8RQjUoVMK6ic+ZXUkrq6W1rrW+53VlRqzjVjiFB8ttOV6Oze+z0PevDnxB+GH7WP7YHxC+JXxB1LQNB0PToIB4V8OeI7qS2ttXdEMUKXLwpIVhjWJXkVVbJmVeQDnz/8Aaavrzxf8afDvi/XPil4D+I2raprFpaG38NyXOzSLZJkZI1iliRYoByqhSfmYk7iSa4SP9kz4qKo/4tf8R0x0A8M3v/xutLwd+yv8VLHxjosjfDH4jCOPUrVyT4cu1CgTxkk/u+ABkknoK6qmMxten7CrhneUruSUleTd9Vs0tl0RhRwuFoVvbU665VHlUW4uySto+l3q7bnxN+3ZCZP2wPjYo+Zm8ba8oGev+nXGK+6/2+fHz/tC/tfeNfGXw/8A21PA3hXwjrNxDLp+mN4116y+zKkCRsBDBatEmXVjhWIOc9a+Zv2vf2MPjN4h/a/+LWoaf8H/AIrahp97421m5trq18I380M8T30zJIjrEQysrAhgSCCDk5rzv/hhv45rx/wpX4wcn/oTdSH/ALRr+r1CW5+vywuX42jhMSsXCE6dPls/Zy+JQbupX1vFfefQH7D3wt+Dvh74sfEfxV8Vfid8J/F3iPwrPGPCNp4i1C+k8PeK9RlHmy6hdyLbNcT28RI/dtGDNIXDFQoJ89/a5/tTxb+1B4b8beIvjR4E+NXibxhrEb6jeeG5rtl0oQz24iidJ4IVjjIkIiSMEKIXzgnngR+wz8cg3/JFfi96/wDInal/8YrY8A/sO/G+38f6FcS/Bn4uRxW+qWju7+DdRVEUToWJPk8YAzntg9qnla3NPqeDpYueZyxynLl5eV+ztbltaPWKb1drXem1jzv/AIKGRNN+23+0BGi7nl8deJUUf3idQugB+Oa9+/4KUft8eI/EX7Suk3Pwp+M3jKPwnZ+D9As418PeJr6ys4buCwjS4UJHIoDrICGwBz3Jrl/23f2I/jd4k/bX+M+qab8F/i5qWm6l49165tLy08H6jNDcQvqM7pJG6QlXRlYEMpIIIIJFeYJ+wV8el+78CfjQzdOPA+qf/GK/dsOsHVhSq1ZxvGNrOz3S79rH+ZeIliYOrShF+9Ju6ue+/wDBLLw/8HvCHg/xd8TPG3jr4S23xY0i/jtfAvh/4gXN2mk2cy7ZJdYu0t4JWuWVnAhhOBvhdmOWRk8j/ab0G+T9qfQfEmtfGbwX8cPEnjDWbfUtW1zw9d3NysMq3cKLFKJ4YdmVPyJGuxUXAwAKwB+wR8fPL/5IP8ZumDnwRqnT/vxWr4E/YN+Ott498OzXXwP+MUMMGrWUjPJ4K1MLGq3EeSSYeAACSegA9qxlQoRrzxXt07pq3u6K2yd7pehdOtVlThQ9k1ZrXXV33t1PdPHv7QF1+yv/AMFevGnxGtLL+1JPBvxW1rUZLLzBGbyEalcrNEGIIVmiZwGIwG2k8Zr07wT8PtN+FHxq8T/EH9nb9r34XfDfw34uW43P4ilnsPEGh2s0wuGtJbJoJBOY5AArRuu/yxyuST5n+2n+yF8WfEn7Zvxh1DT/AIV/ErUNPvvHWuXFtcQeFr2aG7ik1CdkkR1jIZGVgwIJBB7g15v/AMMW/GgDn4O/Fbjp/wAUnqHH/kKv479pWpVJR9ne0rp66a+Xof028PSqxi+e3u2ezv8AJn1145+I3wB/bk/4KVav4z+I3xGsbv4e+EvCGnWsM3iHztDj8e6rawmLy2KJJLb20kmZJGVGYKVAUhq8n/bZ1a/+Lfxd8KeOda+Nnwa+IF5HrGm6HpXhTwI97Ha+GNO85SsVtBNBGkdtHsRSQ29mZCc4AHjo/Yr+NAHHwd+KqjOc/wDCJahz+PlVoeDf2MvjNb+ONBeT4Q/FTyV1WzZyfCOobY1W4jLMT5XQDknt17VUsTWqrlnS1k73s0/+DYKeEo0pqcamytb3duvoeyfHu/h0v9u3xheTuI7az+I1zcSuwJEcaauXZjjPAUEn6Vu/tj/H+48cftq+Pde8NeNtcuvC194p+1WM1nrF1HaS2/mRfMqhwqpgNjjGAeOau/tO/sr/ABU1r9pz4l3tl8MfiJd2l14s1WeC4t/DV7JFPG13IyOrrHhlZSCCMg5BBwa4Vv2RPi9nn4UfEr/wlr/j/wAhV5NSGKhKdKEJWc73s+l+3qf6AZbHhzFUMFjcViqTlDDqnyudNpcyg27O7Uk4pfNn0pq938Ffjv8A8FDvjN4+8XeL/B954YtblL3wvY6xcz22m+K7xogqrNJHFJILaEx5cBCWLqOgbPkf7U11qHjj41eHfGOufFv4e/EvWdV1a1tFtfC5uVh0W3jdDHGkUsSLFbjJVApJyee5rh/+GRPi4u0/8Kp+JS46EeFr8f8AtKtHwj+yb8WrfxlorN8LfiXHGuo2zSH/AIRe9VVUSoSSTHwAOpPGAa6quKr1V7OVBrmbb+Le99tnZaanl5fkmUZbioYulm0ZqlSVOMJSotKEYKLUXvFyavK1nJ2T0SR8pfEn4jeIIviR4kjj8QeJY1TWL5VA1e5CqouJAAMP0AAAHbFYx+IniT/oZPE2fQ6xdf8Axyl+Jp/4ub4m/wCw1ffl9pkr7Z+H/wCxT4B1X9h3SfAFzoK3n7Q/xC8Gan8WdD1TcftFpY2s0DWelKnf7ZZx3sw75TvvG37GlRlNNx6H+KGDwuJxlScac2rXe718vV9D4k/4WR4kLYHibxN8vOP7Yuv/AI5Xqn7C3jrxFd/ty/BW3m8QeI5IZ/iF4filjk1e5ZHU6lbgqyl8EEHBByD6Gof2MfGvgHwzqWvzeIPhzcfFrxtqsNlp3gTw3KJP7Jvb2eZlkluhDIk0jKhiEUS8OZHyyEKa+ivif8EdC+DP/BSb9kcWvgqz+F/jTxPqfhrWPGXgixuHmtfDd62sxpD5YdmeITxL5hjLsUIxn11o0tObfVHTgcHXcI4pVdmrrXv1e1/Lc+B/iP8AEbxHD8RPEiL4k8RKq6veLgarcKpH2iToN/AHTHasc/EzxFu/5GLxHz0/4mtx/wDF1D8XZGt/HHixlJDx6pflSOx8+Sv2A+OX7Idvo/8AwUevPhR4f/YK8L+IPhDJrun6b/wlVppmsafJDZTxx/abtb8T/Zswl5GyFAHlbTycj9+xWOo4VQU6d7xb6La3f12P7sxmYUMDCnCVNNyjf7K0SXfffQ/IUfEvxHggeIvEfr/yFbj/AOLr1L9hn4ha/dftufBeK48QeIJoZfH2gxSRvqdwyup1GAFWG/kEcEHIPoa7b4Tv8Ffg5+0n8WtM0vwfdftCXkWsnQPhNpsxkm0fW/MvZYxdXTQSJLcMIhEIUj+WZndiyfKa9c+OfwH8O/A7/go/+ySlr4Js/hb428UX/hvWfGfgWzunuLXwzetrMSReV5hZ4xPCgkMJdihBGanEY6jL90qVuaLs9NdL7b/PuGKzDDt+w9lbni2nZfy323+e1zpvGnjbW/8AhNddI1zX0X+1LzCrqlwAuLiQYA39h+lZ3/Cca5s3f254iwe/9q3H/wAXW5o/gC8+K/x+h8L6fNHa3viTxNJpcM7ruWBprxk8wjIJCglsZGcYzXtfxD+NXwr+Anxm1LwTpPwU8JeKvB/hXUm0fUNS1ie5k17WGicxzzx3COscLllfYgTHH8OeP866OGqVVPEVq3JBS5U227vV2stdFuz0sRjFTlGjTpOcrX0svdWmrdt+2588nxzri5/4nniLjr/xNbj/AOLru/2VvFuuX37VHwvjk1zXDG3i7SVIe/uHBBvIhggyYIPfI5FanhPUfhD4G+OnjC6s9B174jeH44mj8C6TfwPDHqN3K6LGl9tYSlI87VCgtKxXIBr1vx58KNJ+HH7S/wCzHeSeDLP4a+OPEmu21z4g8L2kztb2aRapbR2lwqSM7RGZTNlN5x5fY9ezL8srOSxCrqXJKN0m3pzKK1213S3a1scOOzKlb2EqbXNF2bS35W9t9Or2T0Pyg8e+Ptdfx94gVdd16NY9VvAirqc4Cjz3AAAb6YHtWWPiJ4gJ3f8ACRa8fUf2rP8A/FVv6Rptrq/7TVlZXkMd1Z3/AIxitbiFxlJopNRVHRhkcFWIPsa+uf2wP2g/gt+zh+2p8Qvh3q37L/wu1DwH4U8QS6VLd6Tc6jp+uCzXG+WOX7Q0fnKCWA2KCRt+XOR/WCUUldH7pis2p4KpSwdLC+1k6fO+XkVlGy+01d67HxN/wsDxAQM69rzZOONUuP8A4qtf4e+P9c/4WB4fVtd1xlbVrNWU6nORzcJ1+bkHpj3r6R+IXwQ+Gn7AH7d/xN8I+LPDWqfF6HQbWMfD3R3VltddurwRSWrX7ROsjRxxy4CxL++fH3OKsft6fAzR/hJ4m/Z71Gb4d2Hwg+Jfi4S3ni3wVYzytb6fHDqFvFYXiRSu7wNcqbjMZkbBgHQ9a5UclfiTAVpww9LDvlrQbhNqKT9zm2+Ky2btZPS9z57/AG8vHuvQ/tz/ABwt7fxD4ihht/iFr8aRx6ncKsajUrkbQN+AAAAAMAeleUr8QfERHy+JfE52+mq3H/xddp/wUSmaH9tf9oKSNtsg8feImVgcY/4mVzX6Gf8ABQH4Q6p8AP2u/GPg34b/APBP/wAG+NvBWizQJpWsx+CPEV6L1WgjkYia2nELbXZl+QADbg8g1+6Sx9PC06NL2ablFO94xSso31dr7n+ZH1evXnVqe0aUZW6ve/Y/L5vH/iAMQfEvijcO39q3H/xda/gPx54iX4geHM+JPEbbtYslYNqtwBj7THwfn6Hkc5zmvZvgT498B6F8cfiNd+LPgXb+JviPq13b6P4I+GdvZ3lvoGn6lJO0NylzD5/2svFhFitySWd5QzKQprqP+CivwI8O/BH9oH4JrZeCLH4U+NfFGh6ZrXjXwJZXT3Fr4ZvWv9kPleYWeITwp5hhLsUIxn10rY6i631aVO3MnZ+72v3v87WJo4atb2yq3UWrrXvb+kV/25PGevWH7cXxrji8Qa/DEnj7XlVE1W4RI1Go3ACgB8AAAAAdOleY/wDCe+JG/wCZj8RN/wBxa5P/ALUrtP8AgoVLJbftj/H2WNmjkj8b+JHVlHzKRf3RyPcda+3P2o/hNd/Bf4h6Lofw9/Yj8K/Ebw9ceF9G1Aa5/wAIfrmofbLqe0SScedbSiIkOeijIJ55r+NfYVcRVqT52lFtdXu32P6anWp0KdOHLdtLqlsl39T87z478R5K/wDCReI8jqP7VuR/7UrQ8EePPEUnj3w//wAVL4iVV1ayLY1e5+YfaI+D+8wc8jBr2L9m74g+A9S8XeOJl+Bmn/ED4qeLdVt7HwX4Fjs7o+G9IiPmPdN5UcwuZZV2hEicgKFYl8lgNL9vv4Q+GPgf+2N4H0vQdBt/BerahpPh/VfFHhG3vmu4fCOsXE26eySRsnaEELgZIHmEA8VhCjU5PbKpdJr5dP66mkcRCVX2Mqbu1/w50n7VPivWrT9qn4nxw65rsaL4v1dVUahMqqPtkoAAD4GOwGOK4P8A4TTXv+hg8QZ9tSn+n9+vXPF3wYvP2jP+CkniTwLZXAsZvFXxD1Ow+1Mu4WqNfTM8mO5VAxA7kCtfx5+1Z8I/hX47vNF8M/AT4feIPA+j3cmnreeIrq6k1zXI4naNpvtYkVbd5NpZQsbADbnrxwPDucp1alXlXO0r31d+lumup/oHhs8oYbCYTA4TBvEVVQpzko8iUYuKSvKbSvJppRW9m3Y8MbxxruCf+Ei8QYXk/wDEzn4/8iVpeCPF+vTePvD8cniDxAVbV7JHVtSuCpH2mPII3/gQa7uz8TfA3wN+0t4k1m1sde8cfDCxtp7zw7pdzutmvbsoGit7yTO9beNzIGdQzMFXrk59E/at+ENj4B8O/BPXNW+G+m/B34ieIteli1DwtZNMtvPp0E9t5N+IJndrdvMYx4LHdnJx2KeBqtSmqibg9UrvrbfbXoPMuKsAq1HAzwUorExfLKUYKzcHJx5W+f3UrSaTjFtJvU/H3/goz8UfFFh/wUL+PUNv4k163hh+IviFI449QmREA1O4AAAbAHtXi4+MXi7/AKGrxIf+4nN/8VX0B+0noNj4r/4LQfEDS9St4rzTtS+Neo2t3by58ueKTXpEdGxg7WViD7Gvef8Agod/wRe+Mmlf8FJviVa/DL9m/wCJT/Cu38WvHop0jwxeTaa1h5iAeTIFIaPG7kNjBPPFfsatY/y4qSlzPU+B/wDhcXi8D/kaPEn/AIM5v/iq6b4LfFjxVP8AGXwjHL4k8RMsmt2W4NqU+GH2hOvzV9xftUeDv2ff2Ov+Czf7RGhap8HbjxvaeH5U0v4Z/D3TkkXRbzWJ47VYheeXIJ2hTzJHEMWTJIyrlBzWb/wUv/Zw8NfA/wAZ/sq65cfC3SfgP8ZPHBlvfG/w+0y5nez0uGLUoYtOvlgneSS2N0n2nMLSvj7OOAScuxHPLuWvCPw/0H4rftbx+G/Fmv2fhPwrrPiye31nWLt/LhsLI3Uhnfd2YxhlX/aZa+rviV/wXF1bRP20ZvHnhv4c/CK58P8AhvWxbaHdSeDbc69/Yts/kxxR3xIkjZ7bcExgJ5uMYr51+Iv7DPx0n+IOvzR/BH4wzRXOrXsySJ4M1FoyrXDkFT5J3Aggg9CCD3rJT9hH47gED4JfGUZ6/wDFFan/APGa+djKtBXhF7n4LQxOPwkXTw8Gryu2k7u2x7T4k/Z2+AniX9pz42eHvDvxe0HwvFJdW2ufCrxC961v4ZkildprnTruVIzJbyxq6RxPwq7G3AkAHu7/AOIeg3nxy/YZ+F9r8QLD4reJvhj42g/tvxLpskt1p8Ud9rVg1tpttczYkuI7dYH+cgKu5QoGSB8tv+wd8dpB83wS+MrfXwVqf/xivSv2Kv2K/jV4X/bU+DWqap8G/i1pul6V490K7ury68H6jDBawx6hA7yO7RBVRVBLE4AAPIxVUpVL25N3v6u51YXFV5VOSGH5eZq7SltzJ/n1Pjj4vxtP428WoF3GTU78ADufPkFfqf8Atja3oXxN/wCCjusfF7wb+3N8OfAvg3UNasL+O3svEesTahZwW8cIlCWcURglLGJ8RltjbgD94ivgn4h/sNfHG6+IHiCaH4LfFySOXVbx0ePwdqLK6tO5BBEPQ9c9wRWSv7Cnx0RP+SI/GHrn/kTNRP8A7Rr94xFCjiI05+1Sai19l6O3e/Y/uTFYXDYpU6irKLUbfZd00u+z03PfPHM/wN/b7/bB/aG8RWvjCH4N6x4i1iHWvhpqOsyGw0G7KyML1L5o42ktpZwEmiccKZJNw3V3Hiv4meH1+Mv7C/wrtPiJpvxY8UfC/wAZxjXfE2lzS3OnwxX2tae9rplvczASXCW6Qv8AOVCrvAX+ID5J/wCGFPjoP+aI/GL/AMIrUv8A4zXpv7E/7FPxm0D9s/4O3+pfB74s6fp9l470S5urq58IX8UFvCmoQu8ju0QVUVVZiWIAAJyKiphcPH3nVuorRXT15bb7/IzxGCwsI+0Ve6gnypuOnutb77dO59I6L4/uvhL+0FD4psoxNd+GfFL6nFETgTmG9Z9hPYNtK57ZzXtnxU/Z/wDhr8efjNq3jfQvjf4E8O+DvFV/LrF5bayZotd0Tz38yaBbQIRMwZn2FXCnjOQPm8v8Y/sqfFS58X624+F/xGdZNSunBHhu8IYGeQqRhPukHIPes/8A4ZP+LDf80t+I/rn/AIRm85Pr/q6/z4oPFUlUoVsM6kHLmV1JWe267rRo7K0cPWlGvTrKEuW104u60ez6+Z2dnofwX+L3xz8e2mm6jP8ADPQZoQfAl9qFxK1lDcwsB/prFXkjWYZZWBBjPXJwK9N03xfo/hXU/wBm/wCHDeNNL+I3ijw78RodZuNV0y6lvLHRrae5t0SwhuZQrS7mXzG24UbQOflNfP7/ALJ3xWAH/FrfiPwcg/8ACNXvH/kOu4/Zg/Zj+J2iftOfDa+vvhx8QLS1s/FWmXE09zoF3HFDGl3EzO7MmAoAJJJAAGcgV3ZfisUqigsNyucleSUlZcye22nR9Djx2Hw8oczr3UU7K8XdqLW++vVdT889DvrfSP2nbG8up47W0s/GUdzPPIcJDGmpB3dvZVBJ9hX19+2b+zX8H/2hP22vH3xE1X9qD4P6d4D8XeIZdSlg0uS+1LWxaNtDxxwLbiMzEAqP3hUE5+bGK+b/AB1+w78bn8d648fwZ+LssbandlZE8HaiVkUzuQwPk8gjkHoQRWWP2F/jhn/kifxg6Y/5E7Uf/jFf1RGm+VXXQ/cMZRweLqUsVQxqpSjT5HyuDum0/tXs9D3Lx/8AFb4X/wDBR/8AbW+LXibxN4mufhLJ4gsYf+Fc6hqM3l6Zps1oI4reHUPLRnjWWGPIeM4iYnO/jNj9o3xvofhL9nn4DfBtviJo/wAXPGPhPxvPr13rOj3M19p/h2zuWt449Mgu5lDTb3TzWCAKpQD+6a8FH7DPxy2kf8KV+L+Oevg3Uf8A4xWt4C/Yd+Nlv4+0B5vgz8Wo411S0d5H8IagqxqJ0LMSYcAADOTwOTVODOWtleWQjTcMYnTor3Yc0Gk1Fx0k/e1Tu1ezZ5z/AMFD42uP22P2gI1Xcz+PfEagep/tG5r7+/4KH2cH7TH7aHjnx14B/bm+FnhXwr4hngl0/S38e61ZNaLHbxxsPKt4miGWR2yhI5z1Jr5b/bd/Yi+NviP9tj4zappnwW+L2pabqXj3XLq1u7XwbqM0NzFJqE7pJHIkTKyMpBDKSCCCDg15kf2C/j4Au34FfGptpzj/AIQjVP8A4xX7d7HDYinRqxrKLhG32Xvbo+t1uf5p+3rUZ1afsnJSlfS62v1R3/wp/Zy+G/jX4gfFbwj4o+N/h7TPiJZS2l74K8df2tcf8In4huC7SXqXV20X2iORg8flzMMbxLuBIXPW/tpfEfw8vgj9l34UWPxE034seKPhbeXw13xLpss13p0Ud9qVk9rplvczgSXCW6wv85AVd4C4ywHiLfsEfHxh/wAkF+NHv/xRGqc/+QK1PBP7B/x2tfHXh2Wf4H/GOCGHVbOSUyeDdSjSNRPHksTDwAAcn0FXWw9Dm9rLEc3Km0rxtfla0t0d9u5FHEVor2UaVlJq712vfr+Z1v8AwUKja5/bF+Psaq0jTeN/EiKoGSxN/dAD8TXtH/BSH9rzVPFX7SWnXfw4+KHiNvD9r4Q0C13+H/EV3b2iXMNiizKEikVRIr8NxkHrzWH+2n+yH8WPEn7Zvxi1DTfhT8Tr+xv/AB1rlxbz2/ha+mhuYnv53SRHWMhkZWBUgkEHqa83/wCGLfjQ/P8Awp34rdMf8ifqHH/kKv42rSxMJVIU4v3pX2fmf1FCFGapyqNe6ttOqX+Rvfsw/CD4d/HX4c+KrPVviRZ/DX4oWN5b3vh698Ram9loWr2TKy3cDzxxtJDdhjuEjMQ6soA5dh3n7bfxa0Hxj4x/Z98H6b40h+KGtfC3SLfQtf8AF9q0strrE8mpRzJbwTS/vZ4bVN0SyuPm8w4xyK8j/wCGK/jRGvy/B74sLt6Y8IagMf8AkKtHwZ+xj8Zo/G/h9n+EPxWVYtVspHdvCeoYRFuELEkw8ADJJzxyaKdSr7P2Kp2vo3Z977bXFKnTVVVZVLpPRNrt99vI+hPFPxpuv2cv+Ck/iLx1aWf9oSeFfiHqV81oX8v7QgvZhJHuwcMUZgD0Bx2r0Hwz4K07wJ8Udc8ZfBH9pzwP8PdD8SCYiTWLifT9d0iCeTzjbS2nkv5xjYDbsYb9q8qRzxP7T/7K3xU139pz4kXtn8L/AIjXdrdeKtVuIJ7fwzeyxTxvdyMjq6x4ZSpBBBIII5INcKP2SPi5z/xaf4mc9ceFL4f+065Yyr0qs4ulJpSumrp3e9mu60P7olgckzHAYavHMadKcqFOnUTdKcZRSTSlCd1eMm2npu73PbP2iviP8K/2yf2ztSn13xxq1notx4TtdI0rxZd27W9rLrFrAiC6uosM4tpW3bsYZSwJqn8QfFOlfDP9mzwT8L734iaL8UPFcnxItPEvnaRey6lYeGrQeXEYVupQC7zuS5RQFABz6nx9v2Rfi2W3f8Kp+J25uSf+EVv+f/IdaXgz9lL4tWfjbQpJPhb8TFjj1Wzd2Phe9URBZ0JJJTgAZJPbFafWsVOcpOi1Kejdns319OjM48O5Bh8Nh8PTzSLo4aK5YOdJ+9FNJqXxRvf3oxaTaXmn+c/7SWu2fhX/AILTePtT1C4is9P0342ajdXNxKcRwRJr0jO7H0VVJPsK6n/gqF+1w/xJ/wCCsHxX8ReD/iFqupeBdV8atd2F5p+rz/YZ7bemXTawGzCnkcYXinf8FAv+Cdn7QXjD9vD436to/wACfjJqmlal4/165tL208FalNb3UL6jcMkiSLCVZGUghlJBByK8g/4dlftI/wDRvvxu/wDCE1T/AOMV+rppo/zTqSXMz7B/a+H7PX7dv/BXj9p241j4uWfhH/hJpI774Z+OFvD/AMIydShit8x3zpE8ixSKrqsqECN0JO7IFZ/7WnxP8K+Af2X/ANmn4Bt8VvDvx4+Ingz4g3XiS78Q6Hcz3+l+GLC7a1jTSLe7mRWmEkkXnuEARWUDB+U18m/8Oyv2kif+Tffjf/4Quqf/ABiul+Df/BNj9orTPi74Vurj4BfG2GG31izlkkfwNqarGqzoSxJgwAADyemKd0Z8yPRPin8RvEUPxP8AEyR+IvESKus33C6rcY/4+JOPv8Vhn4k+JAP+Ri8R/T+1bj/4upPixtHxU8Ubvu/21fMfp9pkr7o+Hn7DPw71n9hPRfh9eaHHc/tJfEbwRqfxd0LUVdhcWljaSwGz0pY84Zryzjvpx3BQdd4x+xYrE0MLTi5wT5v6b9Ef0HjcVhcFCm6sE+ay2Wmmrfkt2fCZ+JfiMD/kYvEfqf8AiaXH/wAXXqP7DHxB1++/be+C8M3iDxBLDN490KORH1OdlZW1GAEEGTBBHUHII9ay/wBib9k/Vv2zvjRa+GdPv7bw/wCH7Wzk13xL4lu1zZ+F9GgAe5v5clRhVICqxAZyoyBkj6b/AGivg38N/gl/wWj+Auk/Cawm0/wFqF34C1nTROztNcpdS20vnzeZ83mSKQ7A4+YkYFZ4zFYVSeGjFczi3tptp95ljsZhPaSwagnJxk9ErJW7nZ+NPG2txeNtcCa54gC/2peKqLqdwAAJ5AAAH6AcAdhWYPHetN/zHPEHpzqtx/8AF1B8UXe38Q+JpI/9ZHqF+V+onkNfod8VP2b4ND/bHPgXR/2TtD1X4ezapa2TeI449Qt5EtZVjM1wLjzPKXy9znPT5Mdea/zxy/KcTmDqzp1HHlko/ad3Ju3wp2Wju3ZbGmYZpRwShFw5m4t/ZXwpX3a116H58f8ACc62Bn+2/EOB1I1a44/8frvP2VfGWrXH7U/wvV9Y1yQSeLtKVg2qTurA3kIwQXIIPcEHIq54P/ZgtPjX+1L4i8H+DvEKt4L0W+vbubxVOVkt9P0WCQ772R8gMAgAU5AdtvYmvVfiv8O/Bvwu/wCClfwn0/wBZyWfhW6uvCeoWnmljJOJ5YpPNk3ch3GGbOOSeB0p4HK8bCSxcpPkjUjH4n7zcrO3ku/mTjsxws74WCvKUHLbZWur9r9j8rfHPjrXLfx7ryR69rkax6ldKNupTrwJ3AGN3A9qzf8AhPfEQH/Iw69/4NLj/wCKrpfCnwpv/jt+1HZ+B9Nnjtb/AMX+LDo1vPIm9LZ571oxIy5G4Ju3EZGQuMjrX0t8Wf2jfgX+zJ+0FrHw20P9nXwP4z+H/gvVZdA1XWNfu7yfxR4gaCUxXNzHcxypFbOXVyiLEVG0dM/L/WMYxtex+7YjMaWG9lhcPhXWqOHM1FRVoqyu3JpavZdT5D/4WB4i2/8AIw6965/tSf8A+Kr1D9hrxxr837b/AMF45Ne12RJPHugho31OcrIDqMAIKlsHOeQeo7c1Z8Ofsr6X+1R+23dfD34H3+pah4Q1PUJptM1bX7c250rSl2vNdXmcYjt1JyzYL4UYDPivoz46/An4W/Az/gpX+yLb/B/zrrwf4gTwhrEerTqwm12V9blia+kDYIaURKcAAYxgAVUYrm2R52dZ5l0of2eqVqtSlOdrL3VyN+9po3rZb6M+BvH/AI/8QDx/4lH/AAkviNBHq96AF1W4xj7TJ0+ft04x0rKbx34kjIVvEfihTnodUuB/7PW9oGk22tftS2en3sK3FnqHjiK0uYW+7NFJqgjdDjnDKxH419iftjftDfAb9l/9t34hfDbVP2TfhXfeAfB3iCbSLm90rUtUsNcNmu0tLFIJ2iEyg5C7QrFQuVzkfvVXGKg4UYUeduPNpyqyVl1P8xfYVqsqlV1eVKTWt9/kfDX/AAsLxARn/hJvE2B3/ta4/wDjlbPgHx94hXx94bX/AISTxHJ5ur2YIOrXGCPtEfB+fvyMHPWvf/i5/wAEsfENj/wU91z9nbwLqS6lax3SX2n69qHEGn6JLClz9vu2BA2QQyAMwKiRlGMFwBuf8FM/gR8JfgP+0t8Abf4MrNceDfEXg3w/rcWqzlxceIZpNUuozfyB/utMsKNhQFxjAAGKiWZYOq40oRu5xctlorX17X1+4mODxcb1JzdoyS3euvT9TJ/bj8Za5Z/txfGqOHXvECRL4+15Ujj1W4VI1Go3AChQ+AAAAAMY9K8xHjjXj/zHvEX0/te5/wDi67P/AIKFztaftj/HieNtrx+OvETKfQjUbjFfd37bPwWm+A37U/i/wj8Pv2C/DfjLwfpM0K6frKeHteuBqCvDG7MJIJPKba7Mvy9NuOCK/jX2FWvOpNzaUZW6vq+x/T/tqVKFOEo3cl3S2S727n5uf8J1r27b/b3iPK9f+Jtc8f8Aj/8AnNaXgfx5ro8e+HyPEHiNdur2RK/2xcjd/pEfGPM5B6c16J8JP2ZdW/bK/az1zwza6NpvwlsbN7rU/FSiCQWHw90224vJHWZt6iLDBY5XBaQ7SeuO4/4KOfCjwB8GP22/Cek/CzTpNP8ABN5onhbV7FZi5muFuhHJ50u/5hI64ZlOOScBawjhq0aft3N2TS7bmjxGHdRUVH3mr9zY/ap8U61B+1J8To49c12FY/GGrBUGoTKqgXkgAUBsKB7Y4rhY/F+vL8w8QeIDkZ/5CVx0/wC+66b9smZrf9pX4uSoxV4/FOtupHYi6mIr64+PvwxX4VeMtI0rwT+yPoPj7Rbnw7pV/wD24dL1e5+2zz2qSS5eCQRkhz29TmuWODqV51ZqdlGXZvdu21+x/fFfijAZLgctw1TDKpOtSTXvU4JckIXvKo4q75lZXvufELeNdd6f8JB4gznGP7SuP/i60vAXi3Xv+FgeHl/4SDxAyyavZKynUrjkG5jyPv8AIOMc12/wN+C1v+1N8aPEVxcx6b8O/AOgRy694su7WJ/sfhTTEJ3xxiQs5kYq0caMSzNk4OzFdH+2j8M/Dfwc/wCCgd14b8G6auj+G7PWNGeyszudoldLVzuDEtuJYk5OSSa51ha6pLE393mSWu++tuysexiOIcqq495EqK9v7GVSVlFxhpH3XJXTl7ydlfTV2ur/AJR/8FGvij4k0/8A4KGfHmG28Ra9bxQ/EXxCkaRX8qIijU7gAABuAABwK8ZPxi8XY/5GrxJ/4M5//iq+oPHH7K15+27/AMF2/G3wlsdQj0uTx18Y9b0uW+aPzPsUDapctNMEyN7JEruFyNxUDIzkejfFH/goZ+zn8CPjbqngf4e/sf8Awj8bfCvw3qD6YdT8VXWpXXirxJBDIY5Lv7dHMiWrzgFlCwMItw4bGK/Z47H+VU5y5nqfDP8AwuHxdj/kafEn/gzn/wDiq6D4TfFnxVdfFXwwsnibxFIj6vZghtSnIP79OvzV6340/ZD8Nftd/wDBR8fC/wDZN/4SDxR4X8aalF/wjCa9bPaXWnRSRCWZblm3HybXEu6YgkpFu+Ynn6C/4Ku/sg/BH9lDxX+yZD8D79fFOh69pcjan4tAdT4uvbbWDay3qqWZViaSOQRhMqI9nzP98vQiVSVnqzpv27fH+vWP7dfxuhi8Qa/FDD8QfEEUapqtyiRqNSnChQHwoAGABwOmBXlx+IviMNj/AISXxNn/ALDF1/8AHK9Y/ah0m31//gqL8QtPvIVuLLUfjHqFpcwtnbNFJrzxyIcc4ZSw49a9g/bR/wCCVPxY0r9t/wAfWfgH4E+OH+Ha+JzFo507SZXsvsG+P/Vvk5jwX5BPGa+bqRlJuSvufgssFjMRKpWottKfLZXvr19D5GPxF8SKu7/hJPE231/ti6/+OVs/Df4g+ID8SvDat4i8TSedrFkpB1i4KkG4jBHMnIOSCO+a+iP2ov2GJPin/wAFgPiB8FvhXpOneHtLt9b2Qg5GneH7BLWCS4u5iTlYYg7O2W5LKoILCrH/AAUP+B/wt+B37SvwIt/g6Jrjwd4g8JaBrMOqT7vP16WTU7mM30gbo0ywo2AFXG3AAqZU5JOXYUsvxlJTqSn7sJcr1ervbT06nyj4V8D+Hfid+2PH4f8AF/iGz8I+ENU8XTwa5rN2SsVhY/a3M75GTuMYKqP7zLnHWvrn4mf8F5fE2l/tyXHjrwn4H+FMnhTw3rq2+gO3guybW20S2k8iOKO+K+dGZLUMFII2ebtAwOfmj4nfsOfHC8+JviWaD4MfFyaGbWL2WKSPwfqLq6m4kKspEPQjkHvketYY/YS+OqY/4sn8XuOmfBmo/wDxmv6DrYfCYmaqV5prlta60+4/vXEYTAYuUamImpJRsldaX3e59NeOf24/h1+xR4h+PHwn+G/gT4YfGX4O/E/xEuuw373uoWZnsdiS2umObd42MNrK8oWMkDLsT1ro/wBpT9qX4e/tMf8ABT79leTwJ4b8H6bDoN34Js7/AFPRbq5meWXfYp/ZsomJCrZNG8aYG5gx3lj0+Qj+wn8dnfcfgp8Xt3qfBuo5/wDRNenfsTfsVfGzw7+2h8HL7UPg78VrDTbHxzodxc3Vz4S1CKG2hTUIXd3dogqoqqxJYgAAkmuSpgMJBe2VS81F9d9La/8AAOGpluApf7Qql5RjK+vxPltffyPaPinG1x4g8TRqrO0moX4AHUnz5K+4f2hfFNj8S/2xtU8d+Gf2ovCvhbwvNqFreww2+s6nJcWscSRCVVtEiELFipO3cQ27B6kV8xeMf2V/ine+MNali+GHxGaNtSu2Ur4cu2Vg08hBGI+QQcg96zx+yb8WGP8AyS34jj/uWr3/AON1/n/gq2Mwntaaw7kpS5vtJpxcrfDutdjtxVHC4nkn7ZRcYtfZejtfe+umh7D45/4KC+H9M8UfGSx8P/C/wv4g8GfFDxAdTnTU5bqynvoolQRRyLAykIZBJKEJ+9O2R1FdD8Xvjl4T+N37dHwBk8K6P4atY9NufDEFze6ZczSmZ99spsnEhwFtirKpHzEMdxNfPrfsnfFgEf8AFrfiP8o4/wCKavfl+n7uu4/Zi/Zg+J2i/tLfDm9vvhv4/tbW08U6ZNNNP4fu4o4YkuomZ3ZkwFABJYkAAHpivQoZtm9aaw9am+Wc4v4Lcvvc1lptdnHWy/LKKdelNc0YtfFv7tk2r9j8+fC3xUvvgT+1LaeNtOt47zUPB/i3+2YIHfYlw1vemXy2bnAcKVz23Zr6a+NP7JHwb/aW/aD174keGf2jvhn4S+HfjjU5fEGoWWvfaYfEnhw3EhmuLYWKxstxIjs4QrIFbjsMt4d47/Ya+N0/jrXZofgz8XJIJNTu2V08IagyOpmcqQRDyCOc9xj1rKH7DPxyA4+CvxgPPbwdqP8A8Yr+pKcG46o/d8VHBYh08VQxqo1Iw5G04O6dnqpdU9nuj3Dw5/wUS+Hv7Nvjf40aD8L/AINeHdZ+FfxOlg06O38R6jfxXr6ZbqR5JljcSLHPIWlaMt/EFbIGB6Z8V/2o/h/+0N+1Z+xPYeCfCngvSbrQpvCMGpXGjXV3LJo8i6q4OjETMQYodyyBiC+6T7xHFfIQ/YY+OGSP+FJ/GD5uuPBmo8/+Qa9L/Yx/Yy+M3hv9s74P6hqHwe+K2m6bp3jnRLu6urrwjfwwW8SX8LO7u0QCqqglmJAABJIxVRg07nj5lkuQ04yx1KuvaxhL/l5dzfs+RXV7N2621Pm/QtWttD/aks9QvJkt7PT/ABzHdXM0h+WCKPVFeR2/2VVST7CvsL9sr9m/4M/tOftzfEj4iax+1T8GNH+HfjHxFLqskWmHU9R15bJtm+JLcWqR/aCAwH71lUsD82MH518efsG/Hafx94hmtvgf8YriGbVr2SN08FakyyKbhyGUiDkHIIPoRWUP2B/j5g4+A3xnCsACP+EI1Tn/AMgV+5VI0qso1Y4jkfLyuzi9HZvfr8j/ADZjWqUnOm6XMnLm1vuvQ+lfHX/Bam01n49/tAa5Y/Bzwn4o8J/G1rLSja+IL67s7yDRLGEQW9mXtWBWOXb5rxhsEttJYAVl/wDBQz9q74c/tF3P7MOn+BfCPgXR7vw/4a8Pw6ldaHd3k0uiOt5cbtCImYr5MJeOUMwMm5+WK8V8/N+wV8fWPPwH+M3J/wChI1T/AOMVq+BP2C/jtF4+8PSSfA/4yW8cGrWckjv4L1NVjUXEZJJMPAAySewGa5pZbllFxq0ppOKe0t/dt3No5hjqj9nOLtJrptrfsdV/wUMha8/bD+O0Ual3k8deIlVQOp/tG4r7W/bx8dTfHz9rvxl4x+H/AO2l8PfDPhPXJ4JtN0tvHWu2LWypBGjDyYLV4lyyk/IxHzZPJr51/bT/AGQ/i14k/bL+MWoaf8K/iZf6fe+OtcuLW4t/C17NDdRPqE7JIjrGQyMpUqwJBBzkg15qf2MPjQR/yR74rev/ACKOoD/2lX8ge0q0qlWDptqTv1XfTQ/piVGFSNOSmk4q3Trbv6HqXgP9txP2S/hz8UvhVfeEPhj8ddJ8faompa7r0usap5fiTZGrxR+dGIpZoVlaVsSAEvJLkNnNbv8AwUm/aZ8D/tHftFfCefwT4f8ACtlDoejeHbe+1LSLu5ma4lCWynTpRLwFs2QohHzHedxJrxA/sYfGj/ojvxWyeuPCOoc/j5VaXgv9jH4zReNtBdvhD8VERdUtGdm8JagPLUTxlmJ8rgAck9B17UvrGKlR+ruOja3W2t99yvq+GjVVeM/e9VZ9Ntj0r9suN5f2k/i9HGpkkfxRriqo/iJupsV6x+3B+1fq/ij422M3gP4jeIv+EftfDGjWo/srWLmC1jniskSdRGjKAwcHdx1x1rL/AGn/ANlT4qaz+018Sruz+GPxEvLW68WarPbzweGr2WKeNruRkdWWMqylSpDDIOQRnNcOf2Rfi624n4U/En5up/4Ra+X3/wCeVedVeKg61OnCXvS3s+jfa3c/vzL4cOY2jluNxmJpSdGjy8spwa9+MLtpt6xcdDpvhl+2rD8L/wBmnWvhrqPwt8F+PdB1rUzrV+2q3l5bz6jcKuIhKYGXeqY+UMTg84zzXYft9fHPwn8fP259N1PwbYaGmmx32lwvq2myzS/24/8AonzSCQ4Bh2tGNgAIBJ55rygfsh/F0Jj/AIVT8SwCP+hXvun/AH6rS8F/slfFqLxxoUz/AAr+JSrBq1m0hPhW9CoBOjFiTHwAMkn2oVXGyprDzptxure7sl8jOrlPCNDM555hcVCNXlqXSrLllKooq7Tla6UUtEl5aK3wV4w/aruP2H/+C8fjb4tW+mjWD4F+Met6nNYeZ5RvYBqlys0KvghGeJnUMQdpIODjFfRXwv8Ahf4c+C37Sfir4sfsq/tzfCP4S+DfHHniVPEs11pnibQ7CeZbl9Pls2tZhO0TKirJE/7wxA5XPHz3/wAFA/8Agnb+0F4x/by+N2r6T8CfjJqml6n8QNeurS9tPBWpTW93C+o3DJIjrCVZGUghhkEEGvIP+HZX7SLH/k3343f+ELqn/wAYr9fWx/mDOS5mfe37Uf8AwXs8MeAv22/ir4i8F+GdB+PGj/EDwFovw+1PxL4mgvNFu9ShtLZ4tQe3W2lSWGG9kcM6lgSIlHTOeI/b7/br+E/7SX7On7IXhfwP8P8A4deHdd0OHfqEWiXuozXHg4/2vM39moJ5WVo5g6XBL+ZJuOAwXg/IB/4JlftJZ/5N9+OH/hC6p/8AGK6H4V/8E2f2jLD4meG7if4B/G2GO31S1d3fwPqiqirMhJJMGAB6mncz5lY+wP2odUt9A/4Ki/EO/vJo7Wz0/wCMWoXVzM+dsMUevO7ucc4VVJOPStz9u39py++IP/BQn4la94R+IniW68G6r4u+2adc2WvXkdjJaF4ssiLIAIxhuAAOOlaX7a37FXxq8S/trfGXVdL+Dnxa1LS9S8e67dWd5aeD9RmguYZNRneOWN1hKsjKQysCQQc5Oa8zP7CHx2zx8EPjJtz/ANCTqX/xivnZ+1V0ovc/Ca1TG0XUo04OznzXs+l1Y+ov2h/+Cn3h34aftjftNDwz4M8A/FrwP8atTtI7nU72/vLc3enxW0a/Zo5bcq/ktIHLLkBiCCGGAOX/AG7f2pvh7+0Rq37NWm+C/Cfg3Sbvw74d0CLUrjRbu7mk0aUXc6tohEzbTDDvWUMQXzJyxHFeCH9hL47qf+SI/GTnn/kStT/+M1r/AA6/YZ+OVl8QtAuJvgr8YYYYdVs5JZJPBmohERbiMsSTCMADknoBntVOpWlFqz/rU6qmZZjXjKlKm7Sf8u3vXfQ+gfHfjbWoPG+uquua+q/2ndfKup3A2gTSADAfoBwB2rMPjjXAP+Q34k/8Gtx/8XR4/wD+R917nb/xMrvk8Y/fyV9QeD/2YPCN5+ypp/hGXR1m+MXjPwxf/EDSr0kmW1tLeWI22nhc9bm2W6kHoV6HIr+Y8vwWKx06ipT+FN6t69kvN9D++MZjsPhacHVXxNLRLS+7fkup8unx3rgH/Ic8Rc9P+Jtcf/F13n7K/jHWrv8Aan+GEcmua35beLtJDJJqU7hwbyIYIL4IPcYIIruf+CcPwPt/jPrHxE1ybwXZ/ECHwj4V+22Gk3kxgtb6+nmVbfzJcrtUJHOxYt90E9q2df8ADep+D/26vgvZ6p8I9F+Ed0uvaVPHaaReveW2rRPfwEXKysSrbcbfkJxnnqK78DleKhSo5hOo+VzWnvfzJavZfNnDjMyoynVwijqou+q6xvonr+B+X3j/AOIGvL4819V13XI1XVLsKBqc4UDz34HzcAVmf8J94gTbu8ReIOvIOqXH/wAVVb4qzta+LPFDRttkjv75lYdj50mK/Wf43fsn2+j/APBRG8+Fug/sOeGdc+FL61Y2B8T22n6vp8kNlMkf2i7W/E/2b91vkbIUAeVt6nI/q2PLZadEftedcRYPJlQpVaCm5wct4RsoKN/iau3zKyvd2PygX4geIGXP/CRa/jrn+07j/wCKr0/9hrxzr8v7b3wZik17XJY5vHugiRH1KdkcHUbcEFS2CCODnqOOa+hP2A/2QPB/xR/aT/aOu/DPg/T/AI6eCfhXpF8/hWzvr4wWeuzy34hsZJbhWQBPJiuHMgYBlQsODWf438F6t8P/APgpj+zrpmsfAfw38CbpfFWhXKWmh3819Za9FJqtuUu0mdnVguCnyMcZOcZGdYxjdOxwY7ibL8TOtltKir+zcr+6t6fNblbUrpNXsrHxP8QPH/iAfEHxIv8AwkviNfL1e9AVdWuMY+0ydPn4x07dKyB8QvEKpu/4SbxNt7n+1bjj/wAfrqPB3wm1H4/ftYWngPSJorTU/GnjM6JbXEqb47V7i/aLzWGRuCbixGRnbjIzX078Xv2mvgL+yt+0prPwx8P/ALM/w98bfDnwLq8vh3V9a8SXV7N4q8SG3lMN3dR3aSxxWrllcxqsTKNo+7u+X97qYiFHlo06TnLlu7JKyVt27a9j/M1UqlSU5zq8qUrK9/0Pjo/EHxB/0M/ib/wbXH/xyvWP2DvHfiA/t0/A6OfX/Ec0Nx8QdASSOTUrgq6nUrYFWG/BB7ggg+hr3v8A4J6/sofDn9oP/goH8Vo/AvhnUPjJ8LfAXhXWvEfh3RNejksZtZcJHHYWk77gUP2iXaHYqWWIuQMEVoePvh1r3w3/AOCin7KNlr37OPg/9n5rrx1pV3b3HhrVpdUsfFcbappwyLhpHjJtyPuo2R9p+YDKk8eLzTDTcsIqfvct9eVWvFu1t797I3oYPExisRKo2lK3XXW2+2p8++O/HWvReP8AxEI/EXiZQ2r3uF/te5CoPtMuBjzOnOABWa3jzxDEvz+IvEW3/sLXJ/8AZ63NL06DVv2l7SzuoUuLS+8bRWtxC/3ZopNVCOh74ZSwPsa+r/2tvj/8Fv2cf2y/iF8P9W/Zf+FF94G8I69Lpkt5pNxqGm62tku0vLHKJmjM6glgNqqdu3jdkfxv7CdXnnKpy2dtb9fT0P6bqVI0+WnGDlpfS36+p8aDx5r5bA17xGx9tWuf/jn61o+CPHXiKXx94d/4qPxHGq6vZZX+17n5gbiPI/1nIPQg19t/AX/gmnpXgX/gpz8WPAMvhqP4t+Gfhn4S1DxPpOmXzCIa+JbeJtMhlkVlCszzhTJwD5TN8oBFeP8A7b3gjXPh78VfhTba9+zz4N+A5uLxbm3m8OatJq1n4mje5sxuE7FkYwEYARj/AMfPzAZXOn1HEU4e0lN6O1u9tH0Jp4yhUqKlGO6Tv/Wv3G1+1X4s1qz/AGqficsOva5Ei+L9XAVdQmVVAvJQAAHwAO2McVwo8ba8rbf+Eg8QbuhH9pT/APxddF+2XI1r+0p8WpFba0fivWWDehF5NzX2N+2J8N5/gn+0t4q8L+B/2P8Awr4l8L6RPElhqR8N6xdG7DQo7HzYZgjYZiPlHG3Brz6eBqYidWoptKMrbSe7fRdrH99VeKMJlGDy3CPDRnOrRUk3KnTSUI007yqNXb51ZLzPhc+N9e3kf8JD4g3LwR/aU/GP+2laXgjxnr0/jvw9HJ4h8QGNtWs1ZW1KfaQbiPORv5B6c19EfsNfAuH47/F34ya9ffCnSta/4Qzw/Jd2vgtjLp+n2+oy3EcUNuzPJviWMRTsd7ZADelcV+1H4a1XwX+0L4D0/WPg34e+DsxmtZ0s9Ivpb211mJ7uIi5WdyyvjBXCMcZ5xkVLy2tGisTzaX631s7btfhudUuL8rxWZVMhjh0qkYc0nzUnZyp86SV+aVk1rFNedj8qP+CjvxT8T6f/AMFCvjxDD4m16GOH4jeIUjSPUJURFGp3AAADYArxkfF7xgpP/FUeJOOv/Eyn4/8AHq9I/wCClPP/AAUW+Px6D/hY/iL/ANOdzX6YftqfBLWf2ffib4b8P/CX/gnZ4N+K3hW48G6DqY8QjwL4l1Vr67udPhluQZrO5SFiJWYYVcgjnJ6fsNkf5czm+Z6n5Ct8X/FzH/kaPEf/AIMpv/iq6L4R/FnxTcfFTwzHL4m8RSK+rWgKnUpsEeenH3q+/v8AgkF+yvpX7X9v+1R8Ybj9nvwP43uvB8Ol2fh74fz3VxpPh6wvL+8cyss8twGijtre1kyJJiQJlyeRXgf/AAUK8F6r8Ov2+/Bmha3+z74e/Zy1KxGmiTw/o15PeWeqK92zrfpNLJIsiuD5YaNin7jH3g1Mlylbc+gv26/iDr1l+3T8boY/EPiSGGH4heII40TVrlURRqVwFVVDgAAAAAdOleWn4i+JdgP/AAkniXr1/ti6/wDjn+c1758Sf2f7z9qz/gsP44+G9he/2XceMvi7renNfeX5hsYW1S5aaUISAxSNXIUkAkAdDW38SP21Pgf8GPiXqXhzwj+zV8L/ABF8N9BvpdNF94pvr6bxF4jjidke5N6sqpbSSbWZAsLhMj6V83Kmm3KTtqfg0sNUqznVnV5FzNK7bu79EuiPmf8A4WL4lb/mZPEnv/xOLrj/AMiVq+AfiD4gl+IHh+OTxF4lbdq1kGB1e5KgfaYwcgvyMdfUV9S/sQfs1fCP9qH/AIKrXGi/D3TtY+InwZ02w1HxHb6VqcTx3V1DFZ7ksZDkE7bqVIldmG75SSepzv23Ph3rngDWvhOuufsx+DfgSNV123ubTWPDGuS6pb62FliWS1Mwd4g0bEPtDBvl443UvYyUeZ2NaOV4mNP6zKp7sZW6u9mk9drep86/t7fEHX7X9u344Q2+veII4bf4ha/HHGmpXCqijUrkBQofgDsAMD0FeTj4leJMf8jF4k/8Glwf/Z69m/aX0i21/wD4Ks/ELT7yFLiz1D4x6jbXELg7JYpNfdHRsEHaysQfYmvXv25f+CVPxU0r9vn4hWXw++AXj7/hXFt4r8rRf7M8OXM2nfYd8Y/dOAd0ZG85B6Z5r+g4YrD0VCnUik3G935W0P7sp4zCYeNOjVUVeCknZdErr1Pjv/hZfiNR/wAjF4k/HVLj/wCLre+F/wAQvEj/ABO8MI/iDxIytrNmCDqlxt/4+Y8g/Pz6Yr9BvBn7BHhHxT/wWs/aL8L6N8LNN8WeCPhPoGp6zpngpWeCxv7lbK2SztS+4eXuuJt2SwwFc9ARXg37dnw/1z4b/G/4N2Wufs4+E/gC11frd21z4a1WXVLHxbG1zZglZ2kkjJtyPuo2R9p+YYKkzHMsPVmqUaa1in00ur+v3GVPNsLXqqhCkk5RT+zpdX23fyPb9E8K6P48/aGXSfEWrw+H/DuoeIpoNT1CZiqWtr9oczNuHQ7AVX1LDpXv3jj/AIKmalZftQXHi7QfBfw5n0fQtUEWk3DeGoW1Y6VC3lIiXWd6s0AbbjAXeBjGa+c/HvgrW7nxzrjLoevSKNUuSGXS7hs5nkIP3O/UHoayx4F1vb/yA/EXJyf+JTcf/EV/nXhM0xuCUoYaLjeXM3bV22W2iV3t956eJy3DYqSqYh8yUbJX0V1q/me8+IPhL8KfFPxY+L3hfQfidZ6HpepXFvqHhC6lvpLfw3qMLyvLLYXwWNjui3gRFhgEMMdM9DF4g0Hw78R/2VfhlpHiXT/GV58P/FYudS1TSyzadFJfalaOlpbu6gusYiJLbQCWGBwQPmY+BdcI/wCQH4jz2P8AZNxx/wCO13f7Kng/WtN/an+F8zaFrioni7SmLPp88YUC7iOSTHgAY5JPFd2FzarOqqcKCjzySbXN8POpWs9NH17aHBisthCm6k6rlyp2TtvyOO++3+Z+efxRga48VeKFXO6TUL5Rj/rtJX6YftZeMPD/AMWv+Ch+s/Fjwd+2j4F8E+E7vVrK/gittZ1pr60hgjhEipZpB9nlYmN8Rl9rbuepFfnd48+HWvSeOvEDR6D4gkT+1LsqRpVxtP798HOz8Qfesr/hXniQrg+HfEHJyf8AiV3H/wARX9Wxi2k5dkfuGZ5NhczjQrLEqDjCUNOSSany30kmr+7oz6r8aa18I/27f2lv2irux8dXHwf/AOE61O31TwaurXT6f4a11Elf7TDqaRIxjlk4nhLZCs8gZd3B6mPxl4X8O/Fv9iH4N6B4u0n4h33wu8dreaxrejmR9Jhl1LWdOkSxtJZFUypEtuSzhVUs64HBFfFZ+HfiRv8AmXdf6/8AQLuP/ia9O/Yf8A6/bftufBiabQNejhh8e6E0jvpc6rGo1G3JJYrgAAZ54HWrjGzv/W55uY8O4KhQdRYlyjTi3GLcXqqTgne3M9L6X3dzyvwd8Wr74BftZ2fjrS7eO81HwV4zOu29tI/lpdNbX7S+UzfwhwpXOON2ea+pvjx+yR8Ff2o/2j/EXxP8NftN/C3wf8NfHmry+IdT0/xH9ptvFXhg3MhmubUWCxuLiVXaQIVkCtkdgC/yT8QPAXiD/hYviRh4b8Rtv1e8YFdKuPl/0iT/AGOQev41kL4C8Q/KR4b8Tjb0/wCJVccf+Q6/eqmHhVca1OryTUbX0d0/J9U9mf5ne2qQlOnKlzRcr6p7n0vomnfs/ftBftKfGrRfC3ivXPgT4H8SaaLT4fXOqavcx6JevC43Qa0pV5fIuRudASRCfvByQK9X8AeIfCPwSuf2PfgHovjzwx8Ttf8ADvxts/Gesap4ZlefRdBW5vLOCHTrad1UzMxDzOUCorcYO4GvhEeAvECnd/wjfiYH1/sq4z/6Lr1f9gvwHrsf7dnwPkm8P+IYYYPiDoEkkkmmXCqijUrYksTHgAY74A68Vx43L6Khzuq2ora6vdRaWu+vXuzahjq1+R0rXdr2e177badzS03UYNH/AGl7W9upY7e0sfG8VzcSycLFFHqod3PB4VQSeOgr6w/a7/Z7+Df7Sf7Z3xA8dal+1D8H9N8D+MNfl1K4t7BdQvtaFm20PFFALcI0zKCARIQCwODjn5G8deCNek8f+IWXw/4iX/ib32GXSrnD/wCkyc52d+oIrO/4QjXh/wAwHxH/AOCu5/8AiK/jb6y6alTlT5tb9VqvQ/p+ph/acs4S5WlbZO6e+59SePfjL4C/4KKftefGjVte8Yar8H7bxppUNn4Invr5rfRilqEijstZWNGbyZoY1YAHZE4YHecVR+N2reFfgz+zR8E/gbovjbwz8R9a0H4gTeLtW1Dw47zaPoi3DQwRafbzMqmZj80zlFVVPYkivmU+ANcZcf8ACP8AiE/XSrn6f3K1PBHgfXl8e+Ht3h/xE4bWLHJOl3J2/wCkJyfk7dSTVRxtWd06fvSe+ve+wRwcYyi4yfLG2ll2te59Aftlwtc/tK/FqNEZ2k8WayoUdybybivrL9s7UtN+Pv7UHi7xd4O/as8DaH4d1qaJrOxfxBrFu1vthjRh5cMRRfmUn5T3yea+Zf2qPBut3v7U3xPkh0HxBNE3i7V2WSPTJ3VwbyUghgmCDnjGeK4UeAfEAbP/AAjviTI9dJuP6R1wrFTpSq03BtSknu1te22vU/vn/V3DZng8txixapTo0eW3LTmmqkabd1UUkmnBW06s9Q+HXwo8G+LdX+JHhHXPi9Y6frd2babRNdk1Cf8A4RXxMQ7PcRXjtH5jEhlMcki4DCTI9eg+OviLw/oXgv8AZ/8AhbonijSfGt18O9VurrUtZ0ndJpsUt/fWjrZ2ruFZ0iEJ3MAFJcYHBFeH/wDCB+IEJYeHfEm49SNKuP6x1o+CPBPiCDx34dkfw74iVY9Ws2Zn0m4wqi4i5zswAOvPapp4qpyexp0rc2l9XpzX6/mdWM4cw31mOPr432ipJyjD92lzqk6fNeKTSabfLe13ddEfm7/wUlbH/BRX4+/9lG8RD/yp3NfR3/BXH/govr/iH9qvQ7v4O/GjxgfCdj4H8NWf/FP+I76zs4L2DTIIrhQiOgV1kQ7sD7wPfNeHf8FG/hX4n1H/AIKFfHqe38N69PDN8RfELxyR6dM6up1O4IIIXBBHevGD8HPFx/5lfxJ/4LJv/ia/Yo7H+WdRrmZ9GfsSfBf4c/tRfs7fEDwdrXxuh+EfxObV7LVNIt/FmsS2fgzxRZqsgmjnMcMmy9ich0kkbDCTaFBDMPQ/+Cgfxh8E3ut/sp/Bvwb4ysfie3wN0ePRtW8X6bHIum6peXWp/amtrNpVDy29srLGshChyWwqjg/GK/B7xcB/yK/iTH/YMm5/8droPhJ8JfFNv8V/DDSeGfESKmr2hYnTJ+B56f7NMzclY/TD4gftCXX7KX/BY/xx8SbOw/tWTwb8XNb1F7HzRF9th/tO6SaIOQQrNEzgMRgNgnivUPB3w40v4bfGfxJ4+/Z3/bB+Gvwx8N+LPPAfXbm403xFolpPMLhrKaza3k89o3ChWjcb9gJ29/Af27fA2u3f7dnxumj0HxBJBN8QvEEsbx6VcSIynUrgqysEwQc8EcEdzXlf/Cv9fB40HxJ0xg6Rcjj/AL5r532jg7ebPw6OMr4erKLpOSUnJaSVn5Ndz7T/AGkfjp8Kf23f2+/Et1qHxa8V+HdD1TwBb+FdL8cyrNpdnqWrW1uiF9RgRWkOn3LeaHX5dpIJyK5rxtN4V/Zq/Y68G/A/T/iB4R+IvibWvirZeNbx/CdzJeaJ4XtooktUhE7JGHnnZixCKuFVs9i3yivw+8Qq25fDviQk9T/ZFz/8TWp4B+HmuHx94f3eH/EUfl6vZMc6RcBcC4jJJ+T8SaTrt9Fd9S6eZ4ipUblSfNJ72lom102+ZuftNavb+H/+CrPxD1C7mjtrTT/jHqFzcTPnbDHHrzu7tjsqqSfYGtn/AIKD/tLf8LD/AOCj3xN8SeFfHmrX3g3WPGDXun3dlq9zHaTWpeI70AcYQYbovrXLft7eA9fuf27/AI4TW+ha7NDN8Q/EEkckemXDK6nU7ghlbZgjHIIznrmvJx8N/EQUf8U74i/8Fdx/8RX9A0aWGqqFWc1dRtbTrY/vDD4WhVjTrzavyKNtLa2Pt39o/wAQfCL9sL/gpN+0y03xfk8Hab4+jjfwX4mTUZYPDGqTpHDvtdUCRs7W8oUhWxtR4ySGyornPjdr/hP4Ofsz/Af4A6F8QPDXxL8QaD8Q5vGOsap4bme40PQFuWggi0+2ndFMzNiSZyqqqtxglgR8j/8ACufEhO7/AIR7xHuHQnTLj/4itz4WfDzxFF8T/DMj+HvESiPWbE5fS7g8faI+c7OAOtZPL6MHFqtpFaLTdKyMFldGnyfvLxglppvGNlra9vI9i+I/7Z/xms/iT4ihi+L/AMU44YdVvIo408WX6qii4kAUDzuAMYA7AAVit+2p8aGA2/GL4r/X/hL7/n/yLXI/FHaPif4kJ4H9sX2Tnp/pElfa3gH9jH4f6r+xDpXgG60WO6/aH+IXgzUvivomol2+0WdlazQmz0xY+jG8s4r2YDg5jHXeMflfKj9xzivk+U0KM6+HjL2jS0jHRWu5PTaK1Z8r/wDDaPxoDfN8YfiwT048Xah/8dr0z9jH9sD4ua9+2d8H9Pv/AIsfFC+sL3xzoVrdW1z4rvpobiJ9QhV0dGlwyspIIOQQSMHNdX/wTL/ZMj+Lnwk+JHxKi+GLfGbWvDt7p/h7wz4Ullkj02W9u0lmmvr9kdCtvbwRq2C6KxkIJBC10/xJ+GGm/DP/AIKMfsxWs3wh1P4H+Nr7xTok3iXQon87QLxxqtsLe90uUzSs0ciBxIhOI2jUZYsTRFJs8bNM4yWpXxGVUcPHmjGS5koaNQctvitb7SVr6XufLPj79vL46R+P/EUUfxw+MUMdvq17GiJ4z1JVjUXDgKuJ+AOAB2ArJ/4bx+PQI/4vn8aPm4APjfVOf/I9eafFqZrXxV4ykXKyR6pflTnHPny1+xPx5/Y3tdB/4KJy/C3w7+wF4W174RXWvWOnN4vtrTVbF47GaOI3N0t4swgQwmSRsgYPl7evNft+PxOEwSpxlRUuaLf2V8KV97Xeux/m5Qw2JxM6ko1WlGVur3v22Py/H7evx5ZMj45fGY54BPjbVP8A4/XqX7EX7bnxs8Q/tr/BfS9T+NXxa1DTdU8e6BbXVpdeMdRmhuYZNRgR43RpiroysVKsCGDEYNeqfse/sJ+F/GHiP9oXxp4F8Dn9pTQ/AfiqPwd8OtEmu2TS9Wa4kuJDql/NG6b7a3tYoyDvRJTITlcqRF8Tfhjpnws/4KZ/sqWs3wV1b4E+PLzxXocvijw/C32jw7eONYtVtr/SpvPlZ45V80SoSBG0ajLFmNc9XMMBWcsPSpa8r1tFW92+2/zta+hpTweMpqNadR2vqrva9t9vkeeeMf25fjhZeNteji+NXxeihXVbxVjTxpqKpGq3EgVR++ACgdAOg9qz1/br+OIjGfjR8YvqfGup/wDx6sHwr8LL746ftO2fgjS7iK11Hxj4wbRbe4lUvHbNcXzReayjBYJuLFQQSFxkZyPpD4t/tEfBL9mf9oXWfhvof7OfgPxp4B8F6u/h/VNW8QT3U3inXjBMYrq5juklSK2fcrmNViK/Kudufl/kG9apzTnVcVe2rf8AWx/S79jTUacKfNK19EtPvPDX/bs+OAJX/hdPxh3D/qddT/8Aj1eg/sjftp/GXxF+1n8K9P1L4wfFi80+98Z6Nb3Ntc+LtQmhuY3vYVaN0eUhkZSQVIIIOMEGvQP2Qv2TPh78YfjF8ePFPw98G+I/jl4N+Gv2GPwV4VvlkspNbu9UmmS3N+RhxbWqW87OzFd6hWbbyDq/FX4X23w4/bD/AGWV1b4HzfA/4gX3jKw/tfT9LcXPhbXrVdStDb3lhL58pEyb9sseSB5sZ3Hgt00sLiopVnN7929L732OaeMoc/s4ws7a3S3tfY5Hxb+1V8VLHxnrkafFD4jCOPU7pVC+I7wBVE7gKB5nAHAA9qp/8NYfFgf81O+JX/hS3nH/AJErEhsodV+PsdncRrNa3fi1YJ4z0kjfUgrqfqpI/GvpP9qP40fCP4GftSeNPA99+zr8PJPB/hXWJdOnutMvL6z1ZrZdu6WNxKUEqqSQNuGwBla8ajTq1IyqSrOKUktW+v39j/QjMqmWYGvh8BQyqOInOk6j5Y0k1GLin8bjdtyVkjwVv2s/ioRj/haHxI5OOfE15/8AHK0fBf7VHxUm8daFC3xO+IzLNqlmHVvEV4cqZ0BVh5nII4I9/evoHwJ/wT4tfh3+0f8AGqyt/DWofFyx+FkGnN4c0FZDCPEEmqFzaPdyJgiCGKOVpCMBsDO0V57+1j8PrX4e+OvhLHqHwjk+EPjS8v0fUrLTX8/w/rFsl3B5NzZyNLIyyLuKyICQPMU56V0SwONow9tVqNWdt33tvtvfQ8PD8WcJ5hjP7PwGDhLmhfm5aSs5U/aJcjtN2ja7UbJtLvb80f8AgoJ/wUD+PXhH9vb43aVpXxv+L2m6bpvxA160s7S18Z6jDBawpqM6JGiLMFRFUABRgAADpXkLf8FJ/wBoxD/yXz42D/uedT/+PU3/AIKUcf8ABRb4+/8AZR/EX/pzuK/VX/goz8CNS/Zt/bQ8ceCPhX/wTP8ACfxC8C6HcQppWvxeD/Et6uoK9vFIzCW3nETbZHdfkGPlx1Br9ZXc/wA2J1Jcz1Pyq/4eV/tGf9F++Nn/AIXOp/8Ax+ug+Ff/AAUc/aEv/if4bhn+PXxolhm1S1SRX8b6kyspmQEEGbBBHY19cf8ABPz9iWb9oyX9oX4yTfsz6f4v8U+E/ENp4T8MfCKL7XYaDo2q3Ana7m1ANMk0dtZRW/KSyqGkmIZ1IFeX/wDBTb4P6L8Hf2m/gtb3HwJ1j9nr4jXiwS+LfDlvifwvflbtFttR0ec3EzvHMokWSMnEbQqAzlnIdk9yPaS7s+jf21/22/jV4X/bU+Mml6b8Zvi1pul6b48121s7S18Y6jDb2kMeoTokUaLMFVFUBVUAAAAYGK80P7ePx4OMfG/4zYPQ/wDCa6n/APH6q/8ABQiVoP22vj5JH/rI/HniRl9iNQuiK+7v2pPhFL8GfH+jaL8Pv2GfC/xM8Pz+FtF1Aa+fDmt3Rvrq4tEknBkt5BESHY9BkE8183GNSpKT5no13PwulhsRiqlWp7VxUZW+092+iufDf/Denx5TI/4Xf8ZuP+p11P8A+PVo+Bf27/jte+PNBhk+N/xikhn1Szjkjk8aakyOpuIwVYGY5BBII7/jX0L+xt+zCvxm+DnxI+Mmi/ADRPiJ4k1Txf8A8It4a8B+fcWvhnwmiW32q9vLtjKjtGgeGGNZJUw5PJLfL5T+3D4E0n4b/tceDdNtPhbrvwZ8SNFpMvifwpdt5mm2t8bsAz6bL5kjSWkyruGT8rK6jPNUqdSEPaNvp9w1gcXRpwxLqtptdZbN29PO25zH7cf7cXxv8NfttfGjTNN+NHxc03TdL8e67bWlna+MNRhgt4U1CdEjRFmCoiqoAVQAAAMYrzBf+CgPx8I/5Ll8Zv8AwuNT/wDj9ei/EH9na9/a4/4LM+OPhnYX39l3HjX4ua1prXvliQ2UR1S5aaYISAxSJXYLkZIAyK6v4ifttfA74RfFPUvCngf9lr4X+Lfhr4evpdN+2+JrrULnxN4khgkaN7o3qSqlq8uCyBYXCZBIPQfvvtqNOMKcKKlLlTei0Xnc/uenOhShClSoc8uVSdklZNaavq+x4cf+CgPx73bf+F5fGbcO3/Ccapx/5HrV8Bft8fHa78eeH4pvjd8Y5YZtVs45EfxtqbI6tPHlSDP0IJ47gmvfv2UP2RfhP+0B+2L8XtW+Gvhnxn8Wvhd8NdBHiXw34MuI5rPU/EV5cyw29rpszf6wW8UzyeZMOTHECcgsTV/bz+C8fwws/g/P4s/Z4X4B/Eq98TrGz+G5vtfhHxVpglhYSI7XEzRXkMjKDGCQ6PuLfdC81bHYaTdBU1quyTV1fbcyqZlgZ1VhoUldrstG1e1t3br2Ppb9qP8Aak+KGh/tPfEqxsfiV8QLKzs/FWqW8EFv4jvIoYIkvJVVEUPhVAAAUAAAADpXCn9rX4sKOfil8SOmf+Rmvf8A45Wp8eNPj1T9u/xhazRrNb3XxJuYJoiTtkjk1cqynHOCpIPsa9K/ad/4J/fEG0/au8Ww+EfhR4sHguPXwunCx01pLNrPdHzG3937/Ttmv4DrU8zxNSvXw9SclGo42Tk7Xbs/TQ6KdTL8PGlSrRiuaHNdpLZLTXq7nkL/ALWfxaU7f+FqfEgHpj/hJr3r/wB/K0/Bn7VPxXvPGmiwy/FD4kMsmo2qMreI71gwM0YII83kEEgjuDX0Vcfsf+H7r9sL9oCbQ/h3J4u0v4Yy6fbaF4LsnaC0vLy9BWMzMpyttEsMsj8gHPJwOfMP2rfAFr8PvGXwvF/8KZPhP4xvL4NqVpYSibQNagFzB5NzaSGVyJV3bZUyR86knoT04zKc1w1N1ateVoya+1qlLlvfbfZNowo5hgMRUjRhSXvK+0dLxva2+zWtj87/AAj8P9A+Kv7XsPh3xR4gs/C/hXWPFk1vrGr3cnlxWFmbpzO5bs3lhgv+0y19WfEn/guH4m0n9tabxx4X8F/C+Twv4b1tbfQXbwfaNq7aLbP5CRR3pHmxs9sGCkEbPMAxgYPx38TPh54gm+IviSSPw/4gkWTV71hjS7gjBuJCDnZ36g1ij4d+IUDAeG/EHXr/AGVcf/EV/U+p+94/IMpzScK+YTjJKHKouStG+70s7uyWu3Q+3fEMvwqvdO/aO/Z58H/Fnwr4Y8FeOfFGmeN/BGuXM8seg38SCQy6RdyqjGDYHiCMUZd1vhgMCobrxj4X8OfFb9h/4O6B420v4kal8MfG63Wr63o7SSaVbPqOs6dJFYWssgVpI4VtyWYKFJcYA5A+Kv8AhXXiIg58O+IG576XcH/2SvTv2G/A3iC0/be+DM02g69Gkfj7QZHkfTJ1WNV1G3JYkrgAdyenqKcbJnh4zhfB4WhPEfWnLljKXK+XWfs3DmbST1W62u7nzz8Xo2uPFPjKNF3NJql+APUmeWv1M/bL8W2PxR/4KOa18VvCf7dPw68C+A7rWrC/trew8V6xdXtlDbxQrKF0+GH7PIzGOTEZfY2/5s7iK/NPx74A8QDx/wCJW/4R3xI3maxesNulXHH+kSf7HQ8fnWOfAXiA9fDfic4450u4/wDiK/dcVgaWL9nP2lnGLW0XpJK+/XTRn+a9HFVcPOpD2bknK/Var06H3h4t/af+G/7at7+1d8NtJ8W6X8HdB+L3jnTvHHhK81+KSz0bV2tlaG5s794gxtftOIrlNyMvmBgwBAzW/wCEv8L+GPi9+wV8FPD/AI80X4m6l8JfHiXes65ofmSaPaPqmuabJFp1nLIA0iQrbkswVVyyYAOQvwwPAXiAHP8Awjfib6/2Vcf/ABuvVv2DfA2uWX7dXwPnm8PeII4YviFoEkrvpk6qijU7YksfLwAB1JIAHcVx1spw1Gm6kKl0ldJ2evI43b326GtPMsTUkoTp6uyvrte9rbG54S+KV98EP2nNP8baZDHdah4R8YtrUFu7bVuWt75pfKJ7bwpXPbd36V9I/Gr9ln4P/tKftB698RvDP7SXwz8I/D3xxqsmvajZ+IGuLfxN4cNzIZrm3FiqMLmRWZ/LZZApyPvAZb5c8c+B9dXx54gU+H/EQ26vfBT/AGVc/N/pMgyPk5z14rMHgjXsKv8AYPiEAHI/4ldyP/ZK/jf2zg5U6tPmV72fdddOh/UEqKko1IScXa2h9b/D/wCN/wAJfGmq/tMfCrwXrA+DXw3+MlvpSeFNQ1y4upLOKTTJpmMN++WkhhvVnbLHd5W1QwbpW54b8QeE/hFD+yX8EdJ8eeGviXr2g/F+DxZqGo+G5nuNH8Pw3FzbRRafbzyKvmMx3yuUCqrA/Lypr4rbwNrpfd/YPiFjnORpVz/8RXpX7GfhDWo/2yfhG0mi+IlRfG+iNI0ml3CpGPt8BJZiuMAAkk4wOa6qOOqTShKHp00bva225y1MDBXqc77+rStuzuFv4tJ+Psd3M6x29r4vE0zsdqxxpqIZmJ7AKCSfQV9KftR/Af4a/G/9rLx14z1T9oD4V6f4N8T63LfvFpst5qOrG2baHjSFIQgmIBUZcqC2ecYPzX408D6/P458QSL4e8RMrateMCuk3BABnk6HZ0PXIrP/AOEG8QH5v+Ed8TZ/7BVx/wDEV49Os4KVKdLmXNfro16eup/opjsmpY+rh8dhsc6E40nTfKqcrxlyt2507P3Vqkz6qv8A9rvRf2pfiJ+0JoN5r8Xw10n4yW+mLoF3qTSCztRpbusdreNGCY454XIZgCEK4IPfkfijrXhv4ZfAf4PfCPS/F+hePNc0Xxq/iW/vdEdptK0WKZ4YksoZWVTISQ0jbQqgjpyDXz/H4I19c7fDniRTntpM/b/tnWp4E8G+IIPHvh9m8P8AiRY49XsnZn0q4woFzHkk7AAB1ya2/tLEVFarC8npfVac3Na17bnirgPJsBJV8JieWlC0lTvBpzhS9mpOfx/Da6uk2r9Wfm3/AMFKv+Ui3x9/7KP4i/8ATncV+oX/AAU41fVf2pP26PHvjz4W/wDBRL4T+B/A+vXMD6ZozfErXrI2apbRRv8Auba3eFMujNhGIO7PUmvzd/4KOfCnxRqH/BQz49TW/hvXp4ZfiN4hZJI9PmZWU6lcEEELgg5rxn/hTvi7H/Ir+JP/AAWTf/E1+vrY/wAw6jXMz72/Z+8UeHfEP7PP7Sn7KHiz9obwTpmteMPFOk+MdE8fS315ceFfGF5biT7Za3V80fnKJfNhlWWSEgy2z7iDtJxf25PiP4P8MfCT9kP9n/w38TdJ+LmsfBvUtWvdb8QaI0smiW0mrahZSx2FnNKFaVYVtmLOFVcygDGCB8Rn4QeMGP8AyLHiX/wWz/8AxNb/AMKvhR4os/ih4Zkn8M+Io411a0JY6ZPwPOT/AGaZDeh9z/8ABQqJp/22/j5GvLP488SKAO5OoXQA/Ovdv+Cj37bniLU/2itNm+Gfxd8XL4atfB+gWoHh7xPd2tnDdxWEazKFikVd4kB3HGc9a8k/bt8Da9d/t1/G6aPQdekgm+IXiCRJE0u4dXQ6lcFWUhMMCDkeo7mvKz8PteDf8gHxJ8vQf2RcD/2SvmZNxco93+R+CVqmJpOrRpxfvSvdJ9G+3e59I/s9eMdE/aH/AGAPEXwC1r4jeH/hzrUHjqHxzp0/ii6mg0XxJC9p9nnspp0STypIpFSdDIpDlumQSsn7ZfxG8K3Wr/s5fDXw74s0/wCIknwb0mDRNV8S6dv/ALPvrm41SK4+zWrSDe9vbKFjV2AB3HAABFfNaeAtfj2sPD/iQH1Gj3Gf/QK1/AHgLxB/wsHw+39g+I1VNWsmZm0m5AUC4jySdnGB37CqVVuHJY6KWYYiUI0JU39lOWuyd7W2+e53vjz9pC6/ZA/4LP8Ajf4nWunrq8ngn4u65qElgZfKN5CNTulliD4IVmjdwCQQGIJGK9d8E/DfRfhl8c/EXxF/Zv8A2zPhr8LvCvi77RuOu3FzpniPRLOaUXDWM1mbeUTmJwoVo3G/YOV5NfN37e3gPXrz9vH45XEGg+IJorj4ha/IkiabcMrq2pXJBDbMEEcgjrXlA+HHiIJ/yLviLH/YKn/+Ir9+lh6VaEZe0UbxSezv632P7r/s+liaMKiqct4JPZppK+z69mfo38Sf+Chvw/8A2h/2tvjd4dj+K2paP4X+L3ws0rwBH8Q9T02azhl1TT9jG5uIEJkjtLtjPG7Yyqz9CM15H8Q9Q8Jfs1fsQeCPgTY/Evwl8UfFWtfFq08bTjwlcyXmieE7SKFLQRLO6IHmndy5CKAAGz2L/IJ+HfiQ4/4p/wARfL0/4llx/wDEVrfDv4feIY/iD4dZ/DviJVTVbNstpVxyPtEffZwBWDyvC0Y80KmiV7aatK2+/wAjCOQ4ahH93U0VnbTVpWvff1SP0m+POoR6V+3n4xuppEht7T4k3M80jHCxRpqxZmJ7BVBJPoKu/tgfFk+LP2yvHOsaF4q1K58PX/icXFpcWupzfZXgLxfOoVgoQ4btggGsv9q7wlq1z+1b8UXTR9bkWTxhq7KU0u4ZWH22UghgpBHoQTkVwf8AwhGtHP8AxJdf+b/qE3H/AMTX+emLxmJpVK1CEXrU5m9el7L8dT1MNhaVSFOu5K6gopXXVK/5aH1z8Tfjp4R+KPx//aY8KR+OtN8P6H8XZdMm0jxJLK50qaSxBY287oCVhmWVkZ8EDy+QRxXA/EvVNA+GPwR+Enwp0/xbo/jfWtG8YP4gvrnRp3l03R0nMUUdlDKyrvJO6RtgCg545BrwM+CNcX/mCeID7/2VcZ/PbWn4J8J65B420PdomvFf7StWb/iV3C7VEyE/wcADJJ6Cu2tn+KxHMpUtZXV/e+GUua1ttH1toYUcloUZRnGekbO2mslHlu3vt0PlP4jft3fHO0+I3iS3t/jd8YoYYNYvIo0j8aakqxoLhwoAE3AAAAHQACsZP29PjwT/AMl0+NGT/wBTtqn/AMfrg/ibhfij4q3c/wDE7vu//TzJX3T8PP2HPh7q37COj/D+68Prd/tIfEbwRqvxe0LVA58+ysLSaFrLSEQcN9sso76YdwY++9dv+lGNq4HBU4OpST5rLZaK2remy6n8TYaOKxNSahUemu71fY+Uv+G9vjx/0XP4yt2z/wAJxqf/AMfr1D9iD9t342+I/wBtX4L6bqXxo+LmoaXqnj3Qba6tLrxhqM0FzC+oQI8bo0pV0ZWIKsCCCQRg153+yh+0X4R+EFhqFjrXwH8EfGzUPEV1btpp1y+vYLi2JGxLe3W3+95rMuAeSxH4fW37R1n8Mvhz/wAFRv2X/hz4N+GfgvwB408FeMtAb4hy+Gbue4sJ9XudRsH+wQtMWZktEXBlJG+SZwFUJk82YVqEZPD/AFfeMmpe7ayW/fy9TbCwrOmq/tno1eN3vfbseM+NP25fjhZeOtfgi+NPxejhg1W8jSKPxnqKoiC4kCqoEw4A4A7fhWe37dPxzU8/Gj4xZH/U66l/8frzX4uvJD4o8XvFuEi6hqBXAyc/aJSK/Vr41/sp2ulf8FCLr4Y6H+w34b1z4UNrVjp7+KrXTtW094rO4SP7RdrfLN9n/db5DwAP3WDyQa/j2jTr4iU2qjVpJdXvtsj+mK9ejQjFezTum+i2t39fU/O7/hu344r/AM1q+MPzf9TrqXP/AJHr0H9kX9tT4y+Jf2tPhTp+o/F/4s31hfeNNGt7i3ufF2oTQ3ET3sKtG6NKQyMrEFSCCDjBBrJ8NfGL4Z/s7/F/4i+GtF+Ffgj9obw/deJ5rLwrq2t6jdLPNapM0MCQG2GJvNJX5v42weAwx75+0ja/D3wR/wAFFv2c/APhH4b+EfAvjDwj4p0Z/H03h27mnsZtWuL2zcWMTSks6WqjBkON7zOAq7Mko06vx+2vZpNa9Xb021KqVYP3FStdNp6bW+84Dxb+1T8VLfxfrUY+KXxICxapdIFHiW8VQBM4AA83gDgAdsVSH7WHxW25/wCFn/EraBnP/CU3gAH/AH8rN0LwDefFz9oePwnp0sVvfeJ/FL6VBLINywNNdsm8rkEhQScZGcYyOte5fEf45/CP4EfGvUvAelfA3wd4u8F+FdTfR9R1PWZ7p/EWtGBzHNcx3COscDllcoojI+UZIz8vk01Ulz1J1nGKdrtyf3JdD/QjHzyvCSo4HCZZHEVZUvaNRjSVoqyu3Kybb0ilu77WPHf+Gsfiqq7v+Fn/ABJxjOR4pvO//bSu+/ZW/ad+J+s/tTfC+zvfiV8RLiyvPF2lW81vceI7uSK4ja9iV0ZWfDKQSCpzkEgjBrnbH4ufDL4E/FnxwdF8AaH8WfBd1cJ/YUniuW6s7nTYELkgLAdzO3mBSW5IjUgAs1fQnxTtvBHgb9qz9nHwhpPw18L+AfiFa+KNF1fximi3NxNHYST3cJt9OzMSS8cbLJI3ADlQoPJHXhKNW6quvfllG6u3u7JJ7O9meBxJmOXRjLALKOX29GpKM3CkuVRpuTco/HHlbUXdX5ml1PxD+KP/AAUc/aGsfiX4kgh+PPxojig1S5SNE8b6mqxqsrgADzsYA4x7Vgf8PKP2jMf8l9+Nn/hc6n/8erO8DaBY+Kv25dI0vUrWK807UvHUFrdW8gzHPFJqIR0YejKSD7GvvD9ur9qT9nf9lf8A4KL/ABK+EXiL9jv4O6t8N/CHiSbRnvNDutV0vxCLQbQ0qTfa2h89c7h+7VTjb8udw/WND/NacpKVrnxB/wAPKf2jCf8Akv3xs/8AC51P/wCP10Pwp/4KN/tDaj8T/DcM/wAePjTPDNqtqjo/jfU2V1MyAgjzuQR25617j8fvC3w1/wCCM37dnxi+F/iP4P8Agv8AaG0FptPu/C9/4qvLu0l0/T5IHuItv2V0DvJHcwq7HAJt1ZQu4gdx/wAFa7L4SfBvx58B/hf4b+Cngf4XfGa1uLDxH8Qx4cu7yaHRZLtke10Ym4kctNHCySTnACyMqqSAxLsiXUlbc9J/bX/ba+Nnhv8AbW+Mml6X8ZPi5p+m6f49121s7O18Y6jDb2kKajOqRRxrMFVFUBVVQAAMYAFea/8ADeHx6B/5Lb8Z/TH/AAmup/8Ax6qf/BQ2Z7T9tr49SRsVkj8feI2Uj+EjUbmvvL9u34T6j8Bv2tPGHhD4d/sHeDvF/g/R54I9K1lPBevX321Ggjdz51vOIjh2ZflAA24PNfO8s6kpNS6/1sfhcMPiMTOrV9q4qMrfab1vtbZaHwyf29fjyE3f8Lw+M2P+x21P/wCP1peBf27fjpeeOtBhm+Nfxklhn1WzieOTxpqRSRTcRhlIMxyCDgj0zWt8O/2kfDPwv+IvjS18Tfsy/D3xdrGvawqWvh68k1LTz4dmQCBtOtYFZpPmlwCshLBzg9sek/8ABS+1+HfgX9pH4X/D3wd8NvCHgPxh4JltH8fS+HLqafT5tVuJrV/sMLSsWZLRVwZCRveZwFUJkqKfLdSv94UMLVUPrH1i/LJJr3r77ank/wC3F+3F8bvDH7bnxo03TfjR8XdN0zTfH2vWtpa2vjLUYYLeJNRnRI40WYKqKqqoVQAAAAOK8wX9v34+Dr8cvjV+PjfVP6z1D/wUQdoP25vj4ytteP4g+JSG9CNSucGvvb9rP4RX3wP+I2iaD8Nf2FfBvxI8OXHhLRNSGvDwdr+oNf3dxZRyXH760nWEkSHGAAQeuSeP3uriaGHjTh7JNyjfotku5/cs8RhcLTo03STco3+ytkr6s+Dz+358fDn/AIvl8avf/it9T/8Aj1a3gP8Aby+PF14/8PwTfG74ySRzaraRukvjXUmWRTPGCpBm5BHGPQn1rrP2e/2mvCcvjbUPDsn7KXwr+IXifxt4oJ0fTp7zUbP+zHnMUMWmWyLISsSyKSDIxYGR9xxjHVf8FOJfhX4a/ba8K+Bvhf4G8J+D5Ph3d2mj+Krzw9LcSafrOsm4hN0sPnMX8m3dTCrMQzkOSFwBTlVpyk6DoWvFu+n6eZrUrUnW+rSw1rxbv7rVkvLz0PrH9qH9qL4oaN+058SbGx+JXxAtLOz8Vapb28Fv4jvIoYI0u5VVEUPhVCgAKAAABjgVw3/DXPxaQf8AJVPiT7/8VNe4/wDRldh41+D91+0B/wAFH/FPgi1uGsJfE3xC1Sya52CT7LGb2YySBSQGKoGIB6kAVoeNf2ovhR8MPHF/o/hz4G+ANa8GaRdvYi6125upNb1pInKPL9qEirA77SygRsFBHrgf5/VI4ipXq1amKdOHO4q7k7u+1lfRdwpyw8IU6UKHPLkUnZRSSaVrt9Weft+158WQrZ+K3xIG3r/xVF7x/wCRK0PB/wC1Z8VrzxloscvxQ+JDRtqVojhvE16RIDOgIx5nIIJBHQ5q3c/Fj4V/Cf43eKL/AMLeFdL+KXgG5jX+yIfEs9zZtpYOJHyY8szKdybm6qFPrn0/9qefwX4P8N/CfSLP4Y+FfA/xI8QX9j4i1hdJubh10iwkmT7Lav5pJM0qnzHGBsCAYJYGpw+HxbhKt9b1ptXV56+8krPZ36DqVsO6kKSw1udaO0e13dbq3U+Gf2vf2wPjBpH7X3xYsrD4sfE/T7Gx8aazbW1tb+K76OG3iS+mVERFlAVQoChQAABgAYrzdf21/jVI3y/GT4rN2/5G7UOf/ItdF8fNPt9Z/wCCknjixvIY7q0vfiveW1xDIPkmjk1p0dG74Kkg+xr2b9tL/gmL8TtH/bs8fWfgL4F+PG+Hdv4p8nSk07QLmSxNlvT/AFThSGjI38g4681/V0eyP2ejjMkwdPD4fF04JyoqfM1FJ2UVbW127nzw37afxqxn/hcfxUxnH/I3X/H/AJFrb+G37Z/xkl+I/hyOb4v/ABSmik1W0WSOTxbflHUzxhgR52MEcEHg5INey/tZ+Mfhj+w1/wAFDfjx4bHwN8AeOPDdpq1tbaNp2rXV5awaBHHBvkEAgcE+a0y7g2ceUuOprZ/4KH2Xw7+Gfjr4MeA9E+EvhD4b/FK1urHXPHI0K6upodLkuJYzbaTmd2LSRxOskx4Afaq8ZqtTlp5rgq9SjRjl9lXjzRlanZLlUm2r8ySuk9N7WPj3wV8OvDvxb/bFh8N+LPElj4R8Ia34wnttb1q9kEcOn2Ru5DcOWPAbywyr/tMv1r66+KH/AAXi1rR/24rjx94X+GXwcuvDnhnXBbaDey+DLY+IDolq/kxwx35PmRu9qG2kY2CQDGBXxX8TfAviH/hZnih/+Ec8SSLNrN6yuNKuMMPtEnIPl8g9sZ61if8ACB+IHz/xTfibkf8AQKuP/jdfvGIy/B4ySniJJ2jZK6sr7/Nn+ZtHFYugnGlBr3rt21fY+y/g1YfA79n/AP4Lar4jsPGPhXUvgp4U1i88aaBfRTBbM4tZbyz09SR8skdwyxKpXIMS8V8//sV+K9W8ef8ABRr4O69r11Je674g+KGh6pqdw/3prqfV4JZmP1d2rzQ+AvEG7d/wjfifOMZ/su4/+N16t+wX4B1yD9ur4GySeH/EMUcHxB0B5HfTLhVRRqVuSxOzAAxySRj1pVMLSpUpzc+Z+z5b3WyT/F31D6xXqTjD2biubm2fX/IyvjFA114h8XRxqzyTajqChQOpM8tfpd+11rWifEb/AIKB618WPB/7bHw/8D+FbzVrG8igsvEeqz3lpDbxwiZVs4k8iRmMb4jLbX3gN1xX53+OfBGvSeP/ABAV8P8AiJdur32D/Zdz8/8ApMnI+TnPByKzl8D+IB/zAfEQ3dcaXc8/+OV/FlLFVKDnBRbu0+q1V+z8z+pK2EVZQfNaya2T0du/ofXHwq/aJ+FXxB/4LTah8XbxrXwz8OLXxDfeLbRNQP2dbma2tne2BXnZLPcqJFQjIZsYJrwb9kTxbqnjz9uj4W+INcnkuNb8QfEPSNT1KV/vSXM+pwyyk+h3u38q8/8A+ED14H/kX/EX/gruee39yvSP2MPBmuQ/tj/CEv4f16OJPHOiFpX0ydEQfb4CSSUwAACSTjHXiiNapVlFOP2r6d3YuWHhTi5K/wANvu/4c9B0L4h3Xwi/aJh8U2MSz33hrxU+qQxHAEzQ3jSbCewYArntnNe4/Fb9nj4W/Hf4yaz440H48fD/AMM+B/FmoS6zeWmtGaHxBoPnSeZPALMIRO6sWCMrhTkZyBz4F4w8A+ID428Qf8U74ikWTVbtht0m4OB58nfZjB4IIqkPAniHbt/4R3xMBjp/ZVz/APG682jiZwU6dSnzRbvrfR/Ly0P9EcVlNDFzo4/B472FWNP2bcfZy5oO0rNSurp6xfRt6M9m+Fus/CXxJ/wUX8P3Wnx/8IX8H7PxJFdxLq07vi2th5geUyZK+bJGp2nOAwHbjD+BvxIvvjF/wUB8GeLdTaQ3/ir4jadqc25iWQy6lG6qf91Sq+wWvNT4D8QKv/Iu+Jvl6Y0q44/8h16B+yV4M1y2/ax+FUkug+IIVTxjpDM8mmTqigXsJJLFMADuTjitMNWqyqwpqFlzqWit1WnounqcuaZTl+FwWJxcK/tJRwsqS5pJuyU23fdym2uZvflR+TfgfXrPwp+3NpOp6hcw2en6b47gubm4lbbHBEmoBndj2AUEk+gr71/b8/Y8+BP7UH/BRf4jfFzXP2xvgPpPw18Z+JZ9Xmg0abUtW8RRWZ2kpHbJaeUZzgqP3pUE5y2CK/PT4s/CXxVP8UvE0kPhnxFJHJq94VcaZP8AMBM/+zXP/wDCnvF3/Qr+JP8AwWTf/E1+yLVH+V9RrmZ92ab+2N8M/wBvv/g4N8LfFvxxNY+Bfg+PFdjdRf21KFitNM0uFfskdxjKjzPs0QZBlR5u3JAyfk3x38edW/ah/bmv/iNrjM2reOPGn9t3IZt3lNPeeYIwf7qKwUDoAoHauF/4U74uPXwt4kP/AHDJv/ia6H4TfCbxVB8V/DMk3hrxFHGur2hdzpk/yjz05+7QZtqx9x/8FDYmn/bZ+PcaqWeTx74jUKO5Oo3Nfdf7fVvF+0d+2H418b+Bf21vhr4X8L69PBJp2mf8JzrNo1sEgjjYGO3jMS5dWb5SeuepNfGX7dngXXbv9u342zR6D4gkhm+IXiCSOSPSrh1ZDqVwVZSEwQc8EZB9a8rb4f8AiA8f2D4l+h0i4H/stfOqThKSto3ftsfhccVVw8qtF0nJSlf7S2ulqvU+iv2JU8F/Av8A4KceH9a8feO/DHiTQPAmo3XiCTxFBfSS6frl5BaPcQbJ5kDyO9yUALKCXU9eK8B8OeKNT8ffGHTte1q4kudb17xDBqmozv8AemuZ7xZZWP1d2qmPAPiLHy+H/ErD/sE3J/8AZa1vAHgLXx4/8Ps2g+IozHq1kWJ0i4C4FxGST8nAA6msZSuuVd3+JyxqV5qFFQaip32fVrf0M/8A4KHK0v7dPx+VV3M3xB8SqB6k6jc17p/wVE/bS1jxL+0zpN18M/iv4mbwza+C/DtoP+Ee8S3ltZwXcOnxpOgWKRVEiuPm4znrzmvJf29/AOvXf7d/xwnt9B8QTQ3HxD8QyRyJptwyuralcEMDswQR0xkHPWvJ/wDhXHiL7x8O+I8sc5Ol3B/9kr+iKUMNW9lUnNe7G1tOtv8AI/0BoYXD1VRr1GnyxtZ2tql/kfQH/BKX4teG/gH+1LefEfxRq2n2Nx8PvC2t69oSahJkaprS2jx2UC5+9I0suQO+DXgHw6uJbz4ieH5riSS4u59Ws3uJpWLSTSG4jLu5PJZmySTySTSj4ceIsZHh3xF6f8gu4/8AiK1vh18O/EUfxD8Os3h3xDGq6tZ5J0q4wf8ASI+fuVtW9hH2lVTTco23XTt95vOlShUqV+bWUbbrRJPb7z9QvF/xouP2f/8AgpH4o8cW9qt83hn4halevbNJ5ZuY/ts6yRhiCFLIzAEjAOO1d14a8Bab4J+I2u+MPgx+0l4K8C6H4kE373VJ5bHXNJt5ZPNa2ltmhfzij4ClHXdsHTOa8j/av8I61e/tWfFCWPRNbMbeL9WZXTT53Ug3kxBBEeCD2IPI5zXBv4H1x1+bRfELY/6hNx/8TX+df9pVsLiKlL2TlFVJSi/eTTbaumrb2WhyRy+FajCpGfK3CMWvdaasmrp3Wnc9+/aQ+K/gj9qH9vvwlqF14ikvfBsb6NpWr63qlubNL+K22i6nZCTtjk+br/fPavO/jT8WZvjn+2Trni1pvPj1rxaklq/b7Ot4kcAU9x5SJjPtXCnwPr0nXRfEv/gruB/7LWj4K8E61B440MtoevKv9qWjMzaXcKFH2iMkk7OBjknsK5q2ZYvFVJKdP45qbsmtbWXyWv3nXh8tw+HinB35Y8qu1te9/Vnzz8eNSt9I/wCClXji8up47W1sfiteXM80hwkMaa07s5Poqgk+wrp/2/f2g28cf8FEPibrvhvxtql54R1bxcbywu7HVp1s57YvGd6AOBsAB7Y4rjf2zPAXiC4/bJ+L0kXh/XpIpvG+tPGy6ZOyup1CfBDBcEHqCM5rzf8A4V14k/6FzxBz/wBQu4/+Ir+u+Vt3P2bAYHAYiGHxdWpHmjR9nZ2atLld/VW0Psr4kfEL4Z/GD/gvhdeMtW8V6C/wrl8dW2sT61JPmwmt7e3ilX5sYKtLCqehOa+YNc+LGofHr9ra48cauzSan4s8XJq8+87mTz70OI8+iKVQeyiuVPw78SN/zLuv9Mf8gq44/wDHK2Phn8PNft/iN4bkk8P6/GkerWTMTpdwFVRcR5ydnoCc+1U9rm2ByrBYCKnCspOFJU1drRRvqvOTtf8Awo9b+IH7cfxv074ieIoYfjR8XooV1W7RI4/GeookSrO4VVUS8DGBj2HpWWv7dnxw2/8AJaPjF65PjTU//j1cH8THKfEzxKVwG/ti8Az0H+kSV9nfD79jjwDqP7F2k+BbjQo7r9oj4h+C9S+K2h3rSMJ7SxtJoTZ6UI8/MbyzjvZgTyDH/FkY/JcP9ZxEpqM2rXe7+S+Z/KdaeGw9OLnBau2y+fyR82yft3fHCNfm+NXxgXPT/itdT5/8jV6F+yN+2l8ZfEX7Wnwt07UvjB8WLzT7vxlo0F1bXPi7UJobmN76JXjdGlIZGBIZSCCOCDmpP+CXPwi8O/GbX/iZI/gnT/ip450Dwomq+CPBeo3ElvZ+IpzOEuZHCMhl8iFkcRbgG3nkHBHp3xp+DGhfBn9vj9lMR+DdJ+F/j7xFf6HqXjXwRpt291aeHbo6vDHbNHudzF9ohDOY9zbTGOTkk9FHD4n2cMR7R2utLvvb0M6mJoRqyoKKvZ66dr7bnJ+K/wBqn4qWXjTXYU+KHxFCJql0iqviO8AVRO4CgeZwAOAPaqS/tZfFdx8vxO+JDc458S3uP/RlcR8TpjbeI/FEi5DR6hfMCO2JpK/R74v/ALMdr4d/bbk+Heifsg6Fq3w6m1W2sv8AhI4hqVvItrKsRmuRceaYV8vc5zwPkx1rz8DhcRi3N06jSTS+0979r9j+9uJM5yDIIYalXwFObqU5z2oxsqahfWbjdvnVkrt2Z8LH9rH4rMAf+Fn/ABG+bkY8TXvP/kWu9/ZU/ah+JmuftT/DGzvPiP8AEC8s7rxfpFtPDP4ku5Ip0e9iVkZGcqylSQQc5yQRg10H7K3wO8E+Mvj58ZtO8NaPY/GK68L2V1L8PtCu7xltfE6x3ZjkmZo2UzeTCVYKGUSbs5HGOk+JXws0j4Xftrfs1NH4P074deMta1LRr/xZ4Qs7hpoNEuf7UjW3ZdzMyCaIFjHkhSnua2o4PFQ5K8qj0kla7/mtvt8ux5GccRcN154jJ6OAipyoykpclNNN0XUT5fjSSsua1lLS9z8YPin/AMFHf2hrD4meIreH49fGiOOHVLmONE8camqoolcAAecBgACsEf8ABSb9ozGf+F/fGvH/AGPGp/8Ax6qHgT4FX/7UH7dOl/DfS7iOz1Dx543TQILmRN6WrXV95PmsuRlU37iARkLX2V8ef2x/2bf2L/2ptc+Dvh/9lH4b/EL4Y/D/AFubw5rfiDxReX1x4w8StbTtBd3cV5HNHDas7K/lqsJVQAe+F/Vlsf5wSqSUnqfIv/DyX9ozn/i/3xs+Xr/xXGp8f+Rq9i/4J3/8FA/jx4w/4KAfAvR9X+OHxf1LStV+IXh+0vbS68Z6jNBdQyalbrJHIjTFWRlJBU8EHBr0T/gn7+zR8Afjb/wUo+MGieAtIufjR4b0fwzq2s/CPwl4lml0g+MtSjETw2VwVKu3lRNcuFLRmb7OMlNxA9W+Ov7Ofh34E/tw/sB6xf8Awl0X4CfGjxd44srnxt4B0a/lmsrO2g16xi02/WCWSV7Y3K/awUMrg/ZwflIbKa0JlUlbc5/x5+3Z8dLXx/r1vD8bPjHFHFq97HHHD4z1JURBcSBVAEw4AGAOwx6Vln9vD49Ddn43fGb5ev8AxWup8f8AkeubtdItde/aat9PvYVuLK+8cJa3MLHiaKTVQjqfqpYfjX1x+1/8e/gb+zh+2Z4++Heq/ss/DG+8C+D9fl0q4vNM1HUrLXGtF27pYpPPaMTKpyF2gMUxlc5HznvzvJytqfg1OjWrqdWdfkSly631b1W2x80/8N6/HgH/AJLh8ZOmf+R21P8A+P1q+B/26fjtceOtBhuPjV8ZJIJdXs1kSTxnqTRupuIwysDMeCDgjoQT1zX0X8Nv+Cf3hH4Q/wDBTH4q/Cm50uD4q33hbwxd658OPD+q3hs4PGl08UFxa29xIhXfsgkkYgFBKYjytcf+318CtI+EOp/s73958M9K+DHxQ8Vyz3fivwTpNzJJa2VtDqNtHp96IpJJGt2nBnBj3uD5XUFTnT2NSCcub+r2OqnlmOpR9vUqP3ZWau31tvt8tzyr9uT9uL43eGP23fjRpmm/Gj4uadpum+PddtbS0tfGOowwW8KahOiRoiyhURVVQFUAAADHFeYL+398fSP+S4fGj/wt9T/+PVW/4KKSNH+3L8fZF4ZfH/iIgj1/tK5r9Ef28/ghN8Af2uPGPg74d/8ABPnwz448G6LcQLpmtJ4c8Q3Qv1eCN3YS283lHDsy/KONuDzX7xUxWHw8aVN00243+ytrd7dz+4JYrCYSnSpukpOUb391LS19XbXU/PZv+CgPx8B/5Ll8Zv8AwuNU/wDj9avgP9vj47Xfj3QYZvjf8Y5oZNVtI5UfxtqboytOgKspm5BHBHfJr2r9gv4PeH/j5+0L8ebi6+C2ga78TPDuhPqHgz4P3M11ZabLeJcrDeQlHcTubWPDiB5AzF2GQQCKf/BRz4FeH/gh8a/gZ5XgnSvhP8RPEmk2mp+OPAul3klzZ+Hbn7eqWzx+Y7tCbiEGQwl22bBySSTNbF4eU5YX2aTa30/lv/T2CWYYKeIeDdJJtf3Xry3+a89j6r/ak/ak+J2gftO/EuysviV8QLKzs/FeqW8EFv4jvIoYI0vJVVEVXwqgAAKAAABgDFcP/wANZ/FnH/JUviR1x/yM17/8cqr+2hO1v+0z8XpEO1o/FOuMp9CLucivq/48fC2T4aeLtL0rwX+ypofjvRbjQNNvBrA0jVbj7VPNbq8vzQSCPIc9sEd6/gWNHHYzEYirCvKMYTt9uT1b2Ub6aFOtg8NSowdJNyjf7K2Svdu3c+WT+1p8Wjn/AIun8SvcDxLe9v8AtpWh4O/au+K174w0WKT4o/EZozqVpG6N4jvWDhp4wQR5vIIJBHQ5r0r9kTwJpPxjvPjBrWm/DHQ/GXjfSlsrnw94Flllj0+C3knlju5Uj8xXn8giNQjSDG7rkjGL+178OtD+GP7TPgnT9N0O08J6td2GjX3iLw1a3T3UHh7UpbjMkEbMc7TGI325IXfjNZyweY08LDHOu3FtdZbc1t3pf+7e6WrKjisHVxTwaopNJ62j/Lf7td9m9D5F/a+/bB+L2jftffFuzs/ix8ULGzsPGus21tbW/iq+hht4lvp1VERZcKqqAABgAAdMV5uP20/jSitj4w/Fgc8k+LtQ/wDj1d94p/Z9v/2rP+CsPir4cWN7/Ztx4x+KOq6c15sEn2KNtRuDLKFJAYrGrsFJGSAMjNdl46/bE+Cnwp+J2oeF/BX7Mvw58WfD3QLuXTxeeIp7648R+IYYZGje5N4kqrbPLjcoWFlTcMg9B/VenU/ZadbBUaFDC0MGqtT2UZysoJJbauVtW00l1szw7/htT415z/wuP4rY/wCxu1D/AOO1r+AP2zvjPc/EDQI5vjB8Unj/ALUs1dH8WX7K6m4TIIM3QjjHQjPrXsP7MvwV+Bvxh/4KVah4d8Kw6l4y+HdzY3+o+C9B16WXS5vEWopaeda6TcTH51Uzbo/M/iVBzzzv/ttfAew+GHw8+B3iTxR8J/D/AMBfi94g8VTWt/4S0eaY22o6RFLbmHUWglkma3cTMYseYd4Yk5wArlaxzYzOsn+sQy/6paVSF7uMU1zRk0uX4nazu0rJ2R4x+2/+298bPDf7bHxm0zS/jT8XNP03TfHeu2ttaW3jDUYYLaJNRnVI0RJlVUVQAAoAAAAAArzE/t6/HlPvfHT4yqR1/wCK31P/AOP10P7S2k22vf8ABVP4hWF5CtxY6h8Y761uonPyzQya+8ciH2ZWYHHY17V+27/wST+L+kft3/EKz+HfwD8dy/DWHxSItG/s3SJnsDp++P8A1T8kx4385PBNfukKmAw8adKtGKbhzXaWtraeup/mzUo4yrOc6U3ZTatd6dT51P7enx6C/wDJdPjP8xwP+K31P/4/W38N/wBu346v8R/DcE3xu+MU0dxrNlFLHJ4z1JlkX7QgKsDPyCCQR05Ir6t/4Y++GK/8Fnv2gvh9pvgHRfEX/CIaPf3fw7+Hc1/JZ6d4i1aK1tZEtHkDbtgR55vL3LvKEblxXnf/AAUA/Z/0P4Q6/wDs16jefC/R/gj8WvFk9xc+LPA+k3ck1rY2sOpWsWnXwikkka3acfaAY/MbPldipzzxzLL6tSFGNK3Ok9o9Ve3e3d2snoOWBxcIOtKq7Requ+jt/S3PM/Dngfw/8S/2r10Dxb4htfCvhXVvFM8Gs6vcErHY2X2pzO2RnDGMMq/7Tr719UfEn/guD4o0z9sa48aeF/Bvwvl8KeHdbWHQ3fwfaPrTaLbP5SRR3pHmxs9uGCkH5BKBgDNfI/xF+HXiSb4meJWXw34k/wCQvevn+y7jBBuJOB8mD2ORnrWT/wAK18Srz/wjfiTnqf7MuM/+i6/jenjK+HThST3vfuf09WwtCvadXXS1ux9p+HtQ+E/hbxN+1h8Jfh/8XvC/g3RvilNpOp+CfFk089ppT2UM89zcaLPcRr5lsuLnyw20hvLIYcAGDxD4+0CH4r/sT/DO18faX8UvFHwy8WI2teI9Nmku7C3jvdXsXttNgupQHuFgWFyWwFXcoUdQPjNfhr4kBz/wjXibOeo0m4b/ANp+9ejfsafD/wASWX7ZHwjkl8OeIY7ePxvojtJJpdwqxj7fASWOzAUYOckYHPFdlLMJ1HyKnbVfde5yzwMIxcnNu2vztbc2PilA114i8VRxjc8moXqqPUmaTFfdX7T3jjS/ij+2xrHxC8K/tYeEPCnhSfUbS+gig1nVXuoI4ViEiLaJEIWLFX+Qttbfz1IPxf408E67P448QH+wdcIOq3bfLp0+MefJ/scg9QRVE+CdeJLNoevZIwcadN/8RXj4PGVMPz0+Ru7T3a1V7bdNdT/RDO+GcLnUcNiHiVTlTpyg1y05pxqez5tJppNcis7dWfVlh8f/AAT8cfHv7TmkaB4k0/4Ux/F7ULDUPDur6kZLG1uYLWWRrixnkiy1utyZBJnGCSwYEgCkvfGGkr8Uf2R/h/b+NNP+IXiTwD4qiXVdd05pLi1iju9TtHg0+GeQB5lhETktgKNwAAr5SfwProUf8SPXgFORnTpuP/Ha779k3wdrEP7WHwpeTRtYjSLxlo7M72MqqgF7CckleAMckkcV0U80rzlGnKG7Wuq0clKyW2/Xc+bzPw+yvA4etjMPiW406cpKL5G+eOHdFNz+NrkXw3tzan5Q+BPjnffswft16X8R9Lto73UPAfjdNfgtpH2JdNa33neUzAHCvs2kgHAOa+zf2jP2Hf2fP2zP2rfEXxg8J/tZfCTwT8KfiJrc3iXVdN8TC7tvFnhg3UpnubUacsbrcuju4jMc2xsAZAwT8PfFf4K+Mrz4o+JZI/CPiiRZNWuypGkz4b98/wDs1g/8KM8cH/mTfFX/AIKLj/4iv1xbH+acviZ+h37L37T/AOz+/wC0J+1l4D+Fetf8M5+E/jT4ai8N/Dvxbrd1dNFpiW1zFJNBdzjfPbQ6gqHcRu8sBVbdjB67RfHvg/4Z3X7CXwEk+Knhn45fEHwb8cbPxFca54fupr/S/CmmXd9p0aaTBdTRqZfMki89wgCoUAIPyk/mH/wovxxn/kTfFX/gpuP/AImvav8Agmz8HPF2lf8ABRX4Az3XhbxHa28PxI8OySyTaZOiRqNUtyWYlcAAZJJo6ES2PfLPUoNG/act768nitbOx8bpdXE8mdkEUeq73du+FVST7CvrX9rn9nr4Q/tL/ts/EPx9qn7T3wf0r4f+LPEMmqyx6YdR1HW/sT7d0ccAtVj89grKMSsASD82MH5H+Inwz8St8QvEJXw34kdZdYvmBGkXBXH2mQgghPcEHnrWSPhj4m2/8i34n/8ABRc//EV8zGrGCakr6/kfglPGuip0Z0lJOfNrfdadD7k8M/t6+F/2oP2nv2m75vFEfwVb42+GbTw74K8R6lK8MWh29nJGFtLmaEFraO7hjUMybghABzgZ4T9o/wAXeH/A/wCzl8AvhD/wsbw/8WvGHhPxzPr1zq2i3c17pvhmwuXtki0uG6mQGXzHTzWCgKhjwR90n5XPwy8SqQf+Eb8TN3/5A9zwf+/dangD4beJh4/8PlvDviRFj1eyY7tJuAuBcxkk/JxjueKp4mUo27/8OdVPOcRV/dzhq3vqtHJPbbTuZP8AwUSVpf25fj4qLuZvH/iMADv/AMTK5r7w/wCCiHj66/aJ/bO8beM/h3+3N8PvCngvXLiCXS9KPjvxBYG1C28cbjybe1aJcujN8hI5yeTXxh+3v8MfEt9+3b8b7iHw34jmhn+IWvyRvHpNwySI2pXBDKQuCD2IzkV5UfhX4qI/5FXxRj/sEXP/AMRX9AvCwrRpVI1FFxjbZPe3c/uz6hDEwo1lU5XGKWyd7pN7+h9c/sm+KNB0Lwb+098G9S+OHhTw94y+Jx0a50n4hyalenRfEItLi4nvLKW/aNZ4xc/aVJd4yHZZAwOFzS/bQ+IXh23+H37Lvwts/iHpXxZ8WfDG6v217xHpUkt3p8EF7qFnJaaXBdy4e4W3WGT5sBV3gL1YD5TPwp8WHn/hFvFH/gouf/iK1vh98L/FEHxB8PSN4a8TRiPV7NmLaTcgY+0R9fk4A681lWy2lGcq7qX0vbTV8tnr6dCZZRRp1XiPaXteVvdWvLy3v2t02P0C/bMha5/aZ+L0ca7nk8U62qj1Ju5wK9W/bW/as1jWvjRp9x4H+IfiJdDh8OaTbj+yNbuYLdbiO1VZgERlG4OMHjOa4P8Aaw8E6zeftV/FF49F1qSOXxfq7KU06Z1cG9mIIITBB7EZ4rgf+Ff69uz/AGD4g3N1P9mT/wDxFf534rHV6NXE4ekmuad29U1Zu23e52YXC0asaNao0+WNradVHXXtY9s/Zr8SaP4q/Zf+IXw1m8baT8PfEniDXbHWYtR1a4ltLLXbSKJ0exmuY1Zo9sjGQBwVYsRzliJ/2ofHuleIfE3wR8M2fiiHx5qvgGxttK1fxHA0klvfTSahHLFDFJJh5o4EygkP3t3AGCK+fPF/gnxdB4R1g6ToOvf2wNPuTY7tMm5uPKfyhyuPv468ZNcf8NPhz8VNO+I3hmSTRfHTLJ4h0gie4jdo3tDeW5ZZLfyA8TLCJfMkLjMnZsgp7mU1K+MwSoS5Y2tG7crtJ82i2Wr3td6nBiqNOhivb6tNuVlbdrl3WtrdNjL1/wDaDuf2VP8AgrT4o+JFnZDUm8H/ABT1bUHsjIEN7ENQuFkjDdFZo2cAngEgmvZfAXgfSfhP8bfEPxC/Z2/a8+Gnw38M+LBcKTr80+na/olpPMLhrOa0a3lEzROqhWjcB9g+7zn5J/bd+GHxEvf25vjRcWOg+IlsZfiBrj2zf2dK0bxm/n2upVMFDk8DPVTk81wHhz4P+Pol33nh/wAVtJHauFR9PuGzIWO3ny/4VA6DktX9Pc3dM/S6cY5lToqEJ03KmoSbjGUZRVmrqWmju4/M/SjxH+3x4A+LH/BQj4j6pa/EjUNGh8afClPh7o/xJv8ATpbP7DqyRQq+oPGhMtvDO0cqGRfmTzM8DmvMPip4k8P/AAZ/Y78A/B26+J3h34teMpvida+KbZ9Av5tS0/whp6xpbtCt3MikvcSMHMaDaApJ5A3fCh+FPxSsovs6+HdemVo2X7R/ZM43fINhxs6hs5NbXhD4dfFJ/HOizWvhXW/tEOsRPbI+myRo+JoTGHby9uNwOSRwD6cUcztojzaOX4LD8nsoVUoKOjjF6wjJJ826Wt2o+6/vPSP2l9XtfDv/AAVY+IGoXk8drZ6f8ZL+6uZn4WCKPxA7yOcZwFVWJ74FdJ/wUE/ar1D4gf8ABRX4meIvB/xH8S3ngrVvGJvNNudO8Q3kNjNZmSIlo1WQDy8BuAuODxXjH7T3w0+L2o/tF+OLjxV4X1xvF1x4x1aTxA2m6e5tzdG8laYIY08tozKW+ZDjGSvUV59B8MPig1vvbwzrizSCMNt0W4wP9Zu42Yyfk59h75/bo1qM5U6k4SdoKNkk90m3+Vj+B5U6seeMJpe+29Wno2unRn6eeKv2qfh38Sv+Cgf7ZdjovxS0XwXD8etHttK8H/EQyTR6fatCbeWe2lnjHmQwXSr5bSKODEM54B8m/aJ8Z+H/AAR+zh+zz8GG+Jnh/wCL/jbwn48ufENxrOg3k19pnhnTrqS1RNJgu51Vpt7oJmVQFQxgEfdJ+Hk+F/xQaKTzfCevKy7fLxos542vnI2DGTtHtk11Xwo+GvxFT4ueFDceF9e+zr4gsw+NHuFKxC5iO4HZg9COeec1wUcHRhUjJc9o23S3UeVO++3RFVK1acXGThd36vZu9rbNn//Z";
                this.loadOBJMesh = function() { 
                    //var groupImport = new APP.Object3D();
                   // this.importedMesh = null;
                    //var importedMesh = new APP.Mesh({
                    //    strMeshOBJ: this.stringMeshOBJ,
                    //    strTextureOBJ: this.stringTextureOBJ,
                    //});
                    //importedMesh.transform.position.xyz = vec3.fromValues(Math.random() * 5 - Math.random() * 10, Math.random() * 5, Math.random() * -5);
                    //console.log(importedMesh);
                    //groupTestUser.add(importedMesh);
                    //scene.add(groupTestUser);
                    //console.log("2");
                };
                this.saveCanvaImage = function() {
                    window.open(gl.canvas.toDataURL());
                };
            };

            var gameGUI = function() {
                this.chooseLevel = "0: Corneria";
                this.startGame = function() {             
                    levelLoader = APP.LevelLoader({
                        scene: scene, //Is scene has not been initialized in main, create a new one
                        levelNo: parseInt(this.chooseLevel.substring(0,1)),
                        camera: camera,
                    });
                    //  console.log("Entering:" , this.chooseLevel , levelNo,this.chooseLevel.substring(0));
                };
                this.saveGame = function() { 
                    this.saveGameInfo = [levelLoader.levelNo, APP.unitMainArwing.transform.position]; 
                    console.log("level number: "+levelLoader.levelNo, "position:" + this.saveGameInfo[2]);
                };
                this.loadGame = function () {
                    levelLoader = APP.LevelLoader({
                        scene: scene, //Is scene has not been initialized in main, create a new one
                        levelNo: this.saveGameInfo[0],
                        mainArwingPosition: this.saveGameInfo[1],//z pos
                    });
                    console.log("Loading:" + this.chooseLevel);
                };
                this.energy = 0;
 //               
            };


            var worldEditorG = new worldEditorGUI();
            gui.add(worldEditorG, 'chooseCubeMap', ["sky", "space", "scpace2", "sun", "mountaintopsnow", "mountainswinter"]);
            gui.add(worldEditorG, 'loadCubeMap');
            gui.add(worldEditorG, 'stringMeshOBJ');
            gui.add(worldEditorG, 'stringTextureOBJ');
            gui.add(worldEditorG, 'loadOBJMesh');
            gui.add(worldEditorG, 'saveCanvaImage');

            var gameG = new gameGUI();
            gui.add(gameG, 'chooseLevel', ["0: Opening Cinematic", "1: Corneria", "2: Asteroid Field", "3: Sector Y", "4: Fichina", "5: Sun", "6: Sector X", "7: Sector Z"]);
            gui.add(gameG, 'startGame');
            gui.add(gameG, 'saveGame');
            gui.add(gameG, 'loadGame');
            gui.add(gameG, 'energy', 0, 100).listen();
            ////////////////////////////////////////////////
            camera.position[2] += 2;
            APP.update = function () {
                //  requestAnimationFrame(APP.update);
                if (APP.unitMainArwing != null) {
                    gameG.energy = APP.unitMainArwing.energy;
                }
               
            };
            //this.update = update;
           // addgrid = false;

            // ------------------------------------------------------------------------------------------//

            var ambientDirectionalFolder = gui.addFolder('Ambient and Directional lighting');
            var ambientColorCtrl = ambientDirectionalFolder.addColor(APP.sceneParameters, 'ambientColor');
            var directionalColorCtrl = ambientDirectionalFolder.addColor(APP.sceneParameters, 'directionalLightColor');
            var specularColorCtrl = ambientDirectionalFolder.addColor(APP.sceneParameters, 'specularColor');
            var directionalXCtrl = ambientDirectionalFolder.add(APP.sceneParameters, 'directionalLightX', -100, 100);
            var directionalYCtrl = ambientDirectionalFolder.add(APP.sceneParameters, 'directionalLightY', -100, 100);
            var directionalZCtrl = ambientDirectionalFolder.add(APP.sceneParameters, 'directionalLightZ', -100, 100);

            ambientColorCtrl.onChange(function(value) {
                scene.ambientLightColor = APP.Utils.convertRGBToWebGLColor(value);
            });
            directionalColorCtrl.onChange(function(value) {
                scene.directionalLight.color = APP.Utils.convertRGBToWebGLColor(value);
            });
            specularColorCtrl.onChange(function(value) {
                scene.directionalLight.specular = APP.Utils.convertRGBToWebGLColor(value);
            });
            directionalXCtrl.onChange(function(value) {
                scene.directionalLight.direction[0] = value;
            });
            directionalYCtrl.onChange(function(value) {
                scene.directionalLight.direction[1] = value;
            });
            directionalZCtrl.onChange(function(value) {
                scene.directionalLight.direction[2] = value;
            });


        },

        draw: function() {
            renderer.render(scene, camera);
        },

        update: function(animate) {

            now = _.now();

            if (animate) {



                if (APP.openingAnimateOn == true) {
                    increasingFactor += 0.5;
                    APP.animateArwings(increasingFactor);
                }


                APP.update();//Energy GameGUI
//                //console.log(APP.APP.unitMainArwing.energy);
                //L'avion avance constamment avec le cubemap

//                 // scene.transform.position.z += mulByTimeFactor(-vitesseCroisiere);
//                 //groupMesh.transform.position.z += mulByTimeFactor(vitesseCroisiere);
//                //snowStormEvent1.animate();
//                //groupTestColors.transform.rotation.z += 1;

                //scene.transform.position.z += mulByTimeFactor(-vitesseCroisiere);
                //groupMesh.transform.position.z += mulByTimeFactor(vitesseCroisiere);


                // square.transform.rotation.x += mulByTimeFactor(0.4);
                // square.transform.rotation.y += mulByTimeFactor(0.4);

                // line3.transform.rotation.x += mulByTimeFactor(4);
                // line3.transform.rotation.y += mulByTimeFactor(4);

                // group1.transform.rotation.y += mulByTimeFactor(2);
                // group2.transform.rotation.y -= mulByTimeFactor(2);
                // triangle2.transform.rotation.x -= mulByTimeFactor(1);

                // quad.transform.rotation.y -= mulByTimeFactor(2);

                // bezier.transform.rotation.y += mulByTimeFactor(0.5);
                // coons.transform.rotation.y += mulByTimeFactor(0.5);
                // coons.transform.rotation.x += mulByTimeFactor(0.3);

                // boxcentered1.transform.rotation.y += 1;
                // boxcentered1.transform.rotation.x += 1;

                //Avions bougent
                // groupMesh.transform.position.xyz = vec3.add(groupMesh.transform.position.xyz, groupMesh.transform.position.xyz, vec3.fromValues(0, 0, -0.1));
                //  console.log(groupMesh.transform.position.xyz);

                ////**************  Plane Control *************************//

                //// OO OO OO  BARREL ROLLING OO OO OO //
                //if (isBarrelRolling == true) {
                //    increasingFactor += 0.08;
                //    decreasingFactor = Math.pow(2, -(increasingFactor)) * 80;
                //    groupMesh.transform.rotation.z += mulByTimeFactor(decreasingFactor);
                //    if (decreasingFactor <= 0.001) {
                //        isBarrelRolling = false;
                //    }
                //}

                //// <- <- <-  MOVE LEFT <- <- <- //
                //if (isMovingLeft == true) {
                //    groupMesh.transform.position.x += -0.15;
                //    scene.transform.position.x += 0.15;
                //    //TODO pivoter l'avion sur lui-même un petit peu
                //    /*                    if (mesh.transform.rotation.z <= 90) {
                //                            increasingFactor += 0.5;
                //                            decreasingFactor = 9 * (Math.pow(5, (-0.2 * Math.pow((increasingFactor - 5), 2))));
                //                            groupMesh.transform.rotation.z += mulByTimeFactor(decreasingFactor);
                //                            groupMesh.transform.position.x += 1;
                //                            if (decreasingFactor <= 0.001) {
                //                                isBarrelRolling = false;
                //                            }

                //                        }
                //    */
                //}


                //// -> -> ->  MOVE RIGHT -> -> -> //
                //if (isMovingRight == true) {
                //    groupMesh.transform.position.x += 0.15;
                //    scene.transform.position.x += -0.15;
                //}

                //// /\ /\ /\  MOVE UP /\ /\ /\ //
                //if (isMovingUp == true) {
                //    groupMesh.transform.position.y += 0.15;
                //    scene.transform.position.y += -0.15;
                //}

                //// \/ \/ \/  MOVE DOWN \/ \/ \/ //
                //if (isMovingDown == true) {
                //    groupMesh.transform.position.y += -0.15;
                //    scene.transform.position.y += 0.15;
                //}

                //// ++ ++ ++  ACCELERATING ++ ++ ++ //
                //if (isAccelerating == true) {
                //    groupMesh.transform.position.z += -0.15;
                //    scene.transform.position.z += 0.15;
                //}

                //// -- -- --  DECELERATING -- -- -- //
                //if (isDecelerating == true) {
                //    groupMesh.transform.position.z += 0.15;
                //    scene.transform.position.z += -0.15;
                //}


                /* mesh.transform.rotation.z += mulByTimeFactor(0.1);
                 mesh.transform.position.xyz = vec3.fromValues(-0.05, 0, 0);*/


            }

            lastTime = now;

        }

    });


    //===============================================

    // Register handler.
    window.addEventListener('resize', onWindowResize, false);

    var gl = APP.WebGLContext;

    function onWindowResize() {
        var canvas = querySelector("#canvas");

        canvasWidth = canvas.clientWidth;
        gl.canvas.width = canvasWidth;
        canvasHeight = canvas.clientHeight;
        gl.canvas.height = canvasHeight;
        gl.viewport(0, 0, gl.drawingBufferWidth, gl.drawingBufferHeight);
        camera.setAspectRatio(canvasWidth / canvasHeight);
    }


    //=====================================================
    // Start the application's main loop.

    APP.app = app;
    app.start();

}
